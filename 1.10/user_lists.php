<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-10 | Upload images.</title>
    </head>
    <body>
        <a href="./"><input type="button" value="Add New"></a><br><br>
        <table border>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Profile</td>
            </tr>
            <?php
                $file = fopen("user_info.csv", "r");
                while (($data = fgetcsv($file)) !== false) {
                    ?> <tr> <?php
                    foreach ($data as $value) {
                        ?> <td><?= $value ?> <?php
                    }
                    ?> </tr> <?php
                }
                fclose($file);
            ?>
        </table>
    </body>
</html>