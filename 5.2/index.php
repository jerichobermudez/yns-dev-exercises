<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 5-2 | Calendar.</title>
        <link href="main.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <h3>Exercise 5-2 | Calendar.</h3>
        <?php 
            $year = isset($_GET['y']) ?  $_GET['y'] : date('Y');
            $month = isset($_GET['m']) ?  $_GET['m'] : intval(date('m'));
            //months array just like Jan,Feb,Mar,Apr in short format
            $months = ['1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec'];
            $days = ['1' => 31, '2' => 28, '3' => 31, '4' => 30, '5' => 31, '6' => 30, '7' => 31, '8' => 31, '9' => 30, '10' => 31, '11' => 30, '12' => 31];
            $day_month = ($month == 2 && ($year % 4) == 0) ? 29 : $days[$month];
            $next_y = (($month + 1) > 12) ? ($year + 1) : $year;
            $next_m = (($month + 1) > 12) ? 1 : ($month + 1);
            $prev_y = (($month - 1) <= 0) ? ($year - 1) : $year;
            $prev_m = (($month - 1) <= 0) ? 12 : ($month - 1);
        ?>
        <table border cellpadding="10" cellspacing="0">
            <tr style="font-size: 16pt;">
                <th> <a href="index.php?y=<?= $prev_y . '&m=' . $prev_m ?>" title="Previous">&lsaquo;&lsaquo;</a> </th>
                <th colspan="5"> <?= $months[$month] . ' '. $year ?> </th>
                <th> <a href="index.php?y=<?= $next_y . '&m=' . $next_m ?>" title="Next">&rsaquo;&rsaquo;</a> </th>
            </tr>
            <?php
                //days array
                $days_array = ['1' => 'Sun', '2' => 'Mon', '3' => 'Tue', '4' => 'Wed', '5' => 'Thu', '6' => 'Fri', '7' => 'Sat'];
                //display
                foreach ($days_array as $key => $val){
                    ?> <th> <?= $val ?> </th> <?php
                }
                ?> <tr><td colspan="6"></td> <?php
                $date = $year . '-' . $month . '-01';
                //find start day of the month
                $startday = array_search(date('D', strtotime($date)), $days_array);
                $monthToday = date("m");
                $dayToday = date("d");
                $yearToday = date("Y");
                //daisplay month dates
                for ($i = 0; $i < ($day_month + $startday); $i++){
                    $day = ($i - $startday + 1 <= 9) ? '0'.($i - $startday + 1) : $i - $startday + 1;
                    if ($day == $dayToday && intval($month) == intval($monthToday) && $year == $yearToday) {
                        $dayHighlight = '<td style = "background: indianred; color: #fff; text-shadow: 1px 1px 1px black;">' . $day . '</td>';
                    } else {
                        $dayHighlight = '<td>' . $day . '</td>';
                    }
                    echo ($i < $startday) ? '<td></td>' : $dayHighlight;
                    echo (($i % 7) == 0) ? '<tr></tr>' : '';
                }
            ?>
        </table>
    </body>
</html>