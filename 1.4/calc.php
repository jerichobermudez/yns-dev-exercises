<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-4 | Solve FizzBuzz problem.</title>
    </head>
    <body>
        <?php
            if (isset($_POST['submit'])) {
                $num = $_POST['num1'];
                for ($i = 1; $i <= $num; $i++) {
                    switch ($i) {
                        case ($i % 15 === 0):
                            echo "\nFizzBuzz";
                            break;
                        case ($i % 3 === 0):
                            echo "\nFizz";
                            break;
                        case ($i % 5 === 0):
                            echo "\nBuzz";
                            break;
                        default:
                            echo "\n$i";
                            break;
                    }
                }
            }
        ?>
    </body>
</html>