<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-14 | Show images according to the options in combo box.</title>
    </head>
    <body>
        <h3>Show images according to the options in combo box.</h3>
        <select id="chooseImage">
            <option value=""> Choose Image </option>
            <option value="https://cdn.shopify.com/s/files/1/0712/4751/products/BX7E-02E_High_Large_TOP_2000x.png">Image 1</option>
            <option value="https://sportshub.cbsistatic.com/i/r/2020/01/28/3454846d-4979-470c-8a22-d7755ab12d4d/thumbnail/1200x675/137f74c409db16eec3e22b06c2be41f3/general-basketball.jpg">Image 2</option>
            <option value="https://a57.foxnews.com/static.foxnews.com/foxnews.com/content/uploads/2018/11/931/524/Basketball-iStock.jpg">Image 3</option>
        </select>
        <br><br>
        <div id="result">
            <img src="https://cdn.shopify.com/s/files/1/0712/4751/products/BX7E-02E_High_Large_TOP_2000x.png" id="image" width="30%">
        </div>
    </body>
    <script type="text/javascript" src="main.js"></script>
</hrml>