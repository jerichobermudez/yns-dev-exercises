<?php
    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'quiz_db');
    $connection = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DATABASE . ";charset=utf8", USERNAME, PASSWORD);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 5-1 | Quiz with three multiple choices.</title>
    </head>
    <body>
        <h3>Exercise 5-1 | Quiz with three multiple choices.</h3>
        <?php
            if (isset($_POST['submit'])) {
                $x = $_POST['x'];
                $choices = [];
                $score = 0;
                for ($i = 1; $i <= $x; $i++) {
                    $choiceID = $_POST[$i];
                    $qry = "SELECT answer_id FROM answers WHERE choice_id = ?";
                    $stmt = $connection->prepare($qry);
                    $stmt->execute([$choiceID]);
                    $result = $stmt->rowCount();
                    if ($result === 1) {
                        $score++;
                    }
                }
                ?>
                <p>Score: <b><?= $score ?> out of <?= $x ?> </b></p>
                <p> Click <a href="./">here</a> to retake Quiz.</p>
                <?php
            } else {
                ?>
                <form method="POST">
                    <ol>
                    <?php
                        $questionArray = [];
                        $qry = "SELECT question_id id, question_text question FROM questions ORDER BY RAND()";
                        $stmt = $connection->prepare($qry);
                        $stmt->execute();
                        $questionArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $count = $stmt->rowCount();
                        $choiceArray = [];
                        foreach ($questionArray as $key => $row) {
                            $id = $row['id'];
                            $question = $row['question'];
                            ?> <li> <?= $question; ?> </li> <br> <?php
                            $qry = "SELECT choice_id, choice_answers FROM choices WHERE question_id = ? ORDER BY RAND()";
                            $stmt = $connection->prepare($qry);
                            $stmt->execute([$id]);
                            $choiceArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($choiceArray as $choiceKey => $choiceRow) {
                                $choiceID = $choiceRow['choice_id'];
                                $cAnswers = $choiceRow['choice_answers'];
                                ?>
                                <input type="radio" id="<?= $choiceID ?>" name="<?= $id ?>" value="<?= $choiceID ?>" required>
                                <label for="<?= $choiceID ?>"><?= $cAnswers ?></label>
                                <br>
                                <?php
                            }
                            ?> <br> <?php
                        }
                    ?>
                    </ol>
                    <input type="text" hidden name="x" value="<?= $count ?>">
                    <input type="submit" name="submit" value="Submit Quiz">
                </form>
                <?php
            }
        ?>
    </body>
</html>