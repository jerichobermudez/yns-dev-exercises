-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2020 at 07:23 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `question_id`, `choice_id`) VALUES
(1, 1, 2),
(2, 2, 4),
(3, 3, 9),
(4, 4, 11),
(5, 5, 15),
(6, 6, 18),
(7, 7, 20),
(8, 8, 22),
(9, 9, 27),
(10, 10, 28);

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `choice_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_answers` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`choice_id`, `question_id`, `choice_answers`) VALUES
(1, 1, 'Hypertext Protocol'),
(2, 1, 'Hypertext Preprocessor'),
(3, 1, 'Hypertext Process'),
(4, 2, 'Rasmus Lerdorf'),
(5, 2, 'Rasmus Rudolf'),
(6, 2, 'Rasmus Moose'),
(7, 3, '1993'),
(8, 3, '1994'),
(9, 3, '1995'),
(10, 4, '7.3'),
(11, 4, '7.4'),
(12, 4, '8.0'),
(13, 5, 'Strong Question Language'),
(14, 5, 'Structured Question Language'),
(15, 5, 'Structured Query Language'),
(16, 6, 'SELECT Persons.FirstName'),
(17, 6, 'EXTRACT FirstName FROM Persons'),
(18, 6, 'SELECT FirstName FROM Persons'),
(19, 7, 'SELECT UNIQUE'),
(20, 7, 'SELECT DISTINCT'),
(21, 7, 'SELECT DIFFERENT'),
(22, 8, 'ORDER BY'),
(23, 8, 'SORT BY'),
(24, 8, 'GROUP BY'),
(25, 9, ' INSERT VALUES (\'Jericho\', \'Bermudez\') INTO Persons'),
(26, 9, ' INSERT (\'Jericho\', \'Bermudez\') INTO Persons'),
(27, 9, ' INSERT INTO Persons VALUES (\'Jericho\', \'Bermudez\')'),
(28, 10, ' DELETE FROM Persons WHERE FirstName = \'Jericho\''),
(29, 10, ' DELETE ROW FirstName=\'Jericho\' FROM Persons'),
(30, 10, ' DELETE FirstName=\'Jericho\' FROM Persons');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `question_text` text NOT NULL,
  `answer_id` int(11) NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `question_text`, `answer_id`, `datecreated`) VALUES
(1, 'PHP means ______?', 0, '2020-03-16 02:39:40'),
(2, 'Founder of PHP', 0, '2020-03-16 02:08:51'),
(3, 'When was PHP created?', 0, '2020-03-16 02:27:46'),
(4, 'What is the latest version of PHP?', 0, '2020-03-16 02:27:46'),
(5, 'What does SQL stand for?', 0, '2020-03-16 02:50:52'),
(6, 'How do you select a column named \"FirstName\" from a table named \"Persons\"?', 0, '2020-03-16 03:02:22'),
(7, 'Which SQL statement is used to return only different values?', 0, '2020-03-16 02:57:48'),
(8, 'Which SQL keyword is used to sort the result-set?', 0, '2020-03-16 02:58:38'),
(9, 'How can you insert a new record into the \"Persons\" table?', 0, '2020-03-16 03:02:17'),
(10, 'How can you delete the records where the \"FirstName\" is \"Peter\" in the Persons Table?', 0, '2020-03-16 03:02:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`choice_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `choice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
