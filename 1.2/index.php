<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-2 | The four basic operations of arithmetic</title>
    </head>
    <body>
        <form action="calc.php" method="POST">
            <label><b>Num 1: </b></label>
            <input type="number" name="num1" required>
            <br><br>
            <label><b>Num 2: </b></label>
            <input type="number" name="num2" required>
            <br><br>
            <input type="submit" name="add" value="Add">
            <input type="submit" name="sub" value="Subtract">
            <input type="submit" name="mul" value="Multiply">
            <input type="submit" name="div" value="Divide">
        </form>
    </body>
<html>