<?php
    $num1 = isset($_POST['num1']) ? $_POST['num1'] : 0;
    $num2 = isset($_POST['num2']) ? $_POST['num2'] : 0;
    $operation = null;
    $result = 0;
    switch(true) {
        case isset($_POST['add']):
            $operation = '+';
            $result = $num1 + $num2;
            break;
        case isset($_POST['sub']):
            $operation = '-';
            $result = $num1 - $num2;
            break;
        case isset($_POST['mul']):
            $operation = '*';
            $result = $num1 * $num2;
            break;
        case isset($_POST['div']):
            $operation = '/';
            $result = $num1 / $num2;
            break;
        default:
            $operation = null;
            $result = 0;
            break;
    }
?>
<b>RESULT:</b><br> <?= $num1 . ' ' . $operation . ' ' . $num2 . ' = ' . $result ?>