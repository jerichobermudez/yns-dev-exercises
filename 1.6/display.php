<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-6 | Input User Information.</title>
    </head>
    <body>
        <?php
            if (isset($_POST['submit'])) {
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $email = $_POST['email'];
                $phone = $_POST['phone'];
                ?>
                <b>User Information:</b>
                <br><br>
                <b>Name: </b> <?= $fname . ' ' . $lname ?>
                <br>
                <b>Email: </b> <?= $email ?>
                <br>
                <b>Phone: </b> <?= $phone ?>
                <?php
            }
        ?>
    </body>
</html>