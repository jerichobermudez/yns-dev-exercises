
var colors = ['darkblue', 'blue', 'royalblue', 'lightblue'];
var active = 0;
document.querySelector('body').style.background = colors[0];
document.querySelector('body').style.color = 'white';
setInterval(function() {
    document.querySelector('body').style.background = colors[active];
    active++;
    if (active == colors.length) {
        active = 4;
    }
    document.querySelector('body').style.color = colors[1];
}, 100);