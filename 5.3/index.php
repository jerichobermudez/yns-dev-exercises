<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 5-3 | Navigation Links.</title>
    </head>
    <body>
        <h3>Exercise 5-3 | Navigation Links.</h3>
        <?php
            $directory = [];
            foreach (new DirectoryIterator('../../yns-dev-exercises') as $files) {
                $link = $files->getFilename();
                if (strpos($link, '.') === 0) continue;
                    array_push($directory, '<a href="../' . $link . '">' . $link . '<br>');
            }
            foreach ($directory as $key => $navigations) {
                echo ($key + 1).' . '.$navigations;
            }
        ?>
    </body>
</html>