var txtChange = document.getElementsByClassName("txtChange");
for (var i = 0; i < txtChange.length; i++) {
    txtChange[i].addEventListener('click', function() {
        var attribute = this.getAttribute("value");
        document.body.style.color = attribute;
    }, false);
}
var bgChange = document.getElementsByClassName("bgChange");
for (var i = 0; i < bgChange.length; i++) {
    bgChange[i].addEventListener('click', function() {
        var attribute = this.getAttribute("value");
        document.body.style.backgroundColor = attribute;
    }, false);
}