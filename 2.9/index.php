<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-9 | Change text and background color when you press buttons.</title>
    </head>
    <body>
        <h3>Change text and background color when you press buttons.</h3>
        <label>Text:</label><br>
        <input type="button" class="txtChange" value="White">
        <input type="button" class="txtChange" value="Black">
        <input type="button" class="txtChange" value="Blue">
        <input type="button" class="txtChange" value="Red">
        <input type="button" class="txtChange" value="Green">
        <br><br>
        <label>Background:</label><br>
        <input type="button" class="bgChange" value="White">
        <input type="button" class="bgChange" value="Black">
        <input type="button" class="bgChange" value="Blue">
        <input type="button" class="bgChange" value="Red">
        <input type="button" class="bgChange" value="Green">
    </body>
    <script type="text/javascript" src="main.js"></script>
</hrml>