<?php
    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');
    $connection = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DATABASE . ";charset=utf8", USERNAME, PASSWORD);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 7-4 | Create sql that will display the OUTPUT.</title>
    </head>
    <body>
        <h3>Exercise 7-4 | Create sql that will display the OUTPUT.</h3>
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>id</th>
                    <th>parent_id</th>
                </tr>
            </thead>
            <?php
                $result = [];
                $qry = 'SELECT id,
                        CASE
                            WHEN parent_id IS NULL THEN "null"
                            ELSE parent_id
                        END AS parent_id 
                        FROM parents ORDER BY 
                        CASE 
                            WHEN parent_id IS NULL THEN id
                            ELSE parent_id
                        END';
                $stmt = $connection->prepare($qry);
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($result as $key => $row) {
                    ?>
                    <tr>
                        <td> <?= $row['id'] ?> </td>
                        <td> <?= $row['parent_id'] ?> </td>
                    </tr>
                    <?php
                }
            ?>
        </table>
    </body>
</html>