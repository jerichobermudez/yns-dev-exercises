<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-7  | Show alert when you click an image.</title>
    </head>
    <body>
        <h3>Show alert when you click an image. Alert will tell you file name of the image.</h3>
        <img src="https://cdn.shopify.com/s/files/1/0712/4751/products/BX7E-02E_High_Large_TOP_2000x.png" id="image" width="100px">
    </body>
    <script type="text/javascript" src="main.js"></script>
</hrml>