<?php
    
    session_start();

    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');

    $connection = new mysqli(HOSTNAME, USERNAME, PASSWORD, DATABASE);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 3-5 | Use database in the applications that you developed.</title>
    </head>
    <body>

        <?php

            if (isset($_POST['login'])) {
                
                $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);

                $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

                $qry = 'SELECT user_id, user_username, user_password FROM tbl_users WHERE user_username = ?';

                $stmt = $connection->prepare($qry);

                $stmt->bind_param('s', $username);

                $stmt->bind_result($id, $dbUsername, $dbPassword);

                $stmt->execute();
                
                $stmt->fetch();
                
                $stmt->close();
                
                if (password_verify($password, $dbPassword)) {

                    $_SESSION['id'] = $dbUsername;

                    $_SESSION['username'] = $dbUsername;

                } else {

                    ?> <font face="Verdana" size="2" color="red">Wrong Username / Password! Please try again.</font><br><br> <?php

                }
                
            }

            if (!isset($_SESSION['username'])) {
                
            ?>

                <form method="POST">

                    <label><b>Username: </b></label>    
                    <input type="text" name="username" required autofocus>

                    <br><br>

                    <label><b>Password: </b></label>
                    <input type="password" name="password" required>

                    <br><br>

                    <input type="submit" name="login" value="Login"> 

                </form>

            <?php

            } else {

            ?>
            
                <br>Welcome: <?= $_SESSION['username']; ?> &nbsp; <a href="signout.php"><input type="button" value="Sign Out"></a>

                <br><br>

                <a href="user_lists.php"><input type="button" value="View List"></a>

                <br><br>

                <form action="display.php" method="POST" enctype="multipart/form-data">

                    <label><b>Name: </b></label>
                    <input type="text" name="name" required autofocus>
                    
                    <br><br>

                    <label><b>Username: </b></label>
                    <input type="text" name="username" required>
                    
                    <br><br>

                    <label><b>Password: </b></label>
                    <input type="password" name="password" required>
                    
                    <br><br>

                    <label><b>Profile: </b></label>
                    <input type="file" name="profile" required>
                    
                    <br><br>

                    <input type="submit" name="submit" value="Submit">

                </form>

                <?php

            }

            ?>

    </body>
</html>