<?php
    
    session_start();

    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');

    $connection = new mysqli(HOSTNAME, USERNAME, PASSWORD, DATABASE);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-13 | Create Login Form.</title>
    </head>
    <body>
        <?php

            if (!isset($_SESSION['username'])) {

                ?> <font face="Verdana" size="2" color="red">You are no yet logged in</font><br><br> Click <a href="./">here</a> to Login <?php

            } else {

            ?>

                <a href="./"><input type="button" value="Add New"></a>
                
                <a href="signout.php"><input type="button" value="Sign Out"></a>
                
                <br><br>

                <?php

                    $qry = "SELECT user_id ,user_fname, user_username, user_profile FROM tbl_users";
                    
                    $stmt = $connection->prepare($qry);

                    $stmt->execute();

                    $stmt->store_result();

                    $recordCount = $stmt->num_rows;

                    $stmt->fetch();

                    $stmt->close();

                    $limit = 10;

                    $pages = ceil($recordCount / $limit);

                    $page = isset($_GET['page']) ? ceil($_GET['page']) : 1;

                    $offset = ($page - 1)  * $limit;

                ?>

                <table width="50%" border>
                    <tr>
                        <td colspan="3" style="padding: 5px; border: none; font-weight: bold;">
                            <center>
                                <?= ($page > 1) ? '<a href="?page=' . ($page - 1) . '">Previous</a>' : 'Previous'; ?> &nbsp; | &nbsp;
                                <?= ($page < $pages) ? '<a href="?page=' . ($page + 1) . '">Next</a>' : 'Next' ?>
                            </center>
                        </td>
                        <td style="width: 150px; padding: 5px; border: none;"> Total # of Users: <u><?= $recordCount; ?></u></td>
                    </tr>
                    <tr>
                        <td>ID</td>
                        <td>Name</td><s></s>
                        <td>Username</td>
                        <td>Profile</td>
                    </tr>
                    <?php

                        $qry = $qry . " LIMIT ?, ?";

                        $stmt = $connection->prepare($qry);

                        $stmt->bind_param('ii', $offset, $limit);

                        $stmt->bind_result($id, $name, $username, $profile);

                        $stmt->execute();

                        while ($stmt->fetch()) {

                        ?>
                        
                            <tr> 
                                <td><?= $id ?></td>
                                <td><?= $name ?></td>
                                <td><?= $username ?></td>
                                <td><img width="150" src="images/<?= $profile ?>"></td>
                            </tr>

                        <?php

                        }
                        
                    ?>
                    
                </table>

                <?php
            }
        ?>
    </body>
</html>