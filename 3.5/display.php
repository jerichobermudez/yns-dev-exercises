<?php

    session_start();

    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');

    $connection = new mysqli(HOSTNAME, USERNAME, PASSWORD, DATABASE);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 3-5 | Use database in the applications that you developed.</title>
    </head>
    <body>
        <?php

            if (!isset($_SESSION['username'])) {

                ?> <font face="Verdana" size="2" color="red">You are no yet logged in</font><br><br> Click <a href = "./">here</a> to Login <?php

            } else {

                if (isset($_POST['submit'])) {

                    $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);

                    $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);

                    $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

                    $passwordHash = password_hash($password, PASSWORD_DEFAULT);

                    $currentDir = getcwd();

                    $uploadPath = "/images/";

                    if (!file_exists($uploadPath)) { mkdir($uploadPath, 0777); }

                    $fileExtensions = ['jpeg','jpg','png'];

                    $fileName = $_FILES['profile']['name'];

                    $fileSize = $_FILES['profile']['size'];

                    $fileTmpName  = $_FILES['profile']['tmp_name'];

                    $fileType = $_FILES['profile']['type'];

                    $fileExtension = explode('.', $fileName);

                    $fileExtension = strtolower(end($fileExtension));

                    $uploadPath = $currentDir . $uploadPath . basename($fileName);


                    if (!preg_match("/^([a-zA-Z' ]+)$/", $name)){

                        ?> <font face="Verdana" size="2" color="red">Invalid Name</font><br><a href = "javascript:history.back()">go back</a> <?php

                    } else if (!preg_match("/^([a-zA-Z' ]+)$/", $username)) {

                        ?> <font face="Verdana" size="2" color="red">Invalid Username</font><br><a href = "javascript:history.back()">go back</a> <?php

                    } else if (!in_array($fileExtension, $fileExtensions)) {

                        ?> <font face="Verdana" size="2" color="red">This file extension is not allowed. Please upload a JPEG or PNG file</font><br><a href = "javascript:history.back()">go back</a> <?php

                    } else if ($fileSize > 10000000) {

                        ?> <font face="Verdana" size="2" color="red">This file is more than 10MB. Sorry, it has to be less than or equal to 10MB</font><br><a href = "javascript:history.back()">go back</a> <?php

                    } else {

                        move_uploaded_file($fileTmpName, $uploadPath);
                        
                        ?>
                        <a href = "./"><input type = "button" value = "Add New"></a>
                        <a href="user_lists.php"><input type="button" value="View List"></a>
                        <a href = "signout.php"><input type = "button" value = "Sign Out"></a>
                        <br><br><b>User Information:</b>
                        <br><br>
                        <b>Name:</b> <?= $name ?>
                        <br>
                        <b>Username:</b> <?= $username ?>
                        <br>
                        <b>Password:</b> <?= $password ?>
                        <br>
                        <b>Profile:</b> <img width = "250" src = "images/<?= $fileName ?>">

                        <?php

                            $qry = "INSERT INTO tbl_users (user_fname, user_username, user_password, user_profile) VALUES (?, ?, ?, ?)";

                            $stmt = $connection->prepare($qry);

                            $stmt->bind_param('ssss', $name, $username, $passwordHash, $fileName);

                            $stmt->execute();

                            $stmt->close();

                    }

                }

            }

        ?>
        
    </body>
</html>