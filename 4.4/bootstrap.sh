# #!/usr/bin/env bash
sudo -i

yum update -y
yum install -y yum-utils

wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
rpm -ivh mysql57-community-release-el7-9.noarch.rpm
yum -y install mysql-server