<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-5 | Input characters in text box and show it in label.</title>
    </head>
    <body>
        <h3>Input characters in text box and show it in label</h3>
        <label><b>Input Text: </b></label>
        <input type="text" id="txt">
        <br><br>
        Result: <label id="result"> - - - - - </label>
    </body>
    <script type="text/javascript" src="main.js"></script>
</html>