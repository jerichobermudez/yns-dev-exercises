<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-3 | The greatest common divisor.</title>
    </head>
    <body>
        <?php
            function gcd($num1, $num2) {
                return ($num1 % $num2) ? gcd($num2, $num1 % $num2) : $num2;
            }
            if (isset($_POST['submit'])) {
                echo "The greatest common divisor is: " . gcd($_POST['num1'], $_POST['num2']);
            }
        ?>
    </body>
</html>