<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-5 | Input date.</title>
    </head>
    <body>
        <?php
            if (isset($_POST['submit'])) {
                $date = $_POST['date'];
                echo date("D - F d, Y", strtotime('+3 days', strtotime($date)));
            }
        ?>
    </body>
</html>