<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-9 | Show the user information using table tags.</title>
    </head>
    <body>
        <a href="index.php"><input type="button" value="Add New"></a><br><br>
        <table border>
            <?php
                $file = fopen("user_info.csv", "r");
                while (($data = fgetcsv($file)) !== false) {
                    ?> <tr> <?php
                    foreach ($data as $value) {
                        ?> 
                        <td> <?= $value ?> </td>
                        <?php
                    }
                    ?> </tr> <?php
                }
                fclose($file);
            ?>
        </table>
    </body>
</html>