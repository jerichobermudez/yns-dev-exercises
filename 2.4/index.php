<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-4 | Show prime numbers.</title>
    </head>
    <body>
        <h3>Show prime numbers</h3>
        <label><b>Enter Number: </b></label>
        <input type="number" id="num1">
        <br><br>
        <input type="button" id="Calculate" value="Calculate">
        <br><br>
        <div id="result"></div>
    </body>
    <script type="text/javascript" src="main.js"></script>
</html>