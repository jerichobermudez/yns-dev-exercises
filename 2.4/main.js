document.getElementById("Calculate").addEventListener("click", function() {
    var num = Number(document.getElementById("num1").value);
    var result = '';
    for (var i = 2; i <= num; i++) {
        var notPrime = false;
        for (var j = 2; j <= i; j++) {
            if (i % j == 0 && j != i) {
                notPrime = true;
            }
        }
        if (notPrime == false) {
            result += i + '<br>';
        }
    }
    document.getElementById('result').innerHTML = 'List of all Prime Numbers from 2 to inputted number: <br>' + result;
});