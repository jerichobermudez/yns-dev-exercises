<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-7 | Add validation.</title>
    </head>
    <body>
        <?php
            if (isset($_POST['submit'])) {
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $email = $_POST['email'];
                $phone = $_POST['phone'];
                if (!preg_match("/^([a-zA-Z' ]+)$/", $fname.' '.$lname)) {
                    ?>
                    <font face="Verdana" size="2" color="red">Invalid Name</font><br><a href="javascript:history.back()">go back</a>    
                    <?php
                } else if (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) {
                    ?>
                    <font face="Verdana" size="2" color="red">Invalid Email</font><br><a href="javascript:history.back()">go back</a>
                    <?php
                } else if (!preg_match('/^[0-9]{11}+$/', $phone)) {
                    ?>
                    <font face="Verdana" size="2" color="red">Invalid Phone</font><br><a href="javascript:history.back()">go back</a>
                    <?php
                } else {
                    ?>
                    <b>User Information:</b>
                    <br><br>
                    <b>Name: </b> <?= $fname . ' ' . $lname ?>
                    <br>
                    <b>Email: </b> <?= $email ?>
                    <br>
                    <b>Phone: </b> <?= $phone ?>
                    <?php
                }
            }
        ?>
    </body>
</html>