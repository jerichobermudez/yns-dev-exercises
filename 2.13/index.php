<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-13 | Change size of images when you press buttons.</title>
    </head>
    <body>
        <h3>Change size of images when you press buttons.</h3>
        <input type="button" class="resizeImage" size="250px" value="Small">
        <input type="button" class="resizeImage" size="500px" value="Medium">
        <input type="button" class="resizeImage" size="750px" value="Large">
        <br><br><br>
        <img src="https://cdn.shopify.com/s/files/1/0712/4751/products/BX7E-02E_High_Large_TOP_2000x.png" id="image" width="250px">
    </body>
    <script type="text/javascript" src="main.js"></script>
</hrml>