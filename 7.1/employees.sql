-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2020 at 07:44 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testing_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employee_id` int(11) NOT NULL,
  `first_name` varchar(65) NOT NULL,
  `last_name` varchar(65) NOT NULL,
  `middle_name` varchar(65) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `department_id` int(11) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `boss_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES
(1, 'Manabu', 'Yamazaki', NULL, '1976-03-17', 1, NULL, NULL),
(2, 'Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1),
(3, 'Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-04-01', 1),
(4, 'Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1),
(5, 'Lorraine ', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
(6, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
(7, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
(8, 'Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
(9, 'Hideshi', 'Ogoshi', NULL, '1983-07-15', 4, '2014-06-01', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
