<?php
    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');
    $connection = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DATABASE . ";charset=utf8", USERNAME, PASSWORD);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 7-1 | Selecting by age using birth date.</title>
    </head>
    <body>
        <h3>Exercise 7-1 | Selecting by age using birth date.</h3>
        <p> Get employees who is older than 30 years old but younger than 40 years old. </p>
        <table border cellpadding="5">
            <thead>
                <th>Employee Name</th>
                <th>Birthday</th>
                <th>Age</th>
            </thead>
            <tbody>
                <?php
                    $result = [];
                    $qry = "SELECT CONCAT(first_name, ' ', last_name) AS full_name, birth_date, YEAR(CURDATE()) - YEAR(birth_date) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(birth_date), '-', DAY(birth_date)) ,'%Y-%c-%e') > CURDATE(), 1, 0) AS age FROM employees WHERE birth_date <= NOW() - INTERVAL 30 YEAR AND birth_date >= NOW() - INTERVAL 40 YEAR";
                    $stmt = $connection->prepare($qry);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($result as $key => $row) {
                        ?>
                        <tr>
                            <td> <?= $row['full_name'] ?> </td>
                            <td> <?= $row['birth_date'] ?> </td>
                            <td> <?= $row['age'] ?> </td>
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </body>
</html>