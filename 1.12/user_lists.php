<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-12 | Add pagination in the list page.</title>
    </head>
    <body>
        <a href="./"><input type="button" value="Add New"></a><br><br>
        <?php
            $file = fopen('user_info.csv', 'r');
            $count = count(file('user_info.csv'));
            $limit = 10;
            $pages = $count / $limit;
            $page = isset($_GET['page']) ? $_GET['page'] : 1;
        ?>
        <table width="50%" border>
            <tr>
                <td colspan="3" style="padding: 5px; border: none; font-weight: bold;">
                    <center>
                        <?= ($page > 1) ? '<a href="?page=' . ($page - 1) . '">Previous</a>' : 'Previous'; ?> &nbsp; | &nbsp;
                        <?= ($page < $pages) ? '<a href="?page=' . ($page + 1) . '">Next</a>' : 'Next' ?>
                    </center>
                </td>
                <td style="padding: 5px; border: none;"> Total # of Users: <u><?= $count ?></u></td>
            </tr>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Profile</td>
            </tr>
            <?php
                $offset = ($page - 1)  * $limit;
                for ($i = 0; $i < $offset; $i++) {
                    fgets($file);
                }
                for ($i=0; $i < $limit; $i++) {
                    $data = explode(', ', fgets($file));
                    if (count($data) !== 1) {
                        ?>
                        <tr>
                            <td> <?= $data[0] ?> </td>
                            <td> <?= $data[1] ?> </td>
                            <td> <?= $data[2] ?> </td>
                            <td><img width="150" src="images/<?= trim($data[3]) ?>"></td>
                        </tr>
                        <?php
                    }
                }
                fclose($file);
            ?>
        </table>
    </body>
</html>