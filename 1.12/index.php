<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-12 | Add pagination in the list page.</title>
    </head>
    <body>
        <form action="display.php" method="POST" enctype="multipart/form-data">
            <a href="user_lists.php"><input type="button" value="View List"></a>
            <br><br>
            <label><b>Firstname: </b></label>
            <input type="text" name="fname" required autofocus>
            <br><br>
            <label><b>Lastname: </b></label>
            <input type="text" name="lname" required>
            <br><br>
            <label><b>Email: </b></label>
            <input type="text" name="email" required>
            <br><br>
            <label><b>Phone: </b></label>
            <input type="text" name="phone" required>
            <br><br>
            <label><b>Profile: </b></label>
            <input type="file" name="profile" required>
            <br><br>
            <input type="submit" name="submit" value="Submit">
        </form>
    </body>
</html>