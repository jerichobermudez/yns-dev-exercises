<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-3 | The four basic operations of arithmetic.</title>
    </head>
    <body>
        <h3>Calculator</h3>
        <label><b>Num 1: </b></label>
        <input type="number" id="num1">
        <br><br>
        <label><b>Num 2: </b></label>
        <input type="number" id="num2">
        <br><br>
        <label><b>Operation </b></label>
        <select id="operation">
            <option value=""> Choose Operation </option>
            <option value="+"> Addittion </option>
            <option value="-"> Subtraction </option>
            <option value="*"> Multiplication </option>
            <option value="/"> Division </option>
        </select>
        <br><br>
        <input type="button" id="Calculate" value="Calculate">
        <br><br>
        <div id="result"></div>
    </body>
    <script type="text/javascript" src="main.js"></script>
</html>