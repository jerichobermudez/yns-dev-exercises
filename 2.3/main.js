document.getElementById("Calculate").addEventListener("click", function() {
    num1 = Number(document.getElementById("num1").value);
    num2 = Number(document.getElementById("num2").value);
    operation = document.getElementById("operation").value;
    switch(operation) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            result = "Something went wrong";
    }
    document.getElementById('result').innerHTML = "Result: " + num1 + ' ' + operation + ' ' + num2 + ' = ' + result;
});