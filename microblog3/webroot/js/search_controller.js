$(document).on('click', '.view-user-profile', function() {
    localStorage.setItem('profileId', parseInt($(this).data('id')));
    localStorage.setItem('profileUrl', $(this).data('value'));
});

$(document).on('click', '.follow-unfollow', function() {
    var id = $(this).data('id');
    var url = $(this).data('value');
    $(this).addClass('disabled');
    $.ajax({
        type: 'ajax',
        url: $('body').attr('root_directory') + 'followers/follow-unfollow/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#follow' + id).html('...');
        },
        success: function(result) {
            if (result.status === 'unfollow') {
                $('#follow' + id).html('Follow').blur();
            } else {
                $('#follow' + id).html('Unfollow').blur();
            }
            $('#follow' + id).removeClass('disabled');
        }
    });
});