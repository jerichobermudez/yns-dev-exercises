$(document).on('click', '.view-user-profile', function() {
    localStorage.setItem('profileId', parseInt($(this).data('id')));

});
var root_directory = $('body').attr('root_directory');

function divLoader() {
    return '<div class="col-lg-12 col-xs-12"><div class="box box-widget flat text-center"><img src="' + root_directory + 'webroot/img/hacker-preloader.gif" width="100px"></div></div>';
}
function loadProfilePost() {
    var profileId = localStorage.getItem('profileId') ? localStorage.getItem('profileId') : '';
    $.ajax({
        url:  root_directory + 'users/profile-posts/' + profileId,
        beforeSend: function () {
            $("#loadProfilePosts").html(divLoader());
        },
        success: function(result) {
            $("#loadProfilePosts").html(result);
        }
    });
}

loadProfilePost();

$(document).on('click', '.edit-post', function() {
    $("#editPostModal").modal();
    $.ajax({
        type: 'ajax',
        url:  root_directory + 'posts/edit-post/' + $(this).data('id'),
        beforeSend: function () {
            $("#editPostContent").html(divLoader());
        },
        success: function(result) {
            $("#editPostContent").html(result);
        }
    });
});

$(document).on('submit', "#editPostForm", function(e) {
    e.preventDefault();
    var formData = new FormData($('#editPostForm')[0]);
    formData.append("image", $("input[type=file]")[0].files[0]);
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url:  root_directory + url,
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#editPostModal").modal('hide');
                loadProfilePost();
            }
        }
    });

});

$(document).on('click', '.delete-post', function() {
    $("#deletePostModal").modal();
    $.ajax({
        type: 'ajax',
        url:  root_directory + 'posts/delete_post/' + $(this).data('id'),
        beforeSend: function () {
            $("#deletePostContent").html(divLoader());
        },
        success: function(result) {
            $("#deletePostContent").html(result);
        }
    });
});


$(document).on('submit', "#removePostForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url:  root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#deletePostModal").modal('hide');
                loadProfilePost(result.data);
            }
        }
    });

});

$(document).on('click', '.like-unlike', function() {
    var id = $(this).data('id');
    var url = $(this).data('value');
    $.ajax({
        type: 'ajax',
        url: root_directory + 'likes/like_unlike/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#like' + id).html('...');
        },
        success: function(result) {
            if (result.status === 'unliked') {
                $('#like' + id).removeClass('active').html('<i class="glyphicon glyphicon-thumbs-up"></i> Like').attr('data-original-title', 'Like').blur();
            } else {
                $('#like' + id).addClass('active').html('<i class="glyphicon glyphicon-thumbs-down"></i> Unlike').attr('data-original-title', 'Unlike').blur();
            }
        }
    });
});

function loadComment(id) {
    $.ajax({
        url: root_directory + 'comments/post_comment/' + id,
        beforeSend: function () {
            $("#commentPostContent").html(divLoader());
        },
        success: function(result) {
            $("#commentPostContent").html(result);
        }
    });
}

$(document).on('click', '.post-comment', function() {
    $("#commentPostModal").modal();
    loadComment($(this).data('id'));
});


$(document).on('keypress', '.comment-to-post', function(event) {
    if (event.keyCode == 13) {
        $('#addCommentForm').submit();
        return false;
    }
});

$(document).on('submit', "#addCommentForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url:  root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                loadComment(result.data);
            }
        }
    });

});

$(document).on('click', '.edit-post-comment', function() {
    $("#editCommentModal").modal();
    $.ajax({
        url: root_directory + 'comments/edit_comment/' + $(this).data('id'),
        beforeSend: function () {
            $("#editCommentContent").html(divLoader());
        },
        success: function(result) {
            $("#editCommentContent").html(result);
        }
    });
});

$(document).on('submit', "#editCommentForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url:  root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#editCommentModal").modal('hide');
                loadComment(result.data);
            }
        }
    });
    
});

$(document).on('click', '.delete-post-comment', function() {
    $("#deleteCommentModal").modal();
    $.ajax({
        type: 'ajax',
        url: root_directory + 'comments/delete_comment/' + $(this).data('id'),
        success: function(result) {
            $("#deleteCommentContent").html(result);
        }
    });
});

$(document).on('submit', "#removeCommentForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url: root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#deleteCommentModal").modal('hide');
                loadComment(result.data);
            }
        }
    });

});

$(document).on('click', '.retweet-post', function() {
    $("#retweetModal").modal();
    $.ajax({
        type: 'ajax',
        url: root_directory + 'posts/retweet_post/' + $(this).data('id'),
        beforeSend: function () {
            $("#retweetContent").html(divLoader());
        },
        success: function(result) {
            $("#retweetContent").html(result);
        }
    });
})

$(document).on('submit', "#retweetPostForm", function(e) {
    e.preventDefault();
    var formData = new FormData($('#retweetPostForm')[0]);
    formData.append("image", $("input[type=file]")[0].files[0]);
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url: root_directory + url,
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#retweetModal").modal('hide');
                loadProfilePost();
            }
        }
    });

});

$(document).on('click', '.follow-unfollow', function() {
    var id = $(this).data('id');
    $(this).addClass('disabled');
    $.ajax({
        type: 'ajax',
        url: root_directory + 'followers/follow-unfollow/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#follow' + id).html('...');
        },
        success: function(result) {
            loadModal('followers/following', 'following');
            if (result.status === 'unfollow') {
                $('#follow' + id).html('Follow').blur();
            } else {
                $('#follow' + id).html('Unfollow').blur();
            }
            $('#follow' + id).removeClass('disabled');
        }
    });
});

function loadModal(url, modal) {
    $.ajax({
        type: 'ajax',
        url: root_directory + url,
        beforeSend: function () {
            $('#' + modal + 'Content').html(divLoader());
        },
        success: function(result) {
            $('#' + modal + 'Content').html(result);
        }
    });
}

$(document).on('click', '.user-followers', function() {
    $("#followersModal").modal();
    loadModal('followers/followers', 'followers');
});

$(document).on('click', '.user-following', function() {
    $("#followingModal").modal();
    loadModal('followers/following', 'following');
});

$(document).on('click', '.edit-profile', function() {
    $("#editProfileModal").modal();
    loadModal('users/edit-profile', 'editProfile');
});

$(document).on('submit', '#updateProfileForm', function(e) {
    e.preventDefault();
    var formData = new FormData($('#updateProfileForm')[0]);
    formData.append("profile", $("input[type=file]")[0].files[0]);
    $.ajax({
        type: 'post',
        url:  root_directory + 'users/update-photo',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === "success") {
                $.jGrowl(result.message, { header: 'SUCCESS!', position: 'top-right' });
                setTimeout(function(){ window.location.reload(); }, 900)
            } else {
                $.jGrowl(result.message, { header: 'ERROR!', position: 'top-right' });
            }
        }
    });
});

$(document).on('submit', '#updateInformationForm', function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        type: 'post',
        url:  root_directory + 'users/update-info',
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === "success") {
                $.jGrowl(result.message, { header: 'SUCCESS!', position: 'top-right' });
                setTimeout(function(){ window.location.reload(); }, 900)
            } else {
                $.jGrowl(result.message, { header: 'ERROR!', position: 'top-right' });
            }
        }
    });
});

$(document).on('submit', '#updatePasswordForm', function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        type: 'post',
        url:  root_directory + 'users/update-password',
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === "success") {
                $.jGrowl(result.message, { header: 'SUCCESS!', position: 'top-right' });
                setTimeout(function(){ window.location.reload(); }, 900)
            } else {
                $.jGrowl(result.message, { header: 'ERROR!', position: 'top-right' });
            }
        }
    });
});

$(document).on('click', '.show-more-result', function() {
    $(this).html('Loading...');
    setTimeout(function () {
        loadProfilePost();
    }, 600);
});