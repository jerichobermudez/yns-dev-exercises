var root_directory = $('body').attr('root_directory');

function divLoader() {
    return '<div class="col-lg-12 col-xs-12"><div class="box box-widget flat"><img src="' + root_directory + 'webroot/img/hacker-preloader.gif" width="100px"></div></div>';
}

function loadPost(limit = 5) {
    $.ajax({
        url:  root_directory + 'posts/post_content/' + limit,
        beforeSend: function () {
            $("#loadPosts").html(divLoader());
        },
        success: function(result) {
            $("#loadPosts").html(result).fadeIn();
        }
    });
}

loadPost();

function submitForm(formID) {
    $(document).on('submit', formID, function(e) {
        e.preventDefault();
        var formData = new FormData($(formID)[0]);
        formData.append("image", $("input[type=file]")[0].files[0]);
        var url = $(this).attr('value');
        $.ajax({
            type: 'post',
            url:  root_directory + url,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },
            success: function(result) {
                $('.callout').remove();
                flashMessage(formID, result.message, result.flash);
                if (result.status === 'success') {
                    $(formID)[0].reset();
                    loadPost();
                    $('#addEditPreview, input[type=file]').attr('src', '');
                    setTimeout(function () {
                        $('.modal').modal('hide')
                    }, 900);
                    $('input, textarea').css('border', '1px solid #d2d6de');
                } else {
                    $.each(result.data, function(model, errors) {
                        $(formID + " #" + model).css('border', '1px solid red');
                        $.each(errors, function(index, value) {
                            $.jGrowl(value, { header: index.toUpperCase(), position: 'top-right' });
                        });
                    });
                }
            }
        });
    });

}

function flashMessage(form, message, flash) {
    var createElement = $(document.createElement('div'));
    createElement.addClass('callout callout-' + flash).text(message);
    createElement.insertAfter($(form + ' #flashMessage')).fadeOut().fadeIn();
}

submitForm('#addPostForm');

submitForm('#editPostForm');

submitForm('#removePostForm');

$(document).on('click', '.edit-post', function() {
    $("#editPostModal").modal();
    $.ajax({
        type: 'ajax',
        url:  root_directory + 'posts/edit_post/' + $(this).data('id'),
        beforeSend: function () {
            $("#editPostContent").html(divLoader());
        },
        success: function(result) {
            $("#editPostContent").html(result);
        }
    });
});

$(document).on('click', '.delete-post', function() {
    $("#deletePostModal").modal();
    $.ajax({
        type: 'ajax',
        url:  root_directory + 'posts/delete_post/' + $(this).data('id'),
        beforeSend: function () {
            $("#deletePostContent").html(divLoader());
        },
        success: function(result) {
            $("#deletePostContent").html(result);
        }
    });
});

function loadComment(id) {
    $.ajax({
        url: root_directory + 'comments/post_comment/' + id,
        beforeSend: function () {
            $("#commentPostContent").html(divLoader());
        },
        success: function(result) {
            $("#commentPostContent").html(result);
        }
    });
}

$(document).on('click', '.post-comment', function() {
    $("#commentPostModal").modal();
    loadComment($(this).data('id'));
});

$(document).on('click', '.edit-post-comment', function() {
    $("#editCommentModal").modal();
    $.ajax({
        url: root_directory + 'comments/edit_comment/' + $(this).data('id'),
        beforeSend: function () {
            $("#editCommentContent").html(divLoader());
        },
        success: function(result) {
            $("#editCommentContent").html(result);
        }
    });
});

$(document).on('click', '.delete-post-comment', function() {
    $("#deleteCommentModal").modal();
    $.ajax({
        type: 'ajax',
        url: root_directory + 'comments/delete_comment/' + $(this).data('id'),
        success: function(result) {
            $("#deleteCommentContent").html(result);
        }
    });
});

$(document).on('click', '.retweet-post', function() {
    $("#retweetModal").modal();
    $.ajax({
        type: 'ajax',
        url: root_directory + 'posts/retweet_post/' + $(this).data('id'),
        beforeSend: function () {
            $("#retweetContent").html(divLoader());
        },
        success: function(result) {
            $("#retweetContent").html(result);
        }
    });
})

$(document).on('keypress', '.comment-to-post', function(event) {
    if (event.keyCode == 13) {
        $('#addCommentForm').submit();
        return false;
    }
});

$(document).on('submit', "#addCommentForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url: root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                loadComment(result.data);
            }
        }
    });

});

$(document).on('submit', "#editCommentForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url: root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#editCommentModal").modal('hide');
                loadComment(result.data);
            }
        }
    });
    
});

$(document).on('submit', "#removeCommentForm", function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url: root_directory + url,
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#deleteCommentModal").modal('hide');
                loadComment(result.data);
            }
        }
    });

});

$(document).on('submit', "#retweetPostForm", function(e) {
    e.preventDefault();
    var formData = new FormData($('#retweetPostForm')[0]);
    formData.append("image", $("input[type=file]")[0].files[0]);
    var url = $(this).attr('value');
    $.ajax({
        type: 'post',
        url:  root_directory + url,
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            if (result.status === 'success') {
                $("#retweetModal").modal('hide');
                loadPost();
            }
        }
    });

});

$(document).on('click', '.like-unlike', function() {
    var id = $(this).data('id');
    $.ajax({
        type: 'ajax',
        url: root_directory + 'likes/like_unlike/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#like' + id).html('...');
        },
        success: function(result) {
            if (result.status === 'unliked') {
                $('#like' + id).removeClass('active').html('<i class="glyphicon glyphicon-thumbs-up"></i> Like').attr('data-original-title', 'Like').blur();
            } else {
                $('#like' + id).addClass('active').html('<i class="glyphicon glyphicon-thumbs-down"></i> Unlike').attr('data-original-title', 'Unlike').blur();
            }
        }
    });
});

$(document).on('click', '.view-user-profile', function() {
    localStorage.setItem('profileId', parseInt($(this).data('id')));

});

$(document).on('keypress', '.edit-comment', function(event) {
    if (event.keyCode == 13) {
        $('#editCommentForm').submit();
        return false;
    }
});

sessionStorage.setItem('limit', 5);

$(document).on('click', '.show-more-result', function() {
    var limit = parseInt(sessionStorage.getItem('limit')) + 5;
    sessionStorage.setItem('limit', limit);
    $(this).html('Loading...');
    setTimeout(function () {
        loadPost(limit);
    }, 600);
});