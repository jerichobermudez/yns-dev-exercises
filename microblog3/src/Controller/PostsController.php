<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    public function initialize() {
        parent::initialize();

        $this->loadModel('Posts');
        $this->loadModel('Followers');
        $this->loadModel('Users');
        $this->loadModel('Likes');
        $this->loadModel('Comments');

        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
    }

    /**
     * Allow pages method
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // $this->Auth->allow(['createPost', 'editPost']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
    }

    /**
     * postContent method
     *
     * @param int $limit.
     * @return \Cake\Http\Response|null
     */
    public function postContent($limit)
    {   
        if ($this->request->is('ajax')) {
            $followingIds = $this->Followers->find('list', ['valueField' => 'following_id'])
                ->select(['following_id'])
                ->where(['user_id' => $this->Auth->user('id')])
                ->toArray();

            $userIds = array_merge($followingIds, [$this->Auth->user('id')]);

            $posts = $this->Posts->find('all')
                ->where([
                    'Posts.user_id IN' => $userIds,
                    'Posts.deleted !=' => 1
                ])
                ->contain(['User', 'Original', 'likes'])
                ->limit($limit)
                ->order(['Posts.created' => 'DESC'])->toArray();

            $this->set('posts', $posts);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function createPost()
    {
        $this->autoRender = false;
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $post = $this->Posts->patchEntity($post, $data);

            $postsTable = TableRegistry::getTableLocator()->get('posts');
            $postResult = $postsTable->newEntity();
            $posts = $this->Posts->patchEntity($postResult, $data);

            $filename = null;
            if (!empty($data['image']['tmp_name'])) {
                $filename= basename($data['image']['name']);
                move_uploaded_file($data['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'posts' . DS . $filename);
            }

            $postResult->user_id = $this->Auth->user('id');
            $postResult->title = h($data['title']);
            $postResult->content = h($data['content']);
            $postResult->image = $filename;
            $postResult->deleted_date = null;
            
            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $post->errors()
            ];
            if (!$post->errors()) {
                if ($postsTable->save($postResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Post successfuly created',
                        'flash' => 'success',
                        'data' => 'success'
                    ];
                }
            }
        }
        echo json_encode($result);
    }

    /** 
     * Check if user is authorized to edit post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function editPost($id)
    {
        if ($this->request->is('ajax')) {
            $post = $this->Posts->findByIdAndDeleted($id, 0)->first();
            $this->set('post', $post);
        }
    }

    /** 
     * Ajax update post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function updatePost($id)
    {
        $this->autoRender = false;
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $post = $this->Posts->patchEntity($post, $data);
            
            $postsTable = TableRegistry::getTableLocator()->get('posts');
            $postResult = $postsTable->findById($id)->first();

            $postResult->title = h($data['title']);
            $postResult->content = h($data['content']);
            if (!empty($data['image']['tmp_name'])) {
                $filename = basename($data['image']['name']);
                move_uploaded_file($data['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'posts' . DS . $filename);
                $postResult->image = $filename;
            }
            
            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $post->errors()
            ];
            if (!$post->errors()) {
                if ($postsTable->save($postResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Post successfuly updated',
                        'flash' => 'success',
                        'data' => 'success'
                    ];
                }
            }
            
            echo json_encode($result);
        }
    }

    /** 
     * Check if user is authorized to remove post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function deletePost($id)
    {
        if ($this->request->is('ajax')) {
            $post = $this->Posts->findByIdAndDeleted($id, 0)->first();
            $this->set('post', $post);
        }
    }

    /** 
     * Ajax remove post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function removePost($id)
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            
            $postsTable = TableRegistry::getTableLocator()->get('posts');
            $postResult = $postsTable->findById($id)->first();

            $postResult->deleted = 1;
            $postResult->deleted_date = date('Y-m-d H:i:s');

            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => 'error'
            ];
            if ($postsTable->save($postResult)) {
                $result = [
                    'status' => 'success',
                    'message' => 'Post successfuly remove',
                    'flash' => 'success',
                    'data' => 'success'
                ];
            }
        }

        echo json_encode($result);
    }

    /** 
     * Select Retweet post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function retweetPost($id)
    {
        if ($this->request->is('ajax')) {
            $post = $this->Posts->findById($id)->contain(['user'])->first();
            $this->set('post', $post);
        }
    }

    /** 
     * Ajax retweet post Method
     * 
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function postRetweet()
    {
        $this->autoRender = false;
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $post = $this->Posts->patchEntity($post, $data);

            $postsTable = TableRegistry::getTableLocator()->get('posts');
            $postResult = $postsTable->newEntity();

            $filename = null;
            if (!empty($data['image']['tmp_name'])) {
                $filename= basename($data['image']['name']);
                move_uploaded_file($data['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'posts' . DS . $filename);
            }

            $postResult->user_id = $this->Auth->user('id');
            $postResult->title = h($data['title']);
            $postResult->content = h($data['content']);
            $postResult->is_retweet = 1;
            $postResult->original_post_id = $data['id'];
            $postResult->image = $filename;
            $postResult->deleted_date = null;
            
            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $post->errors()
            ];
            if (!$post->errors()) {
                if ($postsTable->save($postResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Post successfuly created',
                        'flash' => 'success',
                        'data' => 'success'
                    ];
                }
            }
        }
        echo json_encode($result);
    }

    /** 
     * Ajax search Method
     * 
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function search()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $search = h($data['search']);

            $users = $this->Users->find('all')
                ->where([
                    'deleted !=' => 1,
                    'activated !=' => 0,
                    'OR' => [
                        'first_name LIKE' => "%$search%",
                        'last_name LIKE' => "%$search%",
                        'username LIKE' => "%$search%"
                    ]
                ])
                ->contain(['followers'])
                ->order(['username' => 'ASC'])
                ->toArray();

            $posts = $this->Posts->find('all')
                ->where([
                    'deleted !=' => 1,
                    'OR' => [
                        'title LIKE' => "%$search%",
                        'content LIKE' => "%$search%"
                    ]
                ])
                ->order(['id' => 'DESC'])
                ->toArray();

            $this->set(['search' => $search, 'users' => ($users), 'posts' => ($posts)]);
        }
    }
}
