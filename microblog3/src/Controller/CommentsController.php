<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{

    public function postComment($id)
    {
        if ($this->request->is('ajax')) {
            $comments = $this->Comments->find('all')
                ->where(['Comments.post_id' => $id, 'Comments.deleted !=' => 1])
                ->contain(['users', 'posts'])
                ->order(['Comments.id' => 'ASC']);
                
            $this->set(['id' => $id, 'comments' => ($comments)]);
        }
    }

    public function addComment()
    {
        $this->autoRender = false;
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $comment = $this->Comments->patchEntity($comment, $data);

            $commentsTable = TableRegistry::getTableLocator()->get('comments');
            $commentResult = $commentsTable->newEntity();
            $comments = $this->Comments->patchEntity($commentResult, $data);

            $commentResult->user_id = $this->Auth->user('id');
            $commentResult->post_id = $data['post_id'];
            $commentResult->comment = h($data['comment']);
            $commentResult->deleted_date = null;

            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $comment->errors()
            ];
            if (!$comment->errors()) {
                if ($commentsTable->save($commentResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Comment successfuly created',
                        'flash' => 'success',
                        'data' => $data['post_id'],
                        'url' => Router::url(['controller' => 'comments', 'action' => 'post_comment'])
                    ];
                }
            }
        }
        echo json_encode($result);
    }

    /** 
     * Check if user is authorized to edit comment Method
     * 
     * @param string|null $id Comment id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function editComment($id)
    {
        if ($this->request->is('ajax')) {
            $comment = $this->Comments->findById($id)->first();
            $this->set('comment', $comment);
        }
    }

    /** 
     * Ajax update comment Method
     * 
     * @param string|null $id Comment id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function updateComment($id)
    {
        $this->autoRender = false;
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $comment = $this->Comments->patchEntity($comment, $data);
            
            $commentsTable = TableRegistry::getTableLocator()->get('comments');
            $commentResult = $commentsTable->findById($id)->first();
            
            $commentResult->comment = $data['comment'];

            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $comment->errors()
            ];
            if (!$comment->errors()) {
                if ($commentsTable->save($commentResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Comment successfuly created',
                        'flash' => 'success',
                        'data' => $commentResult->post_id,
                        'url' => Router::url(['controller' => 'comments', 'action' => 'post_comment'])
                    ];
                }
            }
        }

        echo json_encode($result);
    }

    /** 
     * Check if user is authorized to remove post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function deleteComment($id)
    {
        if ($this->request->is('ajax')) {
            $comment = $this->Comments->findByIdAndDeleted($id, 0)->first();
            $this->set('comment', $comment);
        }
    }

    /** 
     * Ajax remove post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function removeComment($id)
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            
            $commentsTable = TableRegistry::getTableLocator()->get('Comments');
            $commentResult = $commentsTable->findById($id)->first();

            $commentResult->deleted = 1;
            $commentResult->deleted_date = date('Y-m-d H:i:s');

            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => 'error'
            ];
            if ($commentsTable->save($commentResult)) {
                $result = [
                    'status' => 'success',
                    'message' => 'Comment successfuly remove',
                    'flash' => 'success',
                    'data' => $commentResult->post_id,
                    'url' => Router::url(['controller' => 'comments', 'action' => 'post_comment'])
                ];
            }
        }

        echo json_encode($result);
    }
}
