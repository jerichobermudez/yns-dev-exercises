<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('Followers');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
    }

    /**
     * Allow pages method
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['signup', 'forgot', 'activate', 'view']);
    }

    /**
     * Signin method
     *
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['activated'] === 1) {
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'posts', 'action' => '/']);
                } else {
                    $this->Flash->warning('Please Activate your Account First! Check your email to activate your account');
                }
            } else {
                $this->Flash->error('Invalid Login Credentials');
            }
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        }
    }

    /**
     * Signup method
     *
     * @return \Cake\Http\Response|null
     */
    public function signup()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);

            $usersTable = TableRegistry::getTableLocator()->get('users');
            $result = $usersTable->newEntity();
            $users = $this->Users->patchEntity($result, $data);

            $activationCode = 'ABCDEFGHIJKLMNOPQRSYUVWXYZ1234567890';
            $activationCode = substr(str_shuffle($activationCode), 0, 8);
            $result->activation_code = $activationCode;
            if ($usersTable->save($result)) {
                $folder = new Folder();
                $path = WWW_ROOT . DS . 'users/';
                $userDir = $path . $data['username'] . DS;
                if (!file_exists($folder->create($path))) { $folder->create($path); }
                if (!file_exists($folder->create($userDir))) { $folder->create($userDir); }
                if (!file_exists($folder->create($userDir . 'posts'))) { $folder->create($userDir . 'posts'); }
                if (!file_exists($folder->create($userDir . 'profile'))) {
                    $folder->create($userDir . 'profile');
                    $file = new File(WWW_ROOT . DS . 'img/default.png');
                    if ($file->exists()) {
                        $dir = new Folder($userDir . 'profile', true);
                        $file->copy($dir->path . DS . $file->name);
                    }
                }
                $message = 'Hi ' . $data['first_name'] . ", \n\nYou recently register to Microblog 3.\nActivation Code: " . $activationCode . ". Click the link below to Activate your account\n\nLink: http://" . $_SERVER['HTTP_HOST'] . Router::url(['controller' => 'users', 'action' => 'activate']) . "\n\nThanks";
                $email = new Email('default');
                $email->setTransport('gmail')
                      ->setFrom(['yns@microblog.com' => 'MicroBlog 3'])
                      ->setTo($data['email'])
                      ->setSubject('Activate Account')
                      ->send($message);
                $this->Flash->success(__('Registration Success! Please check your email to activate your account'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('Something went wrong. Please try again'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Activate Account method
     *
     * @return \Cake\Http\Response|null
     */
    public function activate()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            $usersTable = TableRegistry::getTableLocator()->get('users');
            $result = $usersTable->findByActivationCode($data['activation_code'])->first();
            if ($result) {
                $result->activated = 1;
                $result->activation_code = null;
                $usersTable->save($result);
                $this->Flash->success(__('Account Activation Success. You may now login your account'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('Something went wrong. Please check your email for your Activation Code and try again'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Forgot Password method
     *
     * @return \Cake\Http\Response|null
     */
    public function forgot() {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $usersTable = TableRegistry::getTableLocator()->get('users');
            $result = $usersTable->findByEmail($data['email'])->first();
            if ($result) {
                $str = 'ABCDEFGHIJKLMNOPQRSYUVWXYZ1234567890';
                $newPassword = substr(str_shuffle($str), 0, 8);
                $result->password = $newPassword;
                $usersTable->save($result);
                $message = "You've recently request for new password to your Microblog Account.\nNew Password: " . $newPassword . ".\n\nClick the link below to Login your Account\n\nLink: http://". $_SERVER['HTTP_HOST'] . Router::url(['controller' => 'users', 'action' => 'login']) ."\n\nThanks";
                $email = new Email('default');
                $email->setTransport('gmail')
                      ->setFrom(['yns@microblog.com' => 'MicroBlog 3'])
                      ->setTo($data['email'])
                      ->setSubject('Forgot Password')
                      ->send($message);
                $this->Flash->success(__('Password Request Success! Please check your email for your New Password'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('Something went wrong. Please try again'));
            }
        }
    }

    /**
     * Logout method
     *
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    /**
     * getUser method
     *
     * @return \Cake\Http\Response|null
     */
    public function getUser($id = null)
    {
        $userID = $id ? $id : $this->Auth->user('id');
        $data = $this->Users->findById($userID, 0)->first();
        $this->response->body($data);
        
        return $this->response;
    }

    /**
     * Profile method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profile($id = null)
    {
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        
        $id = $id ? $id : $this->Auth->user('id');

        $user = $this->Users->get($id, [
            'where' => ['followers.following_id' => $this->Auth->user('id')],
            'contain' => ['followers']
        ]);
        $this->set('user', $user);
    }

    /**
     * Profile Posts method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profilePosts($id = null)
    {
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        
        if ($this->request->is('ajax')) {
            $id = $id ? $id : $this->Auth->user('id');
            $posts = $this->Posts->find('all')
                ->where([
                    'Posts.deleted !=' => 1,
                    'Posts.user_id' => $id
                ])
                ->contain(['User', 'Original', 'likes'])
                ->order(['Posts.created' => 'DESC']);
                
            $this->set('posts', $this->paginate($posts));
        }
    }

    /**
     * Get Edit Profile method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editProfile()
    {
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        
        $user = $this->Users->findById($this->Auth->user('id'))->first();
        $this->set('user', $user);
    }

    /**
     * Update Image Profile method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updatePhoto()
    {
        $this->autoRender = false;
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            
            $usersTable = TableRegistry::getTableLocator()->get('users');
            $userResult = $usersTable->findById($this->Auth->user('id'))->first();

            $filename = null;
            if (!empty($data['profile']['tmp_name'])) {
                $filename= basename($data['profile']['name']);
                move_uploaded_file($data['profile']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'profile' . DS . $filename);
            }

            $userResult->profile = $filename;
            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $user->errors()
            ];
            if (!$user->errors()) {
                if ($usersTable->save($userResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Profile successfuly updated',
                        'flash' => 'success',
                        'data' => 'success'
                    ];
                }
            }
            echo json_encode($result);
        }
    }

    /**
     * Update Information method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updateInfo()
    {
        $this->autoRender = false;
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            // $user = $this->Users->patchEntity($user, $data);
            
            $usersTable = TableRegistry::getTableLocator()->get('users');
            $userResult = $usersTable->findById($this->Auth->user('id'))->first();

            $userResult->first_name = h($data['first_name']);
            $userResult->last_name = h($data['last_name']);
            $userResult->username = $userResult->username;
            $userResult->email = h($data['email']);

            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $user->errors()
            ];
            if (!$user->errors()) {
                if ($usersTable->save($userResult)) {
                    $result = [
                        'status' => 'success',
                        'message' => 'Profile Information successfuly updated',
                        'flash' => 'success',
                        'data' => 'success'
                    ];
                }
            }
            echo json_encode($result);
        }
    }

    /**
     * Update Password method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updatePassword()
    {
        $this->autoRender = false;
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            
            $usersTable = TableRegistry::getTableLocator()->get('users');
            $userResult = $usersTable->findById($this->Auth->user('id'))->first();
            
            $result = [
                'status' => 'error',
                'message' => 'Something went wrong! Please try again',
                'flash' => 'danger',
                'data' => $user->errors()
            ];
            if (!password_verify($data['old_password'], $userResult->password)) {
                $result = [
                    'status' => 'error',
                    'message' => 'Old Password not match!. Please try again.',
                ];
            } else if ($data['new_password'] !== $data['retype_new_password']) {
                $result = [
                    'status' => 'error',
                    'message' => 'New Password not match!. Please try again.',
                ];
            } else {
                $userResult->password = $data['new_password'];
                $usersTable->save($userResult);
                $result = [
                    'status' => 'success',
                    'message' => 'Password successfuly changed',
                    'flash' => 'success',
                    'data' => 'success'
                ];
            }
            
            echo json_encode($result);
        }
    }
}
