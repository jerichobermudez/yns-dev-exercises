<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    /**
     * Follow/Unfollow method
     *
     * @param string|null $id Follower.following_id
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function followUnfollow($id = null)
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $followersTable = TableRegistry::getTableLocator()->get('followers');
            $followerResult = $followersTable->findByUserIdAndFollowingId($this->Auth->user('id'), $id)->first();
            if ($followerResult) {
                $follow = $this->Followers->get($followerResult->id);
                $this->Followers->delete($follow);

                $result = [
                    'status' => 'unfollow',
                    'message' => 'User Unfollowed'
                ];
            } else {
                $followerResult = $followersTable->newEntity();
                $followerResult->following_id = $id;
                $followerResult->user_id = $this->Auth->user('id');
                $followersTable->save($followerResult);

                $result = [
                    'status' => 'follow',
                    'message' => 'User Followed'
                ];
            }

            echo json_encode($result);
        }
    }

    /**
     * Followers method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function followers()
    {
        $followers = $this->Followers->find('all')
                ->where(['Followers.following_id' => $this->Auth->user('id')])
                ->contain(['users']);
        
        $userIds = $this->Followers->find('all')
                ->select(['user_id', 'following_id'])
                ->where(['user_id' => $this->Auth->user('id')])->toArray();
            
        $this->set(['followers' => $this->paginate($followers), 'users' => $userIds]);
    }

    /**
     * Following method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function following()
    {
        $following = $this->Followers->findByUserId($this->Auth->user('id'))
                ->contain(['users', 'Following']);
                
            $this->set('following',$this->paginate($following));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Followings'],
        ];
        $followers = $this->paginate($this->Followers);

        $this->set(compact('followers'));
    }

    /**
     * View method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => ['Users', 'Followings'],
        ]);

        $this->set('follower', $follower);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $follower = $this->Followers->newEntity();
        if ($this->request->is('post')) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $users = $this->Followers->Users->find('list', ['limit' => 200]);
        $followings = $this->Followers->Followings->find('list', ['limit' => 200]);
        $this->set(compact('follower', 'users', 'followings'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $users = $this->Followers->Users->find('list', ['limit' => 200]);
        $followings = $this->Followers->Followings->find('list', ['limit' => 200]);
        $this->set(compact('follower', 'users', 'followings'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $follower = $this->Followers->get($id);
        if ($this->Followers->delete($follower)) {
            $this->Flash->success(__('The follower has been deleted.'));
        } else {
            $this->Flash->error(__('The follower could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
