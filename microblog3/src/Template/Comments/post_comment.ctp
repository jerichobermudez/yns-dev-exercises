<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-comments"></span> Comments</h4>
    </div>
    <?= $this->Form->create('Comment', ['id' => 'addCommentForm', 'value' => 'comments/add_comment', 'url' => ['' => '', 'action' => 'add_comment']]); ?>
        <div class="modal-body">
            <div id="flashMessage"></div>
            <?php
                if (!json_decode(json_encode($comments), false)) { ?>
                    <div class="callout callout-warning"><span class="glyphicon glyphicon-info-sign"></span> Be the first one to comment.</div>
                <?php
                }

                foreach ($comments as $comment) {
                    ?>
                    <div class="box-footer box-comments" style="border-bottom: 1px solid lightgray;">
                        <div class="box-comment">
                            <?= $this->Html->image('../users/'. $comment->user['username'] . '/profile/' . $comment->user['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $comment->user['username']]) ?>
                            <div class="comment-text">
                                <span class="username">
                                    <?= $comment->user['username'] ?>
                                    <em class="text-muted">
                                        <?= $timestamp = date('m-d-y', strtotime($comment->created)) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($comment->created)) : date('D, M d, Y h:i A', strtotime($comment->created)); ?>
                                    </em>
                                    <div class="box-tools pull-right">
                                        <?php if ($comment->user_id === $current_user['id']) { ?>
                                        <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-edit', 'data-toggle' => 'tooltip', 'title' => 'Edit Comment']), ['class' => 'btn btn-box-tool edit-post-comment', 'data-id' => $comment->id, 'data-value' => \Cake\Routing\Router::url(['controller' => 'comments', 'action' => 'edit_comment'])], ['escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-remove', 'data-toggle' => 'tooltip', 'title' => 'Remove Comment']), ['class' => 'btn btn-box-tool delete-post-comment', 'data-id' => $comment->id, 'data-value' => \Cake\Routing\Router::url(['controller' => 'comments', 'action' => 'delete_comment'])], ['escape' => false]) ?><br>
                                        <?php } ?>
                                    </div>
                                </span>
                                <p stlye="margin-top: 20px;"><?= h($comment->comment) ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                // echo json_encode(json_decode(json_encode($comments), true)[0]['post_id']);
                //if (count(json_decode(json_encode($comments), false)) >= 3) echo $this->Form->button('Show more', ['class' => 'btn btn-block show-more-comment', 'data-id' => '']);
            ?>
            <div class="box-footer box-comments">
                <?= $this->Html->image('../users/' . $current_user['username'] . '/profile/' . $current_user['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $current_user['username']]) ?>
                <div class="img-push">
                    <?= $this->Form->hidden('post_id', ['value' => $id]); ?>
                    <?= $this->Form->input('comment', ['label' => false, 'type' => 'text', 'class' => 'form-control input-sm comment-to-post', 'placeholder' => 'Comment', 'autocomplete' => 'off']) ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?= $this->Form->button('Close', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
            <?= $this->Form->button('Comment', ['class' => 'btn btn-md btn-flat theme-button']) ?>
        </div>
    <?= $this->Form->end(); ?>
</div>