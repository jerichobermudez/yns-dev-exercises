<section class="content margin">
    <div class="col-lg-5 col-lg-offset-7 col-sm-7 col-sm-offset-5 col-xs-12">
        <div id="login" class="box box-warning">
            <div class="box-body">
                <h1 class="text-center"> Microblog</h1>
                <div class="margin">
                    <?= $this->Flash->render() ?>
                    <?= $this->Form->create('User') ?>
                    <?= $this->Form->input('username', ['label' => 'Username:', 'class' => 'form-control form-group', 'required' => true, 'autocomplete' => 'off', 'autofocus' => true]) ?>
                    <?= $this->Form->input('password', ['label' => 'Password:', 'type' => 'password', 'class' => 'form-control form-group', 'required' => true]) ?>
                    <span class="text-center"> <?= $this->Form->submit('SIGN IN', ['id' => 'loginBtn', 'class' => 'btn btn-md btn-flat form-group theme-button']) ?> </span>
                    <br>
                    <p> Don't have an Account? Click <?= $this->Html->Link('here', ['action' => 'signup'], ['id' => 'signupLink']); ?> to Register. </p>
                    <p> <?= $this->Html->Link('Forgot Password?', ['action' => 'forgot'], ['id' => 'forgotLink']); ?> </p>
                </div>
            </div>
        </div>
    </div>
</section>