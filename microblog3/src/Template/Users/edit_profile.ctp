<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-pencil"></span> Edit Profile</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="panel panel-warning flat">
                    <div class="panel-heading"><span class = "glyphicon glyphicon-picture"></span> Change Profile</div>
                    <div class="panel-body">
                        <?= $this->Form->create('Profile', ['id' => 'updateProfileForm', 'url' => ['controller' => 'users', 'action' => 'update_photo'], 'type' => 'file']) ?>
                        <?= $this->Html->image('../users/'. $user->username . '/profile/' . $user->profile, ['id' => 'profilePreview', 'class' => '', 'width' => '100%']) ?>
                        <?= $this->Form->file('profile', ['label' => false, 'class' => 'form-group form-control', 'onChange' => 'preview(event)', 'required' => 'true']) ?>
                        <?= $this->Form->submit('Update Photo', ['class' => 'btn btn-md btn-flat btn-block theme-button']) ?>
                        <?= $this->Form->end(); ?>
                        <script type="text/javascript">
                            var preview = function(event) {
                                var output = document.getElementById('profilePreview');
                                output.src = '' + URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel panel-warning flat">
                    <div class="panel-heading"><span class = "glyphicon glyphicon-info-sign"></span> Update Information</div>
                    <div class="panel-body">
                        <?= $this->Form->create('Profile', ['id' => 'updateInformationForm', 'url' => ['controller' => 'users', 'action' => 'update_info']]) ?>
                        <?= $this->Form->input('first_name', ['label' => 'Firstname:', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Firstname', 'value' => $user->first_name, 'autocomplete' => 'off']) ?>
                        <?= $this->Form->input('last_name', ['label' => 'Lastname:', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Lastname', 'value' => $user->last_name, 'autocomplete' => 'off']) ?>
                        <?= $this->Form->input('username', ['label' => 'Username:', 'class' => 'form-control form-group', 'placeholder' => 'Username', 'value' => $user->username, 'readonly', 'autocomplete' => 'off']) ?>
                        <?= $this->Form->input('email', ['label' => 'Email:', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Email', 'value' => $user->email]) ?>
                        <?= $this->Form->submit('Update Information', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
                <div class="panel panel-warning flat">
                    <div class="panel-heading"><span class = "glyphicon glyphicon-lock"></span> Change Password</div>
                    <div class="panel-body">
                        <?= $this->Form->create('Profile', ['id' => 'updatePasswordForm', 'url' => ['controller' => 'users', 'action' => 'update_password']]) ?>
                        <?= $this->Form->input('old_password', ['label' => 'Old Password:', 'type' => 'password', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Old Password']) ?>
                        <?= $this->Form->input('new_password', ['label' => 'New Password:', 'type' => 'password', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'New Password']) ?>
                        <?= $this->Form->input('retype_new_password', ['label' => 'Re-type New Password:', 'type' => 'password', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Re-type New Password']) ?>
                        <?= $this->Form->submit('Update Password', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button('Close', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
</div>