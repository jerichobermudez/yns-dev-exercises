<section class="content margin">
    <div class="col-lg-5 col-lg-offset-7 col-sm-7 col-sm-offset-5 col-xs-12">
        <div id="login" class="box box-warning">
            <div class="box-body">
                <h1 class="text-center"> Register</h1>
                <div class="margin">
                    <?= $this->Flash->render(); ?>
                    <?= $this->Form->create($user) ?>
                    <div class="form-group <?= ($this->Form->isFieldError('first_name'))? 'has-error': '' ; ?>">
                        <?= $this->Form->input('first_name', ['label' => 'Firstname:', 'class' => 'form-control', 'autocomplete' => 'off', 'error' => false]) ?>
                        <?= $this->Form->error('first_name', null, ['class' => 'label label-danger']); ?>
                    </div>
                    <div class="form-group <?= ($this->Form->isFieldError('last_name'))? 'has-error': '' ; ?>">
                        <?= $this->Form->input('last_name', ['label' => 'Lastname:', 'class' => 'form-control', 'autocomplete' => 'off', 'error' => false]) ?>
                        <?= $this->Form->error('last_name', null, ['class' => 'label label-danger']); ?>
                    </div>
                    <div class="form-group <?= ($this->Form->isFieldError('username')) ? 'has-error': '' ; ?>">
                        <?= $this->Form->input('username', ['label' => 'Username:', 'class' => 'form-control', 'autocomplete' => 'off', 'error' => false]) ?>
                        <?= $this->Form->error('username', null, ['class' => 'label label-danger']); ?>
                    </div>
                    <div class="form-group <?= ($this->Form->isFieldError('email'))? 'has-error': '' ; ?>">
                        <?= $this->Form->input('email', ['label' => 'Email:', 'class' => 'form-control', 'autocomplete' => 'off', 'error' => false]) ?>
                        <?= $this->Form->error('email', null, ['class' => 'label label-danger']); ?>
                    </div>
                    <div class="form-group <?= ($this->Form->isFieldError('password'))? 'has-error': '' ; ?>">
                        <?= $this->Form->input('password', ['label' => 'Password:', 'type' => 'password', 'class' => 'form-control', 'error' => false]) ?>
                        <?= $this->Form->error('password', null, ['class' => 'label label-danger']); ?>
                    </div>
                    <div class="form-group <?= ($this->Form->isFieldError('repassword'))? 'has-error': '' ; ?>">
                        <?= $this->Form->input('repassword', ['label' => 'Re-type Password:', 'type' => 'password', 'class' => 'form-control', 'error' => false]) ?>
                        <?= $this->Form->error('repassword', null, ['class' => 'label label-danger']); ?>
                    </div>
                    <span class="text-center"> <?= $this->Form->submit('SIGN UP', ['id' => 'signupBtn', 'class' => 'btn btn-md btn-flat form-group theme-button']) ?> </span>
                    <br>
                    <p> Already have an Account? Click <?= $this->Html->Link('here', ['action' => 'login'], ['id' => 'loginLink']); ?> to Sign In. </p>
                    <p> <?= $this->Html->Link('Forgot Password?', ['action' => 'forgot'], ['id' => 'forgotLink']); ?> </p>
                </div>
            </div>
        </div>
    </div>
</section>