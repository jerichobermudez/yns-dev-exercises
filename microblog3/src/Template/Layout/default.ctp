<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Microblog 3';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name = "author" content = "Jericho Ramos Bermudez">
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?php $this->Html->meta('icon') ?>
        <?= $this->HTML->css('bootstrap.min'); ?>
        <?= $this->HTML->css('AdminLTE.min'); ?>
        <?= $this->HTML->css('all-skins.min'); ?>
        <?= $this->HTML->css('font-awesome.min'); ?>
        <?= $this->HTML->css('jquery.jgrowl'); ?>
        <?= $this->HTML->css('main'); ?>
        <?= $this->Html->script('jQuery-2.1.4.min'); ?>
        <?= $this->Html->script('bootstrap.min'); ?>
        <?= $this->Html->script('app.min'); ?>
        <?= $this->Html->script('jquery.jgrowl'); ?>
    </head>
    <body class="hold-transition skin-yellow sidebar-mini" root_directory="<?= $this->request->getAttribute("webroot") ?>">
    <!-- <body class="hold-transition skin-yellow sidebar-mini" root_directory="<?= $this->Url->build('/') ?>"> -->
		<!-- BACKGROUND CONTAINER -->
		<div id = "bgContainer">
			<!-- BACKGROUND IMAGE -->
			<div id = "bgImage"></div>
		</div>
        <?= $this->fetch('content') ?>
        
        <div class="modal fade" id="editPostModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="editPostContent"></div>
            </div>
        </div>
        <div class="modal fade" id="deletePostModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="deletePostContent"></div>
            </div>
        </div>
        <div class="modal fade" id="commentPostModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="commentPostContent"></div>
            </div>
        </div>
        <div class="modal fade" id="editCommentModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="editCommentContent"></div>
            </div>
        </div>
        <div class="modal fade" id="deleteCommentModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="deleteCommentContent"></div>
            </div>
        </div>
        <div class="modal fade" id="retweetModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="retweetContent"></div>
            </div>
        </div>
        
        <div class="modal fade" id="followersModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="followersContent"></div>
            </div>
        </div>
        <div class="modal fade" id="followingModal" tabindex="-1">
            <div class="modal-dialog">
                <div id="followingContent"></div>
            </div>
        </div>
        <div class="modal fade" id="editProfileModal" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div id="editProfileContent"></div>
            </div>
        </div>
    </body>
</html>
