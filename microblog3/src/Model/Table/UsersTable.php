<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        // $this->addBehavior('Timestamp');

        $this->hasMany('posts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('followers', [
            'foreignKey' => 'following_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notBlank('first_name', 'Firstname cannot be left blank.')
            ->add('first_name', [
                'nameChecker' => [
                    'rule' => 'nameChecker',
                    'provider' => 'table',
                    'message' => 'Only letters and spaces are allowed.'
                ]
            ]);

        $validator
            ->notBlank('last_name', 'Lastname cannot be left blank.')
            ->add('last_name', [
                'nameChecker' => [
                    'rule' => 'nameChecker',
                    'provider' => 'table',
                    'message' => 'Only letters and spaces are allowed.'
                ]
            ]);

        $validator
            ->notBlank('username', 'Username cannot be left blank.')
            ->alphaNumeric('username', 'Letters and numbers only.')
            ->minLength('username', 5, 'Minimum of 5 characters.')
            ->add('username', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Username already used. Please try another.'
                ]
            ]);

        $validator
            ->notBlank('email', 'Email cannot be left blank.')
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Email already used. Please try another.'
                ]
            ]);

        $validator
            ->notBlank('password', 'Password cannot be left blank.')
            ->alphaNumeric('password', 'Letters and numbers only.')
            ->minLength('password', 5, 'Minimum of 5 characters.')
            ->add('password', 'custom', [
                    'rule' => ['compareWith', 'repassword'],
                    'message' => 'Password doesn\'t match.'
            ])
            ->add('repassword', null, [
                'rule' => ['compareWith', 'password'],
                'message' => 'Password doesn\'t match.',
            ]);

        $validator
            ->notBlank('activation_code', 'Activation Code cannot be left blank.')
            ->lengthBetween('activation_code', [8, 8], 'Please enter an 8 digit code');

        return $validator;
    }

    
    /**
     * Checks name for a single instance 
     *
     * @param type $data
     * @param array $context
     * @return boolean
     */
    public function nameChecker($data, array $context) {
        if(!preg_match('/^[A-Za-z\s]+$/', $data)) {
            return false;
        }
		return true;
    }
}
