-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2020 at 11:27 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog3_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment` varchar(140) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `deleted_date` datetime DEFAULT current_timestamp(),
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `comment`, `deleted`, `deleted_date`, `created`, `modified`) VALUES
(1, 1, 3, 'cute cute cute', 0, NULL, '2020-05-04 17:27:08', '2020-05-04 17:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `following_id`, `created`, `modified`) VALUES
(1, 1, 2, '2020-05-04 09:10:55', '2020-05-04 09:10:55'),
(2, 2, 1, '2020-05-04 09:12:43', '2020-05-04 09:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20200415065154, 'Initial', '2020-04-14 22:51:58', '2020-04-14 22:51:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(35) NOT NULL,
  `content` varchar(140) NOT NULL,
  `image` varchar(125) DEFAULT NULL,
  `is_retweet` int(11) DEFAULT NULL,
  `original_post_id` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `deleted_date` datetime DEFAULT current_timestamp(),
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `content`, `image`, `is_retweet`, `original_post_id`, `deleted`, `deleted_date`, `created`, `modified`) VALUES
(1, 1, 'Microblogsss', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce efficitur volutpat semper. Duis non lorem quis mi luctus porta. Suspendisse e', 'background.jpg', NULL, NULL, 0, NULL, '2020-03-24 14:47:46', '2020-05-04 09:06:18'),
(2, 1, 'Cute!!', 'Hahahahaha', 'cog.PNG', NULL, NULL, 0, NULL, '2020-03-26 10:18:15', '2020-05-04 09:06:43'),
(3, 2, 'Retweet', 'Retweet this post', NULL, 1, 1, 0, NULL, '2020-04-13 14:40:35', '2020-05-04 09:08:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(125) NOT NULL,
  `profile` varchar(125) DEFAULT 'default.png',
  `activated` int(11) NOT NULL DEFAULT 0,
  `activation_code` varchar(125) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `deleted_date` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `profile`, `activated`, `activation_code`, `deleted`, `deleted_date`, `created`, `modified`) VALUES
(1, 'Jericho', 'Bermudez', 'ekoypogi', 'jerichobermudez.yns@gmail.com', '$2y$10$k4VGfkCCp826RAnGmX7fUeB0q3THEXkZZt2Bf.w3.rJucdubp7j7G', 'cog.PNG', 1, NULL, 0, NULL, '2020-03-24 12:55:55', '2020-04-14 08:55:17'),
(2, 'ekoy', 'ekoy', 'ekoy', 'ekoy@ekoy.com', '$2y$10$k4VGfkCCp826RAnGmX7fUeB0q3THEXkZZt2Bf.w3.rJucdubp7j7G', 'default.png', 1, NULL, 0, NULL, '2020-04-16 05:30:22', '2020-04-16 05:30:22'),
(3, 'Jericho', 'Ramos', 'ekoyekoy', 'dyeybermudezs06@gmail.com', '$2y$10$k4VGfkCCp826RAnGmX7fUeB0q3THEXkZZt2Bf.w3.rJucdubp7j7G', 'default.png', 1, NULL, 0, NULL, '2020-04-17 07:37:42', '2020-04-17 07:37:42'),
(6, 'Dyey', 'Bermudez', 'dyeybermudez', 'dyeybermudez06@gmail.com', '$2y$10$k4VGfkCCp826RAnGmX7fUeB0q3THEXkZZt2Bf.w3.rJucdubp7j7G', 'default.png', 1, NULL, 0, NULL, '2020-04-20 08:33:46', '2020-04-20 08:33:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
