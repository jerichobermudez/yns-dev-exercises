<?php

    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');

    $connection = new mysqli(HOSTNAME, USERNAME, PASSWORD, DATABASE);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 3-4 | SQL PROBLEMS.</title>
    </head>
    <body>
        <h2>SQL PROBLEMS</h2><br>

        1) Retrieve employees whose last name start with "K".<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>first_name</th>
                    <th>last_name</th>
                    <th>middle_name</th>
                    <th>birth_date</th>
                    <th>department_id</th>
                    <th>hire_date</th>
                    <th>boss_id</th>
                </tr>
            </thead>
            <?php

                $like = 'K%';

                $qry = 'SELECT employee_id, first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id FROM employees WHERE last_name LIKE ?';

                $stmt = $connection->prepare($qry);
                
                $stmt->bind_param('s', $like);

                $stmt->bind_result($id, $fname, $lname, $mname, $bday, $depID, $hireDate, $bossID);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $id; ?></td>
                        <td><?= $fname; ?></td>
                        <td><?= $lname; ?></td>
                        <td><?= $mname; ?></td>
                        <td><?= $bday; ?></td>
                        <td><?= $depID; ?></td>
                        <td><?= $hireDate; ?></td>
                        <td><?= $bossID; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>
        
        <br><br> 

        2) Retrieve employees whose last name start with "i".<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>first_name</th>
                    <th>last_name</th>
                    <th>middle_name</th>
                    <th>birth_date</th>
                    <th>department_id</th>
                    <th>hire_date</th>
                    <th>boss_id</th>
                </tr>
            </thead>
            <?php

                $like = '%i';

                $qry = 'SELECT employee_id, first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id FROM employees WHERE last_name LIKE ?';

                $stmt = $connection->prepare($qry);
                
                $stmt->bind_param('s', $like);

                $stmt->bind_result($id, $fname, $lname, $mname, $bday, $depID, $hireDate, $bossID);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $id; ?></td>
                        <td><?= $fname; ?></td>
                        <td><?= $lname; ?></td>
                        <td><?= $mname; ?></td>
                        <td><?= $bday; ?></td>
                        <td><?= $depID; ?></td>
                        <td><?= $hireDate; ?></td>
                        <td><?= $bossID; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>

        <br><br>
        
        3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>full_name</th>
                    <th>hire_date</th>
                </tr>
            </thead>
            <?php

                $start = '2015-1-1';
                
                $end = '2015-3-31';

                $qry = 'SELECT CONCAT(first_name, " ", middle_name, " ", last_name) full_name, hire_date FROM employees WHERE hire_date BETWEEN ? AND ? ORDER BY hire_date ASC';

                $stmt = $connection->prepare($qry);
                
                $stmt->bind_param('ss', $start, $end);

                $stmt->bind_result($fullname, $hireDate);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $fullname; ?></td>
                        <td><?= $hireDate; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>
        
        <br><br>
        
        4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>Employee</th>
                    <th>Boss</th>
                </tr>
            </thead>
            <?php

                $qry = 'SELECT b.last_name AS employee, a.last_name AS boss FROM employees b LEFT JOIN employees a ON b.boss_id = a.employee_id WHERE a.last_name IS NOT NULL';

                $stmt = $connection->prepare($qry);

                $stmt->bind_result($employee, $boss);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $employee; ?></td>
                        <td><?= $boss; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>
        
        <br><br>  

        5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>last_name</th>
                </tr>
            </thead>
            <?php

                $department = 'Sales';

                $qry = 'SELECT last_name FROM employees LEFT JOIN departments USING (department_id) WHERE departments.name = ? ORDER BY last_name DESC';

                $stmt = $connection->prepare($qry);
                
                $stmt->bind_param('s', $department);

                $stmt->bind_result($lastname);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $lastname; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>
        
        <br><br>

        6) Retrieve number of employee who has middle name.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>count_has_middle</th>
                </tr>
            </thead>
            <?php

                $qry = 'SELECT employee_id FROM employees WHERE middle_name IS NOT NULL';

                $stmt = $connection->prepare($qry);

                $stmt->execute();

                $stmt->store_result();

                $count = $stmt->num_rows;

                $stmt->fetch();

                ?>

                    <tr>
                        <td><?= $count; ?></td>
                    </tr>

                <?php

                $stmt->close();
                
            ?>
        </table>
        
        <br><br>

        7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>name</th>
                    <th>count</th>
                </tr>
            </thead>
            <?php

                $qry = 'SELECT name, COUNT(department_id) FROM departments RIGHT JOIN employees USING (department_id) GROUP BY department_id';

                $stmt = $connection->prepare($qry);

                $stmt->bind_result($name, $count);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $name; ?></td>
                        <td><?= $count; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>

        <br><br>
        
        8) Retrieve employee's full name and hire date who was hired the most recently.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>first_name</th>
                    <th>middle_name</th>
                    <th>last_name</th>
                    <th>hire_date</th>
                </tr>
            </thead>
            <?php

                $qry = 'SELECT first_name, middle_name, last_name, hire_date FROM employees ORDER BY hire_date DESC LIMIT 1';

                $stmt = $connection->prepare($qry);

                $stmt->bind_result($first_name, $middle_name, $last_name, $hire_date);

                $stmt->execute();

                $stmt->fetch();

                ?>

                    <tr>
                        <td><?= $first_name; ?></td>
                        <td><?= $middle_name; ?></td>
                        <td><?= $last_name; ?></td>
                        <td><?= $hire_date; ?></td>
                    </tr>

                <?php

                $stmt->close();
                
            ?>
        </table>

        <br><br>

        9) Retrieve department name which has no employee.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>name</th>
                    <th>department_id</th>
                </tr>
            </thead>
            <?php

                $qry = 'SELECT name, department_id AS counts FROM departments d LEFT JOIN employees e USING (department_id) WHERE employee_id IS NULL GROUP BY d.department_id';

                $stmt = $connection->prepare($qry);

                $stmt->bind_result($name, $department_id);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $name; ?></td>
                        <td><?= $department_id; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>
        
        <br><br>

        10) Retrieve employee's full name who has more than 2 positions.<br>
        
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>first_name</th>
                    <th>middle_name</th>
                    <th>last_name</th>
                </tr>
            </thead>
            <?php

                $qry = 'SELECT first_name, middle_name, last_name FROM employee_positions LEFT JOIN employees USING (employee_id) GROUP BY employee_id HAVING COUNT(employee_id) > 2';

                $stmt = $connection->prepare($qry);

                $stmt->bind_result($first_name, $middle_name, $last_name);

                $stmt->execute();

                while ($stmt->fetch()) {

                    ?>

                    <tr>
                        <td><?= $first_name; ?></td>
                        <td><?= $middle_name; ?></td>
                        <td><?= $last_name; ?></td>
                    </tr>

                    <?php

                }

                $stmt->close();
                
            ?>
        </table>
        
        <br><br>

    <body>
</html>