<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-6 | Press button and add a label below button.</title>
    </head>
    <body>
        <h3>Press button and add a label below button.</h3>  
        <input type="submit" id="addLabel" value="Add Label">
        <br><br>
        <label id="result"></label>
    </body>
    <script type="text/javascript" src="main.js"></script>
</html>