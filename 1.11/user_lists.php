<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-11 | Show uploaded images in the table.</title>
    </head>
    <body>
        <a href="index.php"><input type="button" value="Add New"></a><br><br>
        <table border>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Profile</td>
            </tr>
            <?php
                $file = fopen("user_info.csv", "r");
                while (($data = fgetcsv($file)) !== false) {
                    ?>
                    <tr>
                        <td> <?= $data[0] ?> </td>
                        <td> <?= $data[1] ?> </td>
                        <td> <?= $data[2] ?> </td>
                        <td><img width="150" src="images/<?= trim($data[3]) ?>"></td>
                    </tr>
                    <?php
                }
                fclose($file);
            ?>
        </table>
    </body>
</html>