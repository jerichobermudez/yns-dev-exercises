<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-8 | Store inputted user information into a CSV file.</title>
    </head>
    <body>
        <?php
            if (isset($_POST['submit'])) {
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $email = $_POST['email'];
                $phone = $_POST['phone'];
                if (!preg_match("/^([a-zA-Z' ]+)$/", $fname.' '.$lname)) {
                    ?>
                    <font face="Verdana" size="2" color="red">Invalid Name</font><br><a href="javascript:history.back()">go back</a>
                    <?php
                } else if (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) {
                    ?>
                    <font face="Verdana" size="2" color="red">Invalid Email</font><br><a href="javascript:history.back()">go back</a>
                    <?php
                } else if (!preg_match('/^[0-9]{11}+$/', $phone)) {
                    ?>
                    <font face="Verdana" size="2" color="red">Invalid Phone</font><br><a href="javascript:history.back()">go back</a>
                    <?php
                } else {
                    ?>
                    <b>User Information:</b>
                    <br><br>
                    <b>Name: </b> <?= $fname . ' ' . $lname ?>
                    <br>
                    <b>Email: </b> <?= $email ?>
                    <br>
                    <b>Phone: </b> <?= $phone ?>
                    <?php
                    $header = "Name, Email, Phone\n";
                    $data = $fname . ' ' . $lname . ', ' . $email . ',  ' . $phone."\n";
                    $fileName = "user_info.csv";
                    if (file_exists($fileName)) {
                        // Add only data. The header is already added in the existing file.
                        file_put_contents($fileName, $data, FILE_APPEND);
                    } else {
                        // Add CSV header and data.
                        file_put_contents($fileName, $header . $data);
                    }
                }
            }
        ?>
    </body>
</html>