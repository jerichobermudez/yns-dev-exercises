<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-8 | Store inputted user information into a CSV file.</title>
    </head>
    <body>
        <form action="display.php" method="POST">
            <label><b>Firstname: </b></label>
            <input type="text" name="fname" required autofocus>
            <br><br>
            <label><b>Lastname: </b></label>
            <input type="text" name="lname" required>
            <br><br>
            <label><b>Email: </b></label>
            <input type="text" name="email" required>
            <br><br>
            <label><b>Phone: </b></label>
            <input type="text" name="phone" required>
            <br><br>
            <input type="submit" name="submit" value="Submit">
        </form>
    </body>
</html>