<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-13 | Create Login Form.</title>
    </head>
    <body>
        <?php
            session_start();
            if (isset($_POST['login'])) {
                $username = $_POST['username'];
                $password = $_POST['password'];
                $users = file('user_info.csv');
                $dbUser = [];
                for ($i = 0; $i < count($users); $i++) {
                    $dbUser[] = explode(", ", $users[$i]);
                }
                if (in_array($username, array_column($dbUser, 0)) && in_array($password, array_column($dbUser, 2))) {
                    $_SESSION['username'] = $username;
                } else {
                    ?>
                    <font face="Verdana" size="2" color="red">Wrong Username / Password! Please try again.</font><br><br>;
                    <?php
                }
            }
            if (!isset($_SESSION['username'])) {
            ?>
                <form method="POST">
                    <label><b>Username: </b></label>
                    <input type="text" name="username" required autofocus>
                    <br><br>
                    <label><b>Password: </b></label>
                    <input type="password" name="password" required>
                    <br><br>
                    <input type="submit" name="login" value="Login"> 
                </form>
            <?php
            } else {
                ?>
                <br>Welcome: <?= $_SESSION['username'] ?> <a href="signout.php"><input type="button" value="Sign Out"></a>
                <br><br>
                <a href="user_lists.php"><input type="button" value="View List"></a>
                <br><br>
                <form action="display.php" method="POST" enctype="multipart/form-data">
                    <label><b>Firstname: </b></label>
                    <input type="text" name="fname" required autofocus>
                    <br><br>
                    <label><b>Lastname: </b></label>
                    <input type="text" name="lname" required>
                    <br><br>
                    <label><b>Email: </b></label>
                    <input type="text" name="email" required>
                    <br><br>
                    <label><b>Phone: </b></label>
                    <input type="text" name="phone" required>
                    <br><br>
                    <label><b>Profile: </b></label>
                    <input type="file" name="profile" required>
                    <br><br>
                    <input type="submit" name="submit" value="Submit">
                </form>
                <?php
            }
        ?>
    </body>
</html>