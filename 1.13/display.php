<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 1-13 | Create Login Form.</title>
    </head>
    <body>
        <?php
            session_start();
            if (!isset($_SESSION['username'])) {
                ?>
                <font face="Verdana" size="2" color="red">You are no yet logged in</font><br><br> Click <a href = "./">here</a> to Login
                <?php
            } else {
                if (isset($_POST['submit'])) {
                    $fname = $_POST['fname'];
                    $lname = $_POST['lname'];
                    $email = $_POST['email'];
                    $phone = $_POST['phone'];
                    //for file upload
                    $currentDir = getcwd();
                    $uploadPath = "/images/";
                    if (!file_exists($uploadPath)) { mkdir($uploadPath, 0777); }
                    $fileExtensions = ['jpeg','jpg','png'];
                    $fileName = $_FILES['profile']['name'];
                    $fileSize = $_FILES['profile']['size'];
                    $maxFileSize = 1024 * 1024 * 10;
                    $fileTmpName  = $_FILES['profile']['tmp_name'];
                    $fileType = $_FILES['profile']['type'];
                    $fileExtension = explode('.', $fileName);
                    $fileExtension = strtolower(end($fileExtension));
                    $uploadPath = $currentDir . $uploadPath . basename($fileName);
                    if (!preg_match("/^([a-zA-Z' ]+)$/", $fname.' '.$lname)) {
                        ?>
                        <font face="Verdana" size="2" color="red">Invalid Name</font><br><a href="javascript:history.back()">go back</a>
                        <?php
                    } else if (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) {
                        ?>
                        <font face="Verdana" size="2" color="red">Invalid Email</font><br><a href="javascript:history.back()">go back</a>
                        <?php
                    } else if (!preg_match('/^[0-9]{11}+$/', $phone)) {
                        ?>
                        <font face="Verdana" size="2" color="red">Invalid Phone</font><br><a href="javascript:history.back()">go back</a>
                        <?php
                    } else if (!in_array($fileExtension, $fileExtensions)) {
                        ?>
                        <font face="Verdana" size="2" color="red">This file extension is not allowed. Please upload a JPEG or PNG file</font><br><a href="javascript:history.back()">go back</a>
                        <?php
                    } else if ($fileSize > $maxFileSize) {
                        ?>
                        <font face="Verdana" size="2" color="red">This file is more than 10MB. Sorry, it has to be less than or equal to 10MB</font><br><a href="javascript:history.back()">go back</a>
                        <?php
                    } else {
                        move_uploaded_file($fileTmpName, $uploadPath);
                        ?>
                        <b>User Information:</b>
                        <br><br>
                        <b>Name: </b> <?= $fname . ' ' . $lname ?>
                        <br>
                        <b>Email: </b> <?= $email ?>
                        <br>
                        <b>Phone: </b> <?= $phone ?>
                        <br>
                        <b>Profile: </b> <img width="250" src="images/<?= $fileName ?>">
                        <?php
                        $header = "Name, Email, Phone, Profile\n";
                        $data = $fname . ' ' . $lname . ', ' . $email . ',  ' . $phone . ', ' . $fileName . "\n";
                        $fileName = "user_info.csv";
                        if (file_exists($fileName)) {
                            // Add only data. The header is already added in the existing file.
                            file_put_contents($fileName, $data, FILE_APPEND);
                        } else {
                            // Add CSV header and data.
                            file_put_contents($fileName, $header . $data);
                        }
                    }

                }

            }

        ?>
        
    </body>
</html>