-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 09:55 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testing_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_shifts`
--

CREATE TABLE `daily_work_shifts` (
  `shift_id` int(11) NOT NULL,
  `therapist_id` int(11) NOT NULL,
  `target_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_work_shifts`
--

INSERT INTO `daily_work_shifts` (`shift_id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES
(1, 1, '2020-03-17', '14:00:00', '15:00:00'),
(2, 2, '2020-03-17', '22:00:00', '23:00:00'),
(3, 3, '2020-03-17', '00:00:00', '01:00:00'),
(4, 4, '2020-03-17', '05:00:00', '05:30:00'),
(5, 1, '2020-03-17', '21:00:00', '21:45:00'),
(6, 5, '2020-03-17', '05:30:00', '05:50:00'),
(7, 3, '2020-03-17', '02:00:00', '02:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  ADD PRIMARY KEY (`shift_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  MODIFY `shift_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
