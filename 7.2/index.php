<?php
    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');
    $connection = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DATABASE . ";charset=utf8", USERNAME, PASSWORD);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 7-2 | List all therapists according to their daily work shifts.</title>
    </head>
    <body>
        <h3>Exercise 7-2 | List all therapists according to their daily work shifts.</h3>
        <table border cellpadding="5">
            <thead>
                <th>Therapist</th>
                <th>Target Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Sort Start Time</th>
            </thead>
            <tbody>
                <?php
                    $lessStartTime = '05:59:59';
                    $greaterStartTime = '00:00:00';
                    $qry = "SELECT name, target_date, start_time, end_time,
                            CASE 
                                WHEN start_time <= ? AND start_time >= ? THEN CONCAT(DATE_ADD(target_date, INTERVAL 1 DAY), ' ', start_time)
                                ELSE CONCAT(target_date, ' ', start_time)
                            END AS sort_start_time
                            FROM daily_work_shifts INNER JOIN therapists USING (therapist_id) ORDER BY target_date, sort_start_time";
                    $stmt = $connection->prepare($qry);
                    $stmt->execute([$lessStartTime, $greaterStartTime]);
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($result as $key => $row) {
                        ?>
                        <tr>
                            <td> <?= $row['name'] ?> </td>
                            <td> <?= $row['target_date'] ?> </td>
                            <td> <?= $row['start_time'] ?> </td>
                            <td> <?= $row['end_time'] ?> </td>
                            <td> <?= $row['sort_start_time'] ?> </td>
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </body>
</html>