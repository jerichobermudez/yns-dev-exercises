<?php
    define('HOSTNAME', 'localhost');
    define('USERNAME', 'root');
    define('PASSWORD', '');
    define('DATABASE', 'testing_db');
    $connection = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DATABASE . ";charset=utf8", USERNAME, PASSWORD);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 7-3 | Using Case Conditional Statement.</title>
    </head>
    <body>
        <h3>Exercise 7-3 | Using Case Conditional Statement.</h3>
        <table border='1' cellspacing="1" cellpadding="5">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>first_name</th>
                    <th>last_name</th>
                    <th>middle_name</th>
                    <th>birth_date</th>
                    <th>department_id</th>
                    <th>hire_date</th>
                    <th>boss_id</th>
                    <th>position_name</th>
                </tr>
            </thead>
            <?php
                $result = [];
                $qry = 'SELECT employee_id, first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id,
                        CASE
                            WHEN name = "CEO" THEN "Chief Executive Officer"
                            WHEN name = "CTO" THEN "Chief Technical Officer"
                            WHEN name = "CFO" THEN "Chief Financial Officer"
                            ELSE name
                        END AS position_name
                        FROM employees LEFT JOIN employee_positions USING (employee_id) LEFT JOIN positions USING (position_id)';
                $stmt = $connection->prepare($qry);
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($result as $key => $row) {
                    ?>
                    <tr>
                        <td> <?= $row['employee_id'] ?> </td>
                        <td> <?= $row['first_name'] ?> </td>
                        <td> <?= $row['last_name'] ?> </td>
                        <td> <?= $row['middle_name'] ?> </td>
                        <td> <?= $row['birth_date'] ?> </td>
                        <td> <?= $row['department_id'] ?> </td>
                        <td> <?= $row['hire_date'] ?> </td>
                        <td> <?= $row['boss_id'] ?> </td>
                        <td> <?= $row['position_name'] ?> </td>
                    </tr>
                    <?php
                }
                
            ?>
        </table>
    </body>
</html>