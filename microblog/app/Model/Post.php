<?php
    App::uses('AppModel', 'Model');
    class Post extends AppModel {
        public $name = 'Posts';
        public $displayField = 'name';
        public $validate = [
            'title' => [
                'notBlank' => [
                    'rule' => 'notBlank',
                    'message' => 'Title should not be left blank',
                    'allowEmpty' => false
                ]
            ],
            'content' => [
                'notBlank' => [
                    'rule' => 'notBlank',
                    'message' => 'Content should not be left blank',
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 140],
                    'message' => 'Content should not exceed to 140 characters',
                    'allowEmpty' => false
                ]
            ],
            'image' => [
                'Invalid Image Extension. Extension should be jpeg, jpg, png & gif' => [
                    'rule' => 'imgExtension',
                    'message' => 'Invalid Image Extension. Extension should be jpeg, jpg, png & gif',
                    'allowEmpty' => true
                ]
            ]
        ];

        public $belongsTo = [
            'User' => [],
            'Follower' => [
                'className' => 'Followers',
                'foreignKey' => false,
                'conditions' => [
                    'OR' => [
                        'Follower.following_id = Post.user_id',
                    ]
                ],
            ],
            'Like' => [
                'className' => 'like',
                'foreignKey' => false,
                'conditions' => ['Like.post_id = Post.id']
            ],
            'Original' => [
                'className' => 'Post',
                'foreignKey' => 'original_post_id'
            ]
        ];

        public function imgExtension($data) {
            $file = pathinfo($data['image']);
            $fileExt = strtolower($file['extension']);
            $allowedExtension = ['jpeg', 'png', 'jpg', 'gif'];
            if (in_array($fileExt, $allowedExtension)) {
                return true;
            } else {
                return false;
            }
        }
    }