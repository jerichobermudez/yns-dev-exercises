<?php
    class User extends AppModel {
        public $name = 'Users';
        public $displayField = 'name';

        public $validate = [
            'first_name' => [
                'Please Enter your Firstname.' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Enter your Firstname.'
                ],
                'Special characters are not allowed' => [
                    'rule' => '/^[a-z ]*$/i',
                    'message' => 'Only letters and spaces are allowed.'
                ]
            ],
            'last_name' => [
                'Please Enter your Lastname.' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Enter your Lastname.'
                ],
                'Special characters are not allowed' => [
                    'rule' => '/^[a-z ]*$/i',
                    'message' => 'Only letters and spaces are allowed.'
                ]
            ],
            'username' => [
                'Please Enter your Username.' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Enter your Username.'
                ],
                'limit' => [
                    'rule' => ['minLength', 5],
                    'message' => 'Minimum of 5 characters.'
                ],
                'special_chars' => [
                    'rule' => '/^[a-z0-9]*$/i',
                    'message' => 'Special characters are not allowed.'
                ],
                'unique' => [
                    'rule' => 'isUnique',
                    'message' => 'Username already used. Please try another'
                ],
            ],
            'email' => [
                'Valid Email' => [
                    'rule' => ['email'],
                    'message' => 'Please Enter Valid email Address'
                ],
                'special_chars' => [
                    'rule' => '/^[a-z0-9_.-@]*$/i',
                    'message' => 'Special characters are not allowed.'
                ],
                'unique' => [
                    'rule' => 'isUnique',
                    'message' => 'Email already used. Please try another'
                ]
            ],
            'password' => [
                'Not Empty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Enter your Password.'
                ],
                'alphaNumeric' => [
                    'rule' => 'alphaNumeric',
                    'message' => 'Only letters and numbers are allowed.'
                ],
                'length' => [
                    'rule' => ['minLength', 5],
                    'message' => 'Minimum of 5 characters.'
                ],
                'Match Passwords' => [
                    'rule' => 'matchPasswords',
                    'message' => 'Password not match.'
                ]
            ],
            'repassword' => [
                'Not Empty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Confirm your Password.'
                ]
            ],

            'new_password'  => [
                'Not Empty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Enter your Password.'
                ],
                'alphaNumeric' => [
                    'rule' => 'alphaNumeric',
                    'message' => 'Only letters and numbers are allowed.'
                ],
                'length' => [
                    'rule' => ['minLength', 5],
                    'message' => 'Minimum of 5 characters.'
                ],
                'Match Passwords' => [
                    'rule' => 'matchPasswords',
                    'message' => 'Password not match.'
                ]
            ],
            'retype_new_password' => [
                'Not Empty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please Enter your Password.'
                ],
                'alphaNumeric' => [
                    'rule' => 'alphaNumeric',
                    'message' => 'Only letters and numbers are allowed.'
                ],
                'length' => [
                    'rule' => ['minLength', 5],
                    'message' => 'Minimum of 5 characters.'
                ],
                'Match Passwords' => [
                    'rule' => 'matchPasswords',
                    'message' => 'Password not match.'
                ]
            ],
            'activation_code' => [
            ]
        ];

        public $hasMany = array(
            'Posts' => array(
                'className' => 'Post',
                'foreignKey' => 'user_id',
            ),
            'Followers' => array(
                'className' => 'Followers',
                'foreignKey' => 'following_id'
            ),
            'Following' => array(
                'className' => 'Followers',
                'foreignKey' => 'user_id'
            ),
        );

        public function matchPasswords($data) {
            if ($data['password'] === $this->data['User']['repassword']) {
                return true;
            }
            $this->invalidate('repassword', 'Password not match');
            return false;
        }

        public function beforeSave($options = []) {
            if (isset($this->data['User']['password'])) {
                $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
            }
            return true;
        }
    }