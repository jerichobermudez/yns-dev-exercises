<?php
    class Comment extends AppModel {
        public $name = 'Comments';
        public $displayField = 'name';
        public $validate = array(
            'comment' => array(
                'rule' => 'notBlank'
            )
        );
    }