<?php
App::uses('Controller', 'Controller');

class AppController extends Controller {
    public $components = [
        'Session',
        'Auth' => [
            'loginRedirect' => ['controller' => 'users', 'action' => 'signup'],
            'logoutRedirect' => ['controller' => 'users', 'action' => 'login'],
            'authError' => "You can't access this page",
            'authorize' => ['Controller']
        ]
    ];

    public function isAuthorized($user) {
        return true;
    }

    public function beforeFilter() {
        $this->Auth->allow('login', 'signup');
        $this->set('logged_in', $this->Auth->loggedIn());
        $this->set('current_user', $this->Auth->user());
    }
}
