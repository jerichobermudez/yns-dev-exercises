<?php
class CommentsController extends AppController {
    public $name = 'Comments';
    public $helpers = array('Html', 'Form', 'Flash', 'Js');
    public $components = array('Flash');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'post_comment');
    }

    /**
     * This is to add comment on post
     *
     * @param $id - Post.id
     * @return void
     */
    public function post_comment()
    {
        if ($this->request->is('post')) {
            $data = [
                'user_id' => $this->Auth->user('id'),
                'post_id' => $this->request->data['Comment']['post_id'],
                'comment' => $this->request->data['Comment']['comment'],
                'deleted_date' => null
            ];
            $this->Comment->create($data);
            $this->Comment->set('data', $data);
            if (!$this->Comment->save()) {
                $this->Session->setFlash('Something Went Wrong Please Try Again.', 'flash_error');
            }
            return $this->redirect(array('controller' => 'posts', 'action' => 'comments', $data['post_id']));
        }
    }
}
