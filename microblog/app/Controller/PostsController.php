<?php
App::uses('AppModel', 'Model');
App::uses('Sanitize', 'Utility');

class PostsController extends AppController {
    public $name = 'Posts';
    public $helpers = ['Html', 'Form', 'Flash', 'Js'];
    public $components = ['Paginator', 'RequestHandler'];
    public $uses = ['Post', 'Like', 'User', 'Comment'];

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'search', 'post_comment', 'retweet_post', 'logged_in_user');
    }
    
    /**
     * Display all the post of the logged in user with following users
     *
     * @return void
     */
    public function index()
    {
        if (!$this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'users', 'action' => 'login']));
        } else {
            $this->paginate = [
                'conditions' => [
                    'Post.deleted !=' => 1,
                    'OR' => [
                        'User.id' => $this->Auth->user('id'),
                        'Follower.user_id' => $this->Auth->user('id'),
                    ]
                ],
                'limit' => 5,
                'order' => ['created' => 'desc']
            ];
            $this->set('posts', $this->paginate('Post'));

            if ($this->request->is('post')) {
                $filename = null;
                if (!empty($this->request->data['Post']['image']['tmp_name'])) {
                    $filename= basename($this->request->data['Post']['image']['name']);
                    move_uploaded_file($this->request->data['Post']['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'posts' . DS . $filename);
                }
                $data = [
                    'user_id' => $this->Auth->user('id'),
                    'title' => Sanitize::escape($this->request->data['Post']['title']),
                    'content' => Sanitize::escape($this->request->data['Post']['content']),
                    'image' => $filename,
                    'deleted_date' => null
                ];
                $this->Post->create($data);
                $this->Post->set('data', $data);
                if ($this->Post->save()) {
                    $this->Session->setFlash('Post Created', 'flash_success');
                    return $this->redirect(array('action' => '/'));
                } else {
                    $this->Session->setFlash('Something Went Wrong. Please try again', 'flash_error');
                }
            }
        }
    }

    /**
     * This is to edit user post
     *
     * @param $id - Post.id
     * @return void
     */
    public function edit($id = null)
    {
        $post = $this->Post->find('first', ['conditions' => ['Post.id' => $id, 'Post.deleted' => 0]]);
        if ($post['Post']['user_id'] !== $this->Auth->user('id')) {
            $this->Session->setFlash('You are not Authorized to Edit this Post!', 'flash_error');
            return $this->redirect(array('action' => '/'));
        }
        $this->set('post', $post);
        if ($this->request->is('post')) {
            $title = h($this->request->data['Post']['title']);
            $content = h($this->request->data['Post']['content']);
            $filename = null;
            if (!empty($this->request->data['Post']['image']['tmp_name'])) {
                $filename= basename($this->request->data['Post']['image']['name']);
                move_uploaded_file($this->request->data['Post']['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'posts' . DS . $filename);
                $data = ['title' => $title, 'content' => $content, 'image' => $filename];
            } else {
                $data = ['title' => $title, 'content' => $content];
            }
            $this->Post->id = $this->Post->field('id', ['id' => $id]);
            if ($this->Post->id) {
                if ($this->Post->set($data)) {
                    $this->Post->save();
                    $this->Session->setFlash('Post Successfully Updated', 'flash_success');
                    return $this->redirect(array('action' => '/'));
                } else {
                    $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                }
            } else {
                $this->Session->setFlash('Invalid Post!', 'flash_error');
            }
        }
    }

    /**
     * This is to remove user post
     *
     * @param $id - Post.id
     * @param $profile
     * @return void
     */
    public function delete($id = null, $profile = null)
    {
        if (!$id) {
            throw new NotFoundException('Invalid Post');
        }
        if ($this->request->is('get')) {
            $this->Post->id = $this->Post->field('id', ['id' => $id]);
            if ($this->Post->id) {
                if ($this->Post->set(['deleted' => 1, 'deleted_date' => date('Y-m-d H:i:s')])) {
                    $this->Post->save();
                    $this->Session->setFlash('Post Successfully Deleted', 'flash_success');
                } else {
                    $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                }
            } else {
                $this->Session->setFlash('Invalid Post!', 'flash_error');
            }
        }
        $controller = $profile ? 'users' : 'posts';
        $action = $profile ? 'profile' : '/';
        return $this->redirect(['controller' => $controller, 'action' => $action]);
    }

    /**
     * This is to like user post (reload page)
     *
     * @param $id - Post.id
     * @param $comment
     * @param $profile
     * @return void
     */
    public function like($id, $comment = null, $profile = null)
    {
        $this->autoRender = false;
        if ($this->request->is('get')) {
            $this->Like->id = $this->Like->field('id', ['user_id' => $this->Auth->user('id'), 'post_id' => $id]);
            if ($this->Like->id) {
                $this->Like->delete($this->Like->id);
            } else {
                $this->Like->create(['user_id' => $this->Auth->user('id'), 'post_id' => $id]);
                $this->Like->save();
            }
            if ($comment) {
                $controller = $comment == 'comment' ? 'posts' : 'users';
                $action = $comment == 'comment' ? 'comments/' . $id : 'profile/' . $profile;
            } else {
                $controller = 'posts';
                $action = '/';
            }
            return $this->redirect(array('controller' => $controller,'action' => $action));
        }
    }

    /**
     * This is to like user post (ajax)
     *
     * @return void
     */
    public function ajax_like()
    {
        $this->autoRender=false;
        if ($this->request->is('post')) {
            $data = ['user_id' => $this->Auth->user('id'), 'post_id' => $this->request->data('id')];
            $this->Like->id = $this->Like->field('id', $data);
            if ($this->Like->id) {
                $this->Like->delete($this->Like->id);
                $result = ['status' => 'unlike', 'message' => 'Post Unliked'];
            } else {
                $this->Like->create($data);
                $this->Like->save();
                $result = ['status' => 'like', 'message' => 'Post Liked'];
            }
            return json_encode($result);
        }
    }

    /**
     * This is to view comments on post
     *
     * @param $id - Post.id
     * @return void
     */
    public function comments($id = null)
    {
        if (!$this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'users', 'action' => 'login']));
        }
        if (!intval($id) || !($this->Post->field('id', ['id' => $id]))) {
            $this->Session->setFlash('Invalid Post!', 'flash_error');
            return $this->redirect(array('action' => '/'));
        }
        if ($this->request->is('get')) {
            $this->paginate = [
                'conditions' => [
                    'Post.id' => $id,
                    'Post.deleted != 1'
                ],
                'order' => ['Post.modified' => 'desc']
            ];
            $this->set('posts', $this->paginate('Post'));
            
            $this->paginate = [
                    'fields' => ['Comment.id', 'Comment.comment', 'Comment.user_id', 'Comment.created', 'Comment.modified', 'User.username', 'User.profile'],
                    'joins' => [
                        [
                            'table' => 'users',
                            'alias' => 'User',
                            'type' => 'LEFT',
                            'conditions' => 'User.id = Comment.user_id'
                        ]
                    ],
                    'conditions' => ['Comment.post_id' => $id, 'Comment.deleted' => 0],
                    'order' => ['Comment.created' => 'desc']
            ];
            $this->set('comments', $this->paginate('Comment'));
        }
    }

    /**
     * This is to edit comment on post
     *
     * @param $id - Comment.id
     * @param $postId - Comment.post_id
     * @return void
     */
    public function edit_comment($id, $postId)
    {
        if ($this->request->is('post')) {
            $data = ['comment' => $this->request->data['Comment']['comment']];
            $this->Comment->id = $this->Comment->field('id', ['id' => $id, 'user_id' => $this->Auth->user('id'), 'post_id' =>  $postId]);
            if ($this->Comment->id) {
                if ($this->Comment->set($data)) {
                    $this->Comment->save();
                    $this->Session->setFlash('Comment Successfully Updated.', 'flash_success');
                } else {
                    $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                }
            } else {
                $this->Session->setFlash('Invalid Comment!', 'flash_error');
            }
        }
        return $this->redirect(array('action' => 'comments/'. $postId));
    }

    /**
     * This is to remove comments on post
     *
     * @param $id - Comment.id
     * @param $postId - Comment.post_id
     * @return void
     */
    public function remove_comment($id, $postId)
    {
        date_default_timezone_set('Asia/Manila');
        if ($this->request->is('get')) {
            $data = ['deleted' => 1, 'deleted_date' => date('Y-m-d H:i:s')];
            $this->Comment->id = $this->Comment->field('id', ['id' => $id, 'user_id' => $this->Auth->user('id'), 'post_id' =>  $postId]);
            if ($this->Comment->id) {
                if ($this->Comment->set($data)) {
                    $this->Comment->save();
                    $this->Session->setFlash('Comment Successfully Removed.', 'flash_success');
                } else {
                    $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                }
            } else {
                $this->Session->setFlash('Invalid Comment!', 'flash_error');
            }
        }
        return $this->redirect(array('action' => 'comments/'. $postId));
    }

    /**
     * This is to retweet user post
     *
     * @param $id - Post.id
     * @param $retweet
     * @return void
     */
    public function retweet_post($id, $retweet = null) {
        if (!$this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'users', 'action' => 'login']));
        }
        if (!$id) {
            $this->Session->setFlash('Invalid Post!', 'flash_error');
            return $this->redirect(array('action' => '/'));
        }
        if ($this->request->is('post')) {
            $filename = null;
            if (!empty($this->request->data['Post']['image']['tmp_name'])) {
                $filename= basename($this->request->data['Post']['image']['name']);
                move_uploaded_file($this->request->data['Post']['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'posts' . DS . $filename);
            }
            $data = [
                'user_id' => $this->Auth->user('id'),
                'title' => h($this->request->data['Post']['title']),
                'content' => h($this->request->data['Post']['content']),
                'image' => $filename,
                'is_retweet' => 1,
                'original_post_id' => $id,
                'deleted_date' => null
            ];
            $this->Post->create($data);
            $this->Post->set('data', $data);
            if ($this->Post->save()) {
                $this->Session->setFlash('Retweet Success.', 'flash_success');
            } else {
                $this->Session->setFlash('Something Went Wrong.', 'flash_error');
            }
            $action = $retweet == null ? '/' : 'comments/' . $id;
            return $this->redirect(array('action' => '/'));
        }
    }

    /**
     * Destroy Session of the logged in user
     *
     * @return void
     */
    public function signout()
    {
        $this->Session->destroy();
        $this->redirect(['controller' => 'users', 'action' => 'login']);
    }

    /**
     * View search user/s
     *
     * @return void
     */
    public function search()
    {
        if (!$this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'users', 'action' => 'login']));
        }
        if ($this->request->is('post')) {
            $search = h($this->request->data['Search']['search']);
            $this->paginate = [
                'User' => [
                    'fields' => ['User.id', 'User.first_name', 'User.last_name', 'User.username', 'User.profile', 'Follow.id', 'Follow.user_id', 'Follow.following_id'],
                    'joins' => [
                        [
                            'table' => 'followers',
                            'alias' => 'Follow',
                            'type' => 'LEFT',
                            'conditions' => ['Follow.user_id = ' . $this->Auth->user('id'), 'Follow.following_id = User.id']
                        ]
                    ],
                    'conditions' => [
                        'OR' => [
                            'User.first_name LIKE' => "%$search%",
                            'User.last_name LIKE' => "%$search%",
                            'User.username LIKE' => "%$search%"
                        ],
                        'User.deleted !=' => 1,
                        'User.activated ' => 1
                    ],
                    'order' => ['User.username']
                ]
            ];
            $this->set('title',  $search);
            $this->set('searches',  $this->paginate('User'));
        } else {
            $this->redirect($this->Auth->redirect(['controller' => 'posts', 'action' => '/']));
        }
    }
}
