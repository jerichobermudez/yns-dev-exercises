<?php
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppController', 'Controller');

class UsersController extends AppController
{
    public $name = 'Users';
    public $uses = array('Post', 'Like', 'User', 'Comment', 'Follower');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'signup', 'signout', 'activation', 'forgot', 'profile');
    }
    /**
     * Authenticate the user
     *
     * @return void
     */
    public function login()
    {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                if (intval($this->Auth->user('activated')) === 1) { 
                    $this->redirect($this->Auth->redirect(['controller' => 'posts', 'action' => '/']));
                } else {
                    $this->Session->destroy();
                    $this->Session->setFlash(__('Please Activate your Account First! Check your email to activate your account'), 'flash_error');
                }
            } else {
                $this->Session->setFlash(__('Invalid Login Credentials'), 'flash_error');
            }
        }
    }

    /**
     * Destroy Session of the logged in user
     *
     * @return void
     */
    public function signout()
    {
        $this->redirect($this->Auth->signout());
    }

    public function index()
    {
        $this->User->recursive = 0;
        $this->set('user', $this->User->find('all'));
        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }
    
    /**
     * Create or Register new user
     *
     * @return void
     */
    public function signup()
    {
        if ($this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'posts', 'action' => '/']));
        }
        $activationCode = 'ABCDEFGHIJKLMNOPQRSYUVWXYZ1234567890';
        $activationCode = substr(str_shuffle($activationCode), 0, 8);
        if ($this->request->is('post')) {
            $data = [
                'first_name' => $this->request->data['User']['first_name'],
                'last_name' => $this->request->data['User']['last_name'],
                'username' => $this->request->data['User']['username'],
                'email' => $this->request->data['User']['email'],
                'password' => $this->request->data['User']['password'],
                'repassword' => $this->request->data['User']['repassword'],
                'activation_code' => $activationCode,
            ];
            $this->User->create($data);
            $this->User->set('data', $data);
            if ($this->User->save()) {
                $folder = new Folder();
                $path = WWW_ROOT . DS . 'users/';
                $userDir = $path . $data['username'] . DS;
                if (!file_exists($folder->create($path))) { $folder->create($path); }
                if (!file_exists($folder->create($userDir))) { $folder->create($userDir); }
                if (!file_exists($folder->create($userDir . 'posts'))) { $folder->create($userDir . 'posts'); }
                if (!file_exists($folder->create($userDir . 'profile'))) {
                    $folder->create($userDir . 'profile');
                    $file = new File(WWW_ROOT . DS . 'img/default.png');
                    if ($file->exists()) {
                        $dir = new Folder($userDir . 'profile', true);
                        $file->copy($dir->path . DS . $file->name);
                    }
                }
                $activationUrl = Router::url(['controller' => 'users', 'action' => 'activation']);
                $mail = new CakeEmail('gmail');
                $mail->from(['yns@microblog.com' => 'MicroBlog']);
                $mail->to($data['email']);
                $mail->subject('Activate Account');
                $message = 'Hi ' . $data['first_name'] . ", \n\nYou recently register to Microblog.\nActivation Code: " . $data['activation_code'] . ". Click the link below to Activate your account\n\nLink: http://". $_SERVER['HTTP_HOST'] ."/users/activation\n\nThanks";
                $mail->send($message);
                $this->Session->destroy();
                $this->Session->setFlash('Registration Success! Please Check your Email to Activate your Account!', 'flash_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            } else {
                $this->Session->destroy();
                $this->Session->setFlash('Something went Wrong! Please try again.', 'flash_error');
            }
        }
    }
    
    /**
     * Activate user account to proceed to login
     *
     * @return void
     */
    public function activation()
    {
        if ($this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'posts', 'action' => '/']));
        }
        if ($this->request->is('post')) {
            $activationCode = $this->request->data['User']['activation_code'];
            $this->User->id = $this->User->field('id', ['activated' => 0, 'activation_code' => $activationCode]);
            if ($this->User->id) {
                $this->User->set(['activated' => 1, 'activation_code' => null]);
                $this->User->save();
                $this->Session->setFlash('Account Activation Success', 'flash_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            } else {
                $this->Session->setFlash('Activation Code Error! Please Check your Email for your Activation Code', 'flash_error');
                return $this->redirect(array('controller' => 'users', 'action' => 'activation'));
            }
        }
    }

    /**
     * Forgot password
     *
     * @return void
     */
    public function forgot()
    {
        if ($this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'posts', 'action' => '/']));
        }
        if ($this->request->is('post')) {
            $email = $this->request->data['User']['email'];
            $str = 'ABCDEFGHIJKLMNOPQRSYUVWXYZ1234567890';
            $newPassword = substr(str_shuffle($str), 0, 6);
            $data = [
                'password' => $newPassword,
                'repassword' => $newPassword
            ];
            $this->User->id = $this->User->field('id', ['email' => $email]);
            if ($this->User->id) {
                $this->User->set($data);
                $this->User->save();
                $mail = new CakeEmail('gmail');
                $mail->from(['yns@microblog.com' => 'MicroBlog']);
                $mail->to($email);
                $mail->subject('Forgot Password');
                $message = "You've recently request for new password to your Microblog Account.\nNew Password: " . $newPassword . ".\n\nClick the link below to Login your Account\n\nLink: http://". $_SERVER['HTTP_HOST'] ."/users/login\n\nThanks";
                $mail->send($message);
                $this->Session->setFlash('Password Request Success! Please check your email for your New Password', 'flash_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            } else  {
                $this->Session->setFlash('Something went wrong! Please try again', 'flash_error');
            }
        }
    }

    /**
     * Edit the user
     *
     * @return void
     */
    public function edit()
    {
        $this->User->id = $this->Auth->user('id');
        if (!$this->User->exists()) {
            $this->Session->setFlash('User Not Found!');
            $this->redirect(['action' => 'profile']);
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('User Saved Successfully');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash('Something went Wrong! Please try again.');
            }
        } else {
            $this->request->data = $this->User->read();
            $this->set('user', $this->User->find('first', ['conditions' => ['id' => $this->Auth->user('id')]]));
        }
    }

    /**
     * Update the user profile photo
     *
     * @return void
     */
    public function update_photo()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Profile']['image']['tmp_name'])) {
                $filename= basename($this->request->data['Profile']['image']['name']);
                $this->User->id = $this->User->field('id', ['id' => $this->Auth->user('id')]);
                if ($this->User->set(['profile' => $filename])) {
                    $this->User->save();
                    move_uploaded_file($this->request->data['Profile']['image']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'profile' . DS . $filename);
                    $this->Session->setFlash('Profile Successfully Updated', 'flash_success');
                } else {
                    $this->Session->setFlash('Something went wrong. Please try again!', 'flash_error');
                }
            } else {
                $this->Session->setFlash('No Changes Made!', 'flash_info');
            }
        }
        $this->redirect(['controller' => 'users', 'action' => 'edit']);
    }

    /**
     * Get logged in user information
     * 
     * @param $id - user_id
     * @return void
     */
    public function getUser($id = null)
    {
        $userID = $id ? $id : $this->Auth->user('id');
        $data = $this->User->find('first', ['conditions' => ['id' => $userID]]);
        return $data['User'];
    }

    /**
     * Update the user profile information
     *
     * @return void
     */
    public function update_info()
    {
        if ($this->request->is('post')) {
            $data = [
                'first_name' => $this->request->data['Profile']['updt_first_name'],
                'last_name' => $this->request->data['Profile']['updt_last_name'],
                'email' => $this->request->data['Profile']['updt_email']
            ];
            $this->User->id = $this->User->field('id', ['id' => $this->Auth->user('id')]);
            $email = $this->User->find('all', ['conditions' => ['id !=' => $this->Auth->user('id'), 'email' => $data['email']]]);
            if ($email) {
                $this->Session->setFlash('Something went wrong. Please Try Again!', 'flash_error');
            } else {
                if ($this->User->set($data)) {
                    $this->User->save();
                    $this->Session->setFlash('Profile Successfully Updated', 'flash_success');
                } else {
                    $this->Session->setFlash('Something went wrong. Please Try Again!', 'flash_error');
                }
            }
        }
        $this->redirect(['controller' => 'users', 'action' => 'edit']);
    }

    /**
     * Update the user password
     *
     * @return void
     */
    public function update_password()
    {
        if ($this->request->is('post')) {
            $data = [
                'old_password' => $this->request->data['Profile']['old_password'],
                'password' => $this->request->data['Profile']['new_password'],
                'repassword' => $this->request->data['Profile']['retype_new_password']
            ];
            $this->User->id = $this->User->field('id', ['id' => $this->Auth->user('id')]);
            if ($this->User->id) {
                $this->User->password = $this->User->field('password', ['id' => $this->Auth->user('id')]);
                if ($this->User->password != AuthComponent::password($data['old_password'])) {
                    $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                } elseif (AuthComponent::password($data['password']) != AuthComponent::password($data['repassword'])) {
                    $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                } else {
                    if ($this->User->set($data)) {
                        $this->User->save();
                        $this->Session->setFlash('Password Successfully Updated');
                    } else {
                        $this->Session->setFlash('Something went wrong Please Try Again!', 'flash_error');
                    }
                }
            }
        }
        $this->redirect(['controller' => 'users', 'action' => 'edit']);
    }

    /**
     * Update the user profile photo
     *
     * @param $id - user_id
     * @return void
     */
    public function profile($id = null)
    {
        if (!$this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'users', 'action' => 'login']));
        }
        $id = $id ? $id : $this->Auth->user('id');
        $this->paginate = [
            'User' => [
                'fields' => ['User.id', 'User.username', 'User.profile', 'Follow.id', 'Follow.user_id', 'Follow.following_id'],
                'joins' => [
                    [
                        'table' => 'followers',
                        'alias' => 'Follow',
                        'type' => 'LEFT',
                        'conditions' => ['Follow.user_id = ' . $this->Auth->user('id'), 'Follow.following_id = ' . $id]
                    ]
                ],
                'conditions' => ['User.id' => $id]
            ]
        ];
        $this->set('user',  $this->paginate('User'));

        $this->paginate = [
            'conditions' => [
                'Post.user_id' => $id,
                'Post.deleted != 1'
            ],
            'order' => ['Post.modified' => 'desc']
        ];
        $this->set('posts', $this->paginate('Post'));
    }

    /**
     * This function is to follow user (with reload)
     *
     * @param $id - user_id
     * @param $profile
     * @return void
     */
    public function follow_user($id, $profile = null)
    {
        $this->Follower->id = $this->Follower->field('id', ['user_id' => $this->Auth->user('id'), 'following_id' => $id]);
        if ($this->Follower->id) {
            $this->Follower->delete($this->Follower->id);
        } else {
            $this->Follower->create(['user_id' => $this->Auth->user('id'), 'following_id' => $id]);
            $this->Follower->save();
        }
        $action = $profile ? 'follows' : 'profile/'.$id;
        $this->redirect(['action' => $action]);
    }

    /**
     * This function is to follow user (ajax)
     *
     * @return void
     */
    public function ajax_follow()
    {
        $this->autoRender=false;
        if ($this->request->is('post')) {
            $data = ['user_id' => $this->Auth->user('id'), 'following_id' => $this->request->data('id')];
            $this->Follower->id = $this->Follower->field('id', $data);
            if ($this->Follower->id) {
                $this->Follower->delete($this->Follower->id);
                $result = ['status' => 'unfollow', 'message' => 'User Unfollowed'];
            } else {
                $this->Follower->create($data);
                $this->Follower->save();
                $result = ['status' => 'follow', 'message' => 'User Followed'];
            }
            return json_encode($result);
        }
    }

    /**
     * This function is to view user followers / following
     *
     * @return void
     */
    public function follows()
    {
        if (!$this->Auth->login()) {
            $this->redirect($this->Auth->redirect(['controller' => 'users', 'action' => 'login']));
        }
        $this->paginate = [
            'Follower' => [
                'fields' => ['User.id', 'User.username', 'User.profile', 'Follower.user_id', 'Follower.following_id'],
                'joins' => [
                    [
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => ['User.id = Follower.user_id', 'Follower.following_id = ' . $this->Auth->user('id')]
                    ]
                ],
                'conditions' => ['User.id !=' => $this->Auth->user('id')]
            ]
        ];
        $this->set('followers',  $this->paginate('Follower'));
        
        $this->paginate = [
            'Follower' => [
                'fields' => ['User.id', 'User.username', 'User.profile', 'Follower.user_id', 'Follower.following_id'],
                'joins' => [
                    [
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => ['Follower.following_id = User.id', 'Follower.user_id='.$this->Auth->user('id')]
                    ]
                ],
                'conditions' => ['User.id !=' => $this->Auth->user('id')]
            ]
        ];
        $this->set('followings',  $this->paginate('Follower'));
    }
}
