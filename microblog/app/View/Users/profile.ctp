<?= $this->element('sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-header with-border attachment-block clearfix">
                        <?= $this->Session->flash(); ?>
                        <div class="box-body">
                            <?= $this->Html->image('../users/'. $user[0]['User']['username'] . '/profile/' . $user[0]['User']['profile'], ['class' => 'attachment-img']) ?>
                            <div class="attachment-pushed">
                                <h3 class="attachment-heading"><?= $user[0]['User']['username'] ?></h3>
                                <?php
                                    if ($current_user['id'] === $user[0]['User']['id']) {
                                        echo $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-pencil']) . ' Edit Profile', ['controller' => 'users', 'action' => 'edit'], ['escape' => false]) ?>
                                        <h4>
                                            <?= $this->Html->link('Followers / Following', ['controller' => 'users', 'action' => 'follows'], ['class' => 'btn btn-flat btn-xs theme-button']); ?>
                                        </h4>
                                    <?php } else {
                                        if ($user[0]['Follow']['user_id'] === $current_user['id'] && $user[0]['Follow']['following_id'] === $user[0]['User']['id']) { ?>
                                            <h4><button id="follow<?= $user[0]['User']['id'] ?>" class="btn btn-flat btn-xs theme-button" onClick="ajax_follow_unfollow('<?= $user[0]['User']['id'] ?>', '<?= Router::url(['controller' => 'users', 'action' => 'ajax_follow']) ?>')"> Unfollow</button></h4>
                                        <?php } else { ?>
                                            <h4><button id="follow<?= $user[0]['User']['id'] ?>" class="btn btn-flat btn-xs theme-button" onClick="ajax_follow_unfollow('<?= $user[0]['User']['id'] ?>', '<?= Router::url(['controller' => 'users', 'action' => 'ajax_follow']) ?>')"> Follow</button></h4>
                                        <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (!$posts) { ?>
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-header">
                        <h4><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Post to Display!</h4>
                    </div>
                </div>
            </div>
            <?php } else { foreach ($posts as $post) { ?>
                <div class="col-lg-12 col-xs-12">
                    <div class="box box-widget flat">
                        <div class="box-header with-border">
                            <div class="user-block">
                                <?= $this->Html->image('../users/'. $post['User']['username'] . '/profile/' . $post['User']['profile'], ['class' => 'img-circle', 'width' => '128px']) ?>
                                <span class="username"><?= $post['User']['username'] ?></span>
                                <i><span class="description"> <?= $timestamp = date('m-d-y', strtotime($post['Post']['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post['Post']['created'])) : date('D, M d, Y h:i A', strtotime($post['Post']['created'])); ?> </span></i>
                            </div>
                            <div class="box-tools">
                                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimize"><i class="glyphicon glyphicon-minus"></i></button>
                                <?php if ($post['Post']['user_id'] === $current_user['id']) { ?>
                                <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-edit']), ['controller' => 'posts', 'action' => 'edit', $post['Post']['id']], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Edit', 'escape' => false]) ?>
                                <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-remove']), ['controller' => 'posts', 'action' => 'delete', $post['Post']['id'], 'profile'], ['onClick' => "return confirm('Are you Sure you want to Remove this Post?')", 'class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Remove', 'escape' => false]) ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="box-body">
                            <b><span class=""> <?= ucwords($post['Post']['title']) ?> </span></b><br><br>
                            <p><?= $post['Post']['content'] ?></p>
                            <?php if (!empty($post['Post']['image'])) { ?>
                            <div class="attachment-block clearfix"><b>Attachment:</b><br>
                                <?= $this->Html->image('../users/'. $post['User']['username'] . '/posts/' . $post['Post']['image'], ['class' => 'img-thumbnail', 'width' => '100px']) ?>
                            </div>
                            <?php } ?>
                            <?php
                                if (intval($post['Post']['is_retweet']) === 1) { 
                                    if (intval($post['Original']['deleted']) === 1) { ?>
                                        <div class="callout callout-warning">
                                            <h5><span class="glyphicon glyphicon-info-sign"></span> This Post has been Removed by the Owner</h5>
                                        </div>
                                    <?php } else {
                                        $user = $this->requestAction(['controller' => 'users', 'action' => 'getUser', $post['Original']['user_id']]);
                                        ?>
                                        <div class="box box-body" style="padding-left: 45px; background: rgba(243, 156, 18, 0.9);">
                                            <?= $this->Html->image('../users/' . $user['username'] . '/profile/' . $user['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $user['username']]) ?>
                                            <div class="img-push">
                                                <span class="username"><?= $this->Html->link($user['username'], ['controller' => 'users', 'action' => 'profile', $user['id']], ['style' => 'color:;']) ?></span>
                                                <i><span class="description" style="font-size: 11px;"> <?= $timestamp = date('m-d-y', strtotime($post['Original']['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post['Original']['created'])) : date('D, M d, Y h:i A', strtotime($post['Original']['created'])); ?> </span></i> &nbsp; <span class="icon icon-twitter"></span><br>
                                                <p class="description"> <b><?= ucwords($post['Original']['title']) ?></b> </p>
                                                <p class="description"> <?= $post['Original']['content'] ?> </p>
                                                <?php if (!empty($post['Original']['image'])) { ?>
                                                <div class="description"><b style="font-size:8pt;">Attachment:</b><br>
                                                    <?= $this->Html->image('../users/'. $user['username'] . '/posts/' . $post['Original']['image'], ['class' => 'img-thumbnail', 'width' => '40px']) ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            ?>
                            <div class="box-footer" style="padding-top:8px!important; padding: 0px;">
                                <?php
                                    if ($post['Like']['user_id'] == $current_user['id']) { ?>
                                        <button id="like<?= $post['Post']['id'] ?>" class="btn btn-default btn-xs active" onClick="ajax_like_unlike('<?= $post['Post']['id'] ?>', '<?= Router::url(['controller' => 'posts', 'action' => 'ajax_like']) ?>')"> <i class="glyphicon glyphicon-thumbs-down"></i> Unlike</button>
                                    <?php } else { ?>
                                        <button id="like<?= $post['Post']['id'] ?>" class="btn btn-default btn-xs" onClick="ajax_like_unlike('<?= $post['Post']['id'] ?>', '<?= Router::url(['controller' => 'posts', 'action' => 'ajax_like']) ?>')"> <i class="glyphicon glyphicon-thumbs-up"></i> Like</button>
                                    <?php }
                                ?>
                                <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'icon icon-comments']) . ' Comment ', ['controller' => 'posts', 'action' => 'comments', $post['Post']['id']], ['class' => 'btn btn-default btn-xs', 'escape' => false]); ?>
                                <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'icon icon-twitter']) . ' Retweet ', ['class' => 'btn btn-default btn-xs', 'data-toggle' => 'modal', 'data-target' => '#retweet'.$post['Post']['id']], ['escape' => false]) ?>
                                <!-- <span class="pull-right text-muted">127 likes - 3 comments</span> -->
                            </div>
                        </div>
                        <div class="box-footer" style="padding-left: 25px">
                            <?php echo $this->Form->create('Comment', ['url' => ['controller' => 'comments', 'action' => 'post_comment']]); ?>
                                <?= $this->Html->image('../users/' . $current_user['username'] . '/profile/' . $current_user['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $current_user['username']]) ?>
                                <div class="img-push">
                                <?= $this->Form->hidden('post_id', ['value' => $post['Post']['id']]); ?>
                                    <?= $this->Form->input('comment', ['label' => false, 'id' => $post['Post']['id'], 'class' => 'form-control input-sm comment-to-post', 'data-id' => $post['Post']['id'], 'data-value' => Router::url(['controller' => 'posts', 'action' => 'post_comment']), 'placeholder' => 'Comment', 'autocomplete' => 'off']) ?>
                                </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="retweet<?= $post['Post']['id']?>" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title text-yellow"><span class="icon icon-twitter"></span> Retweet Post</h4>
                            </div>
                            <?= $this->Form->create('Post', ['url' => ['controller' => 'posts', 'action' => 'retweet_post', $post['Post']['id']], 'type' => 'file']) ?>
                            <div class="modal-body">
                                <div class="attachment-block clearfix">
                                    <div class="user-block">
                                        <?= $this->Html->image('../users/'. $post['User']['username'] . '/profile/' . $post['User']['profile'], ['class' => 'img-circle', 'width' => '128px']) ?>
                                        <span class="username"><?= $post['User']['username'] ?></span>
                                        <i><span class="description"> <?= $timestamp = date('m-d-y', strtotime($post['Post']['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post['Post']['created'])) : date('D, M d, Y h:i A', strtotime($post['Post']['created'])); ?> </span></i>
                                    </div>
                                    <b><span class=""> <?= ucwords($post['Post']['title']) ?> </span></b><br><br>
                                    <p><?= $post['Post']['content'] ?></p>
                                    <?php if (!empty($post['Post']['image'])) { ?>
                                    <div class="attachment-block clearfix"><b>Attachment:</b><br>
                                        <?= $this->Html->image('../users/'. $post['User']['username'] . '/posts/' . $post['Post']['image'], ['class' => 'img-thumbnail', 'width' => '100px']) ?>
                                    </div>
                                    <?php } ?>
                                </div><br>
                                <?= $this->Form->input('title', ['label' => false, 'class' => 'form-control form-group', 'placeholder' => 'Title']) ?>
                                <?= $this->Form->textarea('content', ['class' => 'form-control form-group', 'rows' => '3', 'placeholder' => 'What\'s on your mind?']) ?>
                                <?= $this->Form->file('image', ['label' => false, 'class' => 'btn btn-md btn-flat pull-left theme-button', 'onChange' => 'preview'.$post['Post']['id'].'(event)']) ?>
                                <br><br>
                                <img src="" id="retweetPreview<?= $post['Post']['id'] ?>" class="pull-left" width="100px">
                                <script type="text/javascript">
                                    var preview<?= $post['Post']['id']?> = function(event) {
                                        var output = document.getElementById("retweetPreview<?= $post['Post']['id'] ?>");
                                        output.src = '' + URL.createObjectURL(event.target.files[0]);
                                    };
                                </script><br><br>
                            </div>
                            <div class="modal-footer">
                                <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
                                <?= $this->Form->button('Retweet', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                            </div>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
            <?php } } ?>
    </section>
    </div>
</div>