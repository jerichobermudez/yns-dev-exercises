<section class="content margin">
    <div class="col-lg-5 col-lg-offset-7 col-sm-7 col-sm-offset-5 col-xs-12">
        <div id="login" class="box box-warning">
            <div class="box-body">
                <h1 class="text-center"> Activate Account? </h1>
                <div class="margin">
                    <?= $this->Session->flash(); ?>
                    <?= $this->Form->create('User') ?>
                    <?= $this->Form->input('activation_code', ['label' => 'Activation Code:', 'class' => 'form-control form-group', 'autocomplete' => 'off', 'autofocus' => true]) ?>
                    <span class="text-center"> <?= $this->Form->submit('SUBMIT', ['id' => 'loginBtn', 'class' => 'btn btn-md btn-flat form-group theme-button']) ?> </span>
                    <br>
                    <p> Already have an Account? Click <?= $this->Html->Link('here', ['action' => 'login'], ['id' => 'loginLink']); ?> to Sign In. </p>
                    <p> Don't have an Account? Click <?= $this->Html->Link('here', ['action' => 'signup'], ['id' => 'signupLink']); ?> to Register. </p>
                    <p> <?= $this->Html->Link('Forgot Password?', ['action' => 'login'], ['id' => 'forgotLink']); ?> </p>
                </div>
            </div>
        </div>
    </div>
</section>