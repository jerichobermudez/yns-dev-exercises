<?= $this->element('sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-header">
                        <h3 class="box-title">Edit Account:</h3>
                    </div>
                    <div class="box-body">
                        <?= $this->Session->flash(); ?>
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="panel panel-warning flat">
                                    <div class="panel-heading"><span class = "glyphicon glyphicon-picture"></span> Change Profile</div>
                                    <div class="panel-body">
                                        <?= $this->Form->create('Profile', ['url' => ['controller' => 'users', 'action' => 'update_photo'], 'type' => 'file']) ?>
                                        <?= $this->Html->image('../users/'. $user['User']['username'] . '/profile/' . $user['User']['profile'], ['id' => 'profilePreview', 'class' => '', 'width' => '100%']) ?>
                                        <?= $this->Form->file('image', ['label' => false, 'class' => 'form-group form-control', 'onChange' => 'preview(event)', 'required' => 'true']) ?>
                                        <?= $this->Form->submit('Update Photo', ['class' => 'btn btn-md btn-flat btn-block theme-button']) ?>
                                        <?= $this->Form->end(); ?>
                                        <script type="text/javascript">
                                            var preview = function(event) {
                                                var output = document.getElementById('profilePreview');
                                                output.src = '' + URL.createObjectURL(event.target.files[0]);
                                            };
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-xs-12">
                                <div class="panel panel-warning flat">
                                    <div class="panel-heading"><span class = "glyphicon glyphicon-info-sign"></span> Update Information</div>
                                        <div class="panel-body">
                                            <?= $this->Form->create('Profile', ['url' => ['controller' => 'users', 'action' => 'update_info']]) ?>
                                            <?= $this->Form->input('updt_first_name', ['label' => 'Firstname:', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Firstname', 'value' => $user['User']['first_name'], 'autocomplete' => 'off']) ?>
                                            <?= $this->Form->input('updt_last_name', ['label' => 'Lastname:', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Lastname', 'value' => $user['User']['last_name'], 'autocomplete' => 'off']) ?>
                                            <?= $this->Form->input('updt_username', ['label' => 'Username:', 'class' => 'form-control form-group', 'placeholder' => 'Lastname', 'value' => $user['User']['username'], 'readonly', 'autocomplete' => 'off']) ?>
                                            <?= $this->Form->input('updt_email', ['label' => 'Email:', 'class' => 'form-control form-group', 'required' => true, 'placeholder' => 'Email', 'value' => $user['User']['email']]) ?>
                                            <?= $this->Form->submit('Update Information', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                                            <?= $this->Form->end(); ?>
                                    </div>
                                </div>
                                <div class="panel panel-warning flat">
                                    <div class="panel-heading"><span class = "glyphicon glyphicon-lock"></span> Change Password</div>
                                        <div class="panel-body">
                                            <?= $this->Form->create('Profile', ['url' => ['controller' => 'users', 'action' => 'update_password']]) ?>
                                            <?= $this->Form->input('old_password', ['label' => 'Old Password:', 'type' => 'password', 'class' => 'form-control form-group', 'placeholder' => 'Old Password']) ?>
                                            <?= $this->Form->input('new_password', ['label' => 'New Password:', 'type' => 'password', 'class' => 'form-control form-group', 'placeholder' => 'New Password']) ?>
                                            <?= $this->Form->input('retype_new_password', ['label' => 'Re-type New Password:', 'type' => 'password', 'class' => 'form-control form-group', 'placeholder' => 'Re-type New Password']) ?>
                                            <?= $this->Form->submit('Update Password', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                                            <?= $this->Form->end(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>