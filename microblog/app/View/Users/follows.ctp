<?= $this->element('sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <fieldset>
                                    <legend>Followers:</legend>
                                    <?php if ($followers == null) { ?>
                                        <h4><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Followers</h4>
                                    <?php } else {
                                        foreach ($followers as $follower) { ?>
                                        <div class="user-panel box-header with-border">
                                            <div class="pull-left image">
                                                <?= $this->Html->image('../users/'. $follower['User']['username'] . '/profile/' . $follower['User']['profile'], ['class' => 'img-circle']) ?>
                                            </div>
                                            <div class="pull- info">
                                                <p><?= $this->Html->link($follower['User']['username'], ['controller' => 'users', 'action' => 'profile', $follower['User']['id']]) ?></p>
                                                <?php
                                                    if ($follower['Follower']['user_id'] === $current_user['id'] && $follower['Follower']['following_id'] === $follower['User']['id']) { ?>
                                                        <?= $this->Html->link('Unfollow', ['controller' => 'users', 'action' => 'follow_user', $follower['User']['id'], 1], ['class' => 'btn btn-flat btn-xs theme-button']); ?>
                                                    <?php } else { ?>
                                                        <?= $this->Html->link('Follow', ['controller' => 'users', 'action' => 'follow_user', $follower['User']['id'], 1], ['class' => 'btn btn-flat btn-xs theme-button']); ?>
                                                    <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php 
                                        }
                                    } 
                                    ?>
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <fieldset>
                                    <legend>Following:</legend>
                                    <?php if ($followings == null) { ?>
                                        <h4><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Following</h4>
                                    <?php } else {
                                        foreach ($followings as $following) { ?>
                                        <div class="user-panel box-header with-border">
                                            <div class="pull-left image">
                                                <?= $this->Html->image('../users/'. $following['User']['username'] . '/profile/' . $following['User']['profile'], ['class' => 'img-circle']) ?>
                                            </div>
                                            <div class="pull- info">
                                                <p><?= $this->Html->link($following['User']['username'], ['controller' => 'users', 'action' => 'profile', $following['User']['id']]) ?></p>
                                                <?php
                                                    if ($following['Follower']['user_id'] === $current_user['id'] && $following['Follower']['following_id'] === $following['User']['id']) { ?>
                                                        <?= $this->Html->link('Unfollow', ['controller' => 'users', 'action' => 'follow_user', $following['User']['id'], 1], ['class' => 'btn btn-flat btn-xs theme-button']); ?>
                                                    <?php } else { ?>
                                                        <?= $this->Html->link('Follow', ['controller' => 'users', 'action' => 'follow_user', $following['User']['id'], 1], ['class' => 'btn btn-flat btn-xs theme-button']); ?>
                                                    <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php 
                                        }
                                    } 
                                    ?>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>