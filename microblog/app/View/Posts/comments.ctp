<?= $this->element('sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <?php foreach ($posts as $post): ?>
                    <div class="box-header with-border">
                        <?= $this->Session->Flash() ?>
                        <div class="user-block">
                            <?= $this->Html->image('../users/'. $post['User']['username'] . '/profile/' . $post['User']['profile'], ['class' => 'img-circle', 'width' => '128px']) ?>
                            <span class="username"><?= $posts[0]['User']['username'] ?></span>
                            <i><span class="description"> <?= $timestamp = date('m-d-y', strtotime($post['Post']['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post['Post']['created'])) : date('D, M d, Y h:i A', strtotime($post['Post']['created'])); ?> </span></i>
                        </div>
                        <div class="box-tools">
                            <?php if ($post['Post']['user_id'] === $current_user['id']) { ?>
                            <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-edit']), ['action' => 'edit', $post['Post']['id']], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Edit', 'escape' => false]) ?>
                            <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-remove']), ['action' => 'delete', $post['Post']['id']], ['onClick' => "return confirm('Are you Sure you want to Remove this Post?')", 'class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Remove', 'escape' => false]) ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-body">
                        <b><span class=""> <?= h(ucwords($post['Post']['title'])) ?> </span></b><br><br>
                        <p><?= h($post['Post']['content']) ?></p>
                        <?php if (!empty($post['Post']['image'])) { ?>
                        <div class="attachment-block clearfix"><b>Attachment:</b><br>
                            <?php echo $this->Html->image('../users/'. $post['User']['username'] . '/posts/' . $post['Post']['image'], ['class' => 'img-thumbnail', 'width' => '100px']) ?>
                        </div>
                        <?php } 
                             if (intval($post['Post']['is_retweet']) === 1) {
                                if (intval($post['Original']['deleted']) === 1) { ?>
                                    <div class="callout callout-warning">
                                        <h5><span class="glyphicon glyphicon-info-sign"></span> This Post has been Removed by the Owner</h5>
                                    </div>
                                <?php } else {
                                    $user = $this->requestAction(['controller' => 'users', 'action' => 'getUser', $post['Original']['user_id']]);
                                    ?>
                                    <div class="box box-body" style="padding-left: 45px; background: rgba(243, 156, 18, 0.8);">
                                        <?= $this->Html->image('../users/' . $user['username'] . '/profile/' . $user['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $user['username']]) ?>
                                        <div class="img-push">
                                            <span class="username"><?= $this->Html->link($user['username'], ['controller' => 'users', 'action' => 'profile', $user['id']], ['style' => 'color:;']) ?></span>
                                            <i><span class="description" style="font-size: 11px;"> <?= $timestamp = date('m-d-y', strtotime($post['Original']['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post['Original']['created'])) : date('D, M d, Y h:i A', strtotime($post['Original']['created'])); ?> </span></i> &nbsp; <span class="icon icon-twitter"></span><br>
                                            <p class="description"> <b><?= h(ucwords($post['Original']['title'])) ?></b> </p>
                                            <p class="description"> <?= h($post['Original']['content']) ?> </p>
                                            <?php if (!empty($post['Original']['image'])) { ?>
                                            <div class="description"><b style="font-size:8pt;">Attachment:</b><br>
                                                <?= $this->Html->image('../users/'. $user['username'] . '/posts/' . $post['Original']['image'], ['class' => 'img-thumbnail', 'width' => '20%']) ?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        ?><hr>
                        <?php
                            if ($post['Like']['user_id'] == $current_user['id']) { ?>
                                <button id="like<?= $post['Post']['id'] ?>" class="btn btn-default btn-xs active" onClick="ajax_like_unlike('<?= $post['Post']['id'] ?>', '<?= Router::url(['controller' => 'posts', 'action' => 'ajax_like']) ?>')"> <i class="glyphicon glyphicon-thumbs-down"></i> Unlike</button>
                            <?php } else { ?>
                                <button id="like<?= $post['Post']['id'] ?>" class="btn btn-default btn-xs" onClick="ajax_like_unlike('<?= $post['Post']['id'] ?>', '<?= Router::url(['controller' => 'posts', 'action' => 'ajax_like']) ?>')"> <i class="glyphicon glyphicon-thumbs-up"></i> Like</button>
                            <?php }
                        ?>
                        <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'icon icon-twitter']) . ' Retweet ', ['class' => 'btn btn-default btn-xs', 'data-toggle' => 'modal', 'data-target' => '#retweet'], ['escape' => false]) ?>
                        
                        <div class="box-footer" style="padding-left: 25px">
                            <?php echo $this->Form->create('Comment', ['url' => ['controller' => 'comments', 'action' => 'post_comment']]); ?>
                                <?= $this->Html->image('../users/' . $current_user['username'] . '/profile/' . $current_user['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $current_user['username']]) ?>
                                <div class="img-push">
                                <?= $this->Form->hidden('post_id', ['value' => $post['Post']['id']]); ?>
                                    <?= $this->Form->input('comment', ['label' => false, 'type' => 'text', 'id' => $post['Post']['id'], 'class' => 'form-control input-sm comment-to-post', 'data-id' => $post['Post']['id'], 'data-value' => Router::url(['controller' => 'posts', 'action' => 'post_comment']), 'placeholder' => 'Comment', 'autocomplete' => 'off']) ?>
                                </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <?php foreach($comments as $comment): ?>
                            <div class="box-footer box-comments" style="padding-left: 25px; border-bottom: 1px solid lightgray;">
                                <div class="box-comment">
                                    <?php //echo $comment['Comment']['user_id'] ?>
                                    <?= $this->Html->image('../users/'. $comment['User']['username'] . '/profile/' . $comment['User']['profile'], ['class' => 'img-responsive img-circle img-sm', 'title' => $comment['User']['username']]) ?>
                                    <div class="comment-text">
                                        <span class="username">
                                            <?= $comment['User']['username'] ?>
                                            <span class="text-muted pull-right">
                                            <?php if ($comment['Comment']['user_id'] === $current_user['id']) { ?>
                                            <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-edit', 'data-toggle' => 'tooltip']), ['class' => 'btn btn-box-tool', 'data-toggle' => 'modal', 'data-target' => '#comment'.$comment['Comment']['id']], ['escape' => false]) ?>
                                            <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-remove']), ['action' => 'remove_comment', $comment['Comment']['id'], $post['Post']['id']], ['onClick' => "return confirm('Are you Sure you want to Remove this Comment?')", 'class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Remove', 'escape' => false]) ?><br>
                                            <?php } ?>
                                                <?= $timestamp = date('m-d-y', strtotime($comment['Comment']['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($comment['Comment']['created'])) : date('D, M d, Y h:i A', strtotime($comment['Comment']['created'])); ?>
                                            </span>
                                        </span>
                                        <?= h($comment['Comment']['comment']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="comment<?= $comment['Comment']['id'] ?>" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title text-yellow"><span class="icon icon-edit"></span> Edit Comment</h4>
                                        </div>
                                        <?= $this->Form->create('Comment', ['url' => ['controller' => 'posts', 'action' => 'edit_comment', $comment['Comment']['id'], $post['Post']['id']]]) ?>
                                        <div class="modal-body">
                                            <?= $this->Form->input('comment', ['label' => false, 'type' => 'text', 'id' => $post['Post']['id'], 'class' => 'form-control edit-comment', 'placeholder' => 'Comment', 'value' => $comment['Comment']['comment'], 'autocomplete' => 'off']) ?>
                                        </div>
                                        <div class="modal-footer">
                                            <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
                                            <?= $this->Form->button('Save', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                                        </div>
                                    </div>
                                    <?= $this->Form->end(); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    
                    <div class="modal fade" id="retweet" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title text-yellow"><span class="icon icon-twitter"></span> Retweet</h4>
                                </div>
                                <?= $this->Form->create('Post', ['url' => ['controller' => 'posts', 'action' => 'retweet_post', $post['Post']['id'], 'Retweet'], 'type' => 'file']) ?>
                                <div class="modal-body">
                                    <?= $this->Form->input('title', ['label' => false, 'class' => 'form-control form-group', 'placeholder' => 'Title']) ?>
                                    <?= $this->Form->textarea('content', ['class' => 'form-control form-group', 'rows' => '3', 'placeholder' => 'What\'s on your mind?']) ?>
                                    <?= $this->Form->file('image', ['label' => false, 'class' => 'btn btn-md btn-flat pull-left theme-button', 'onChange' => 'preview(event)']) ?>
                                    <br><br>
                                    <img src="" id="retweetPreview" class="pull-left" width="100px">
                                    <script type="text/javascript">
                                        var preview = function(event) {
                                            var output = document.getElementById("retweetPreview");
                                            output.src = '' + URL.createObjectURL(event.target.files[0]);
                                        };
                                    </script><br><br>
                                </div>
                                <div class="modal-footer">
                                    <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
                                    <?= $this->Form->button('Retweet', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                                </div>
                            </div>
                            <?= $this->Form->end(); ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    </div>
</div>