<?php //require_once 'sidebar.ctp' ?>
<?= $this->element('sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-header">
                        <h3 class="box-title">Edit Post:</h3>
                    </div>
                    <div class="box-body">
                        <?= $this->Session->flash(); ?>
                        <?= $this->Form->create('Post', ['type' => 'file']) ?>
                        <div class="form-group <?= ($this->Form->isFieldError('title'))? 'has-error': '' ; ?>">
                            <?= $this->Form->input('title', ['label' => false, 'class' => 'form-control', 'value' => $post['Post']['title'], 'placeholder' => 'Title', 'autocomplete' => 'off', 'error' => false]) ?>
                            <?= $this->Form->error('title', null, array('class' => 'label label-danger')); ?>
                        </div>
                        <div class="form-group <?= ($this->Form->isFieldError('content'))? 'has-error': '' ; ?>">
                            <?= $this->Form->input('content', ['label' => false, 'class' => 'form-control', 'rows' => '3', 'value' => $post['Post']['content'], 'placeholder' => 'What\'s on your mind?', 'error' => false]) ?>
                            <?= $this->Form->error('content', null, array('class' => 'label label-danger')); ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group pull-left">
                                    <?= $this->Form->input('image', ['type'=> 'file', 'label' => false, 'class' => 'btn btn-md btn-flat theme-button', 'onChange' => 'preview(event)', 'error' => false]) ?>
                                    <?= $this->Form->error('image', null, array('class' => 'help-block label label-danger')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->submit('UPDATE', ['class' => 'btn btn-md btn-flat pull-right theme-button']) ?>
                                <a href="javascript:void(0)" onClick="window.history.back()" class="btn btn-md btn-flat theme-button pull-right" style="margin-right: 5px;">CANCEL</a>
                            </div>
                        </div>
                        <?= $this->Html->image('../users/'. $current_user['username'] . '/posts/' . $post['Post']['image'], ['id' => 'imageEditPreview', 'class' => 'pull-left', 'width' => '25%']) ?>
                        <?= $this->Form->end(); ?>
                        <br><br>
                        <script type="text/javascript">
                            var preview = function(event) {
                                var filename = document.querySelector('input[type=file]').files[0].name;
                                var extension =  filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                                var allowed_extensions = ['jpeg', 'jpg', 'png', 'gif'];
                                var output = document.getElementById('imageEditPreview');
                                if (allowed_extensions.indexOf(extension) !== -1) {
                                    output.src = URL.createObjectURL(event.target.files[0]);
                                }
                            };
                        </script>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>