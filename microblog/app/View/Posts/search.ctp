<?= $this->element('sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-header">
                        <h3 class="box-title">Search Result for "<?= h($title) ?>"</h3>
                    </div>
                    <div class="box-body">
                        <?php if (!$searches) { ?>
                        <div class="col-lg-12 col-xs-12">
                            <div class="box box-widget flat"><h4><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Search Result for "<?= h($title) ?>"</h4></div>
                        </div>
                        <?php } else { foreach ($searches as $search) { ?>
                            <div class="user-panel box-header with-border attachment-block clearfix">
                                <?= $this->Html->image('../users/'. $search['User']['username'] . '/profile/' . $search['User']['profile'], ['class' => 'attachment-img']) ?>
                                <div class="attachment-pushed">
                                    <h3 class="attachment-heading"><?= $this->Html->link($search['User']['username'], ['controller' => 'users', 'action' => 'profile', $search['User']['id']]) ?> <i><h6> &nbsp; &nbsp; (<?= $search['User']['first_name'] . ' ' . $search['User']['last_name'] ?>) <?= $current_user['id'] == $search['User']['id'] ? '(You)' : ''; ?></h6></i></h3>
                                    <h4>
                                    <?php
                                        if ($current_user['id'] !== $search['User']['id']) {
                                            if ($search['Follow']['user_id'] === $current_user['id'] && $search['Follow']['following_id'] === $search['User']['id']) { ?>
                                                <button id="follow<?= $search['User']['id'] ?>" class="btn btn-flat btn-xs theme-button" onClick="ajax_follow_unfollow('<?= $search['User']['id'] ?>', '<?= Router::url(['controller' => 'users', 'action' => 'ajax_follow']) ?>')"> Unfollow</button>
                                            <?php } else { ?>
                                                <button id="follow<?= $search['User']['id'] ?>" class="btn btn-flat btn-xs theme-button" onClick="ajax_follow_unfollow('<?= $search['User']['id'] ?>', '<?= Router::url(['controller' => 'users', 'action' => 'ajax_follow']) ?>')"> Follow</button>
                                            <?php
                                            }
                                        }
                                    ?>
                                    </h4>
                                    <h5 class="attachment-heading"><?= $this->Html->link('See all Posts for "' . $search['User']['username'] . '"', ['controller' => 'users', 'action' => 'profile', $search['User']['id']]) ?></h5>
                                </div>
                            </div>
                        <?php } }?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>