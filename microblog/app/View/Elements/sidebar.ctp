<?php $user = $this->requestAction('users/getUser'); ?>
<div class="wrapper">
    <header class="main-header" style="position: fixed; width:100%;">
        <a href="#" class="logo">
            <span class="logo-mini" style="font-size: 0.8vw;">M BLOG</span>
            <span class="" style="font-size: 16pt; text-transform: uppercase;">My-cro Blog</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">&nbsp;
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div id = "searchContainer">
                <?= $this->Form->create('Search', ['url' => ['controller' => 'posts', 'action' => 'search']]); ?>
                <?= $this->Form->input('search', ['label' => false, 'type' => 'text', 'placeholder' => 'Search name, post & etc.', 'class' => 'form-control', 'style' => 'position: absolute; top: 7px; left: 50px; width: 300px']) ?>
                <?= $this->Form->submit('Search', ['label' => false, 'class' => 'btn btn-flat', 'style' => 'position: absolute; top: 7.4px; left: 350px;']) ?>
                <?= $this->Form->end(); ?>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar" style="position:fixed;">
        <section class="sidebar"><br>
            <div class="user-panel">
                <div class="pull-left image">
                    <?= $this->Html->image('../users/'. $user['username'] . '/profile/' . $user['profile'], ['class' => 'img-circle']) ?>
                </div>
                <div class="pull- info">
                    <p><?= $this->Html->link($user['username'], ['controller' => 'users', 'action' => 'profile'], ['style' => 'color:#fff;']) ?></p>
                    <a href="#"><i class="icon icon-circle text-success"></i> Online</a>
                </div>
            </div><br>
            <ul class="sidebar-menu">
                <li> <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-home']) . '<span> Home </span>', ['controller' => 'posts', 'action' => '/'], ['escape' => false]) ?> </li>
                <li> <?= $this->Html->link($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-log-out']) . '<span> Signout </span>', ['controller' => 'posts', 'action' => 'signout'], ['escape' => false]) ?> </li>
            </ul>
        </section>
    </aside><br><br>
    <section class="content-header">
      <span class="breadcrumb" style="background:transparent!important;"><ol></ol></span>
    </section>