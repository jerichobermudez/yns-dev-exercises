<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Microblog');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
    <head>
		<meta name = "author" content = "Jericho Ramos Bermudez">
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <?= $this->Html->charset() ?>
        <title>
            <?= $cakeDescription ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        <?= $this->HTML->css('bootstrap.min'); ?>
        <?= $this->HTML->css('AdminLTE.min'); ?>
        <?= $this->HTML->css('all-skins.min'); ?>
        <?= $this->HTML->css('font-awesome.min'); ?>
        <?= $this->HTML->css('main'); ?>
    </head>
    <body class="hold-transition skin-yellow sidebar-mini">
		<!-- BACKGROUND CONTAINER -->
		<div id = "bgContainer">
			<!-- BACKGROUND IMAGE -->
			<div id = "bgImage"></div>
		</div>
        <?= $content_for_layout ?>
    </body>
    
    <?= $this->Html->script('jQuery-2.1.4.min'); ?>
    <?= $this->Html->script('bootstrap.min'); ?>
    <?= $this->Html->script('app.min'); ?>
    <?= $this->Html->script('main'); ?>
</html>
