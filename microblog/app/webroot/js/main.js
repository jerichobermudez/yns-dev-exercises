$(document).ready(function() {

});

function ajax_like_unlike(id, url) {
    $.ajax({
        type: 'post',
        url: url,
        data: {id: id},
        dataType: 'json',
        success: function(result) {
            if (result.status === 'unlike') {
                $('#like' + id).removeClass('active').html('<i class="glyphicon glyphicon-thumbs-up"></i> Like').blur();
            } else {
                $('#like' + id).addClass('active').html('<i class="glyphicon glyphicon-thumbs-down"></i> Unlike').blur();
            }
        }
    });
}

function ajax_follow_unfollow(id, url) {
    $.ajax({
        type: 'post',
        url: url,
        data: {id: id},
        dataType: 'json',
        success: function(result) {
            if (result.status === 'unfollow') {
                $('#follow' + id).html('Follow').blur();
            } else {
                $('#follow' + id).html('Unfollow').blur();
            }
        }
    });
}

$(document).on('keypress', '.edit-comment', function(event) {
    if (event.keyCode == 13) {
        // $('#CommentEditCommentForm').submit();
        return false;
    }
});