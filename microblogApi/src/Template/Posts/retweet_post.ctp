<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-twitter"></span> Retweet Post</h4>
    </div>
    <?= $this->Form->create('Post', ['type' => 'file', 'id' => 'retweetPostForm', 'value' => 'posts/post_retweet', 'url' => ['' => '', 'action' => 'post_retweet']]) ?>
    <div class="modal-body">
        <div id="flashMessage"></div>
        <div class="attachment-block clearfix">
            <div class="user-block">
                <?= $this->Html->image('../users/'. $post['User']['username'] . '/profile/' . $post['User']['profile'], ['class' => 'img-circle', 'width' => '128px']) ?>
                <span class="username"><?= $post['User']['username'] ?></span>
                <i><span class="description"> <?= $timestamp = date('m-d-y', strtotime($post->created)) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post->created)) : date('D, M d, Y h:i A', strtotime($post->created)); ?> </span></i>
            </div>
            <b><span class=""> <?= h(ucwords($post->title)) ?> </span></b><br><br>
            <p><?= h($post->content) ?></p>
            <?php if (!empty($post->image)) { ?>
            <div class="attachment-block clearfix"><b>Attachment:</b><br>
                <?= $this->Html->image('../users/'. $post['User']['username'] . '/posts/' . $post->image, ['class' => 'img-thumbnail', 'style' => 'max-width: 30%;']) ?>
            </div>
            <?php } ?>
        </div><br>
        <?= $this->Form->hidden('id', ['value' => $post->id]) ?>
        <div class="form-group" id="post-title">
            <?= $this->Form->control('title', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Title', 'autocomplete' => 'off', 'required' => true]) ?>
            <?= $this->Form->error('title', null, array('class' => 'label label-danger')); ?>
        </div>
        <div class="form-group" id="post-content">
            <?= $this->Form->control('content', ['label' => false, 'class' => 'form-control', 'rows' => '3', 'placeholder' => 'What\'s on your mind?', 'required' => true]) ?>
            <?= $this->Form->error('content', null, array('class' => 'label label-danger')); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('image', ['type'=> 'file', 'label' => false, 'id' => 'retweetImage', 'class' => 'btn btn-md btn-flat theme-button', 'onChange' => 'prev(event)', 'error' => false]) ?>
            <?= $this->Form->error('image', null, array('class' => 'help-block label label-danger')); ?>
        </div>
        <img src="" id="retweetImagePreview" style="max-width: 100%;">
                
        <script type="text/javascript">
            var prev = function(event) {
                var filename = document.getElementById('retweetImage').files[0].name;
                var extension =  filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                var allowed_extensions = ['jpeg', 'jpg', 'png', 'gif'];
                var output = document.getElementById('retweetImagePreview');
                if (allowed_extensions.indexOf(extension) !== -1) {
                    output.src = URL.createObjectURL(event.target.files[0]);
                }
            };
        </script>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
        <?= $this->Form->button('Retweet', ['class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
    <?= $this->Form->end(); ?>
    <?php
        // if ($post->user_id !== $current_user['id']) {
            ?>
            <!-- <div class="modal-body">
                <div class="callout callout-danger">You are not authorized to remove this Post.</div>
            <div> -->
            <?php
        // } else {
        //     echo $this->Form->create('Post', ['type' => 'file', 'id' => 'removePostForm', 'url' => ['' => '', 'action' => 'remove_post', $post->id]]);
            ?>
            <!-- <div class="modal-body">
                <h4>Are you sure you want to <em>Remove</em> this post?</h4>
            </div> -->
            <?php
        // }
    ?>
</div>