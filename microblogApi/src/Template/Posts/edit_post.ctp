<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-pencil"></span> Edit Post</h4>
    </div>
    <?php
        if ($post->user_id !== $current_user['id']) {
            ?>
            <div class="modal-body">
                <div class="callout callout-danger">You are not authorized to edit this Post.</div>
            <div>
            <?php
        } else {
            echo $this->Form->create('Post', ['type' => 'file', 'id' => 'editPostForm', 'value' => 'posts/update_post/' . $post->id, 'url' => ['' => '', 'action' => 'update_post', $post->id]]);
            ?>
            <div class="modal-body">
                <div class="form-group" id="post-title">
                    <?= $this->Form->control('title', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Title', 'autocomplete' => 'off', 'value' => $post->title, 'required' => true]) ?>
                    <?= $this->Form->error('title', null, array('class' => 'label label-danger')); ?>
                </div>
                <div class="form-group" id="post-content">
                    <?= $this->Form->control('content', ['label' => false, 'class' => 'form-control', 'rows' => '3', 'placeholder' => 'What\'s on your mind?', 'value' => $post->content, 'required' => true]) ?>
                    <?= $this->Form->error('content', null, array('class' => 'label label-danger')); ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('image', ['type'=> 'file', 'label' => false, 'id' => 'editImage', 'class' => 'btn btn-md btn-flat theme-button', 'onChange' => 'prev(event)', 'error' => false]) ?>
                    <?= $this->Form->error('image', null, array('class' => 'help-block label label-danger')); ?>
                </div>
                <?php
                    if ($post->image) {
                        echo $this->Html->image('../users/'. $current_user['username'] . '/posts/' . $post->image, ['id' => 'imageEditPreview', 'style' => 'max-width:100%;']);
                    } else {
                        ?>
                        <img src="" id="imageEditPreview" style="max-width:100%">
                        <?php
                    }
                ?>
                
                <script type="text/javascript">
                    var prev = function(event) {
                        var filename = document.getElementById('editImage').files[0].name;
                        var extension =  filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                        var allowed_extensions = ['jpeg', 'jpg', 'png', 'gif'];
                        var output = document.getElementById('imageEditPreview');
                        if (allowed_extensions.indexOf(extension) !== -1) {
                            output.src = URL.createObjectURL(event.target.files[0]);
                        }
                    };
                </script>
            </div>
            <?php
        }
    ?>
    <div class="modal-footer">
        <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
        <?= $this->Form->button('Save', ['class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
    <?= $this->Form->end(); ?>
</div>