<?= $this->element('post/sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-body">
                        <?= $this->Flash->render() ?>
                        <?= $this->Form->create('Post', ['type' => 'file', 'id' => 'addPostForm', 'value' => 'posts/create_post']) ?>
                        <div id="flashMessage"></div>
                        <div class="form-group" id="post-title">
                            <?= $this->Form->control('title', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Title', 'autocomplete' => 'off', 'required' => true]) ?>
                            <?= $this->Form->error('title', null, array('class' => 'label label-danger')); ?>
                        </div>
                        <div class="form-group" id="post-content">
                            <?= $this->Form->control('content', ['label' => false, 'class' => 'form-control', 'rows' => '3', 'placeholder' => 'What\'s on your mind?', 'required' => true, 'style' => 'max-width:100%']) ?>
                            <?= $this->Form->error('content', null, array('class' => 'label label-danger')); ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 col-sm-12">
                                <div class="form-group">
                                    <?= $this->Form->control('image', ['type'=> 'file', 'label' => false, 'class' => 'btn btn-md btn-flat theme-button', 'onChange' => 'preview(event)', 'error' => false]) ?>
                                    <?= $this->Form->error('image', null, array('class' => 'help-block label label-danger')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4 col-sm-12">
                                <?= $this->Form->submit('POST', ['class' => ' btn btn-md btn-flat pull-right theme-button']) ?>
                            </div>
                        </div>
                        <?= $this->Form->end(); ?>
                        <img src="" id="addEditPreview" class="pull-left" style="max-width:50%">
                        <script type="text/javascript">
                            var preview = function(event) {
                                var filename = document.querySelector('input[type=file]').files[0].name;
                                var extension =  filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                                var allowed_extensions = ['jpeg', 'jpg', 'png', 'gif'];
                                var output = document.getElementById('addEditPreview');
                                if (allowed_extensions.indexOf(extension) !== -1) {
                                    output.src = URL.createObjectURL(event.target.files[0]);
                                }
                            };
                        </script>
                    </div>
                </div>
            </div>
            <div id="loadPosts"></div>
        </section>
    </div>
</div>