<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-remove"></span> Remove Post</h4>
    </div>
    <?php
        if ($post->user_id !== $current_user['id']) {
            ?>
            <div class="modal-body">
                <div class="callout callout-danger">You are not authorized to remove this Post.</div>
            <div>
            <?php
        } else {
            echo $this->Form->create('Post', ['type' => 'file', 'id' => 'removePostForm', 'value' => 'posts/remove_post/' . $post->id, 'url' => ['' => '', 'action' => 'remove_post', $post->id]]);
            ?>
            <div class="modal-body">
                <h4>Are you sure you want to <em>Remove</em> this post?</h4>
            </div>
            <?php
        }
    ?>
    <div class="modal-footer">
        <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
        <?= $this->Form->button('Remove', ['class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
    <?= $this->Form->end(); ?>
</div>