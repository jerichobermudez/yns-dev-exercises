<?= $this->Html->script('search'); ?>
<?= $this->element('post/sidebar') ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-body">
                        <?php
                            if (!$users && !$posts) {
                            ?>
                                <div class="box box-widget flat"><h4><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Search Result for &ldquo;<?= h($search) ?>&rdquo;</h4></div>
                            <?php
                            } else {
                        ?>

                        <fieldset>
                            <legend>User result for &ldquo;<?= h($search) ?>&rdquo;</legend>
                            <?php 
                                if (!$users) {
                                    ?>
                                    <a><i class="glyphicon glyphicon-info-sign text-yellow"></i> No User/s found</a>
                                    <?php
                                } else { 
                                    foreach ($users as $user) {
                                        $follow = false;
                                        foreach ($user->followers as $row) {
                                            if ($row['user_id'] === $current_user['id'])
                                                $follow = true;
                                        }
                                        ?>
                                        <div class="user-panel box-header with-border attachment-block clearfix">
                                            <?= $this->Html->image('../users/'. $user->username . '/profile/' . $user->profile, ['class' => 'attachment-img']) ?>
                                            <div class="attachment-pushed">
                                                <h3 class="attachment-heading"><?= $this->Html->link($user->username, ['controller' => 'users', 'action' => 'profile', $user->id], ['class' => 'view-user-profile', 'data-id' => $user->id]) ?> <i><h6> &nbsp; &nbsp; (<?= $user->first_name . ' ' . $user->last_name ?>) <?= $current_user['id'] == $user->id ? '(You)' : ''; ?></h6></i></h3>
                                                <h4>
                                                <?php
                                                    if ($current_user['id'] !== $user->id) {
                                                        $followUnfollow = $follow ? 'Unfollow' : 'Follow';
                                                        echo $this->Form->button($followUnfollow, ['id' => 'follow'.$user->id, 'class' => 'btn btn-flat btn-xs theme-button follow-unfollow', 'data-id' => $user->id]);
                                                    }
                                                ?>
                                                </h4>
                                                <h5 class="attachment-heading"><?= $this->Html->link('See all Posts for "' . $user->username . '"', ['controller' => 'users', 'action' => 'profile', $user->id], ['class' => 'view-user-profile', 'data-id' => $user->id]) ?></h5>
                                            </div>
                                        </div>
                                        <?php 
                                    } 
                                }
                            ?>
                        </fieldset>
                        <hr>
                        <fieldset>
                            <legend>Post result for &ldquo;<?= h($search) ?>&rdquo;</legend>
                            <?php 
                                if (!$posts) {
                                    ?>
                                    <a><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Post/s found</a>
                                    <?php
                                } else {
                                    foreach ($posts as $post) {
                                        $user = $this->requestAction('Users/getUser/'.$post->user_id);
                                        $user = (json_decode($user, false));
                                        ?>
                                        <div class="box box-widget with-border attachment-block">
                                            <?= $this->Html->image('../users/' . $user->username . '/profile/' . $user->profile, ['class' => 'img-responsive img-circle img-sm', 'title' => $user->username]) ?>
                                            <div class="img-push">
                                                <span class="username"><?= $this->Html->link($user->username, ['controller' => 'users', 'action' => 'profile', $user->id], ['class' => 'view-user-profile', 'data-id' => $user->id, 'style' => 'color:;']) ?></span>
                                                <i><span class="description" style="font-size: 11px;"> <?= $timestamp = date('m-d-y', strtotime($post->created)) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post->created)) : date('D, M d, Y h:i A', strtotime($post->created)); ?> </span></i><br>
                                                <p class="description"> <b><?= h(ucwords($post->title)) ?></b> </p>
                                                <p class="description"> <?= h($post->content) ?> </p>
                                                <?php 
                                                    if (!empty($post->image)) { ?>
                                                        <div class="description"><b style="font-size:8pt;">Attachment:</b><br>
                                                            <?= $this->Html->image('../users/'. $user->username . '/posts/' . $post->image, ['class' => 'img-thumbnail', 'style' => 'max-width:30%;']) ?>
                                                        </div>
                                                    <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php 
                                    } 
                                }
                            ?>
                        </fieldset>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>