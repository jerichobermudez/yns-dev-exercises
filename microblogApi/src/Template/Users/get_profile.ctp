<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-pencil"></span> Edit Profile</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="panel panel-warning flat">
                    <div class="panel-heading"><span class = "glyphicon glyphicon-picture"></span> Change Profile</div>
                    <div class="panel-body">
                        <?= $this->Form->create('Profile', ['id' => 'updateProfileForm', 'url' => ['controller' => 'users', 'action' => 'update_photo'], 'type' => 'file']) ?>
                        <?= $this->Html->image('../users/'. $user->username . '/profile/' . $user->profile, ['id' => 'profilePreview', 'class' => '', 'width' => '100%']) ?>
                        <div class="form-group">
                            <?= $this->Form->control('profile', ['label' => false, 'type' => 'file', 'class' => 'form-control', 'onChange' => 'preview(event)', 'required' => 'true']) ?>
                        </div>
                        <?= $this->Form->submit('Update Photo', ['class' => 'btn btn-md btn-flat btn-block theme-button']) ?>
                        <?= $this->Form->end(); ?>
                        <script type="text/javascript">
                            var preview = function(event) {
                                var output = document.getElementById('profilePreview');
                                output.src = '' + URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel panel-warning flat">
                    <div class="panel-heading"><span class = "glyphicon glyphicon-info-sign"></span> Update Information</div>
                    <div class="panel-body">
                        <?= $this->Form->create('Profile', ['id' => 'updateInformationForm', 'url' => ['controller' => 'users', 'action' => 'update_info']]) ?>
                        <div class="form-group" id="profile_first_name">
                            <?= $this->Form->control('first_name', ['label' => 'Firstname:', 'id' => 'first_name', 'class' => 'form-control', 'placeholder' => 'Firstname', 'value' => $user->first_name, 'autocomplete' => 'off']) ?>
                        </div>
                        <div class="form-group" id="profile_last_name">
                            <?= $this->Form->control('last_name', ['label' => 'Lastname:', 'id' => 'last_name', 'class' => 'form-control', 'required' => true, 'placeholder' => 'Lastname', 'value' => $user->last_name, 'autocomplete' => 'off']) ?>
                        </div>
                        <div class="form-group" id="profile_edit_username">
                            <?= $this->Form->control('edit_username', ['label' => 'Username:', 'id' => 'edit_username', 'class' => 'form-control', 'placeholder' => 'Username', 'value' => $user->username, 'autocomplete' => 'off']) ?>
                        </div>
                        <div class="form-group" id="profile_edit_email">
                            <?= $this->Form->control('edit_email', ['label' => 'Email:', 'id' => 'edit_email', 'type' => 'email', 'class' => 'form-control', 'required' => true, 'placeholder' => 'Email', 'value' => $user->email]) ?>
                        </div>
                        <?= $this->Form->submit('Update Information', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
                <div class="panel panel-warning flat">
                    <div class="panel-heading"><span class = "glyphicon glyphicon-lock"></span> Change Password</div>
                    <div class="panel-body">
                        <?= $this->Form->create('Profile', ['id' => 'updatePasswordForm', 'url' => ['controller' => 'users', 'action' => 'update_password']]) ?>
                        <div class="form-group" id="profile_oldpassword">
                            <?= $this->Form->control('oldpassword', ['label' => 'Old Password:', 'type' => 'password', 'class' => 'form-control', 'required' => true, 'placeholder' => 'Old Password']) ?>
                        </div>
                        <div class="form-group" id="profile_password">
                            <?= $this->Form->control('password', ['label' => 'New Password:', 'type' => 'password', 'class' => 'form-control', 'required' => true, 'placeholder' => 'New Password']) ?>
                        </div>
                        <div class="form-group" id="profile_repassword">
                            <?= $this->Form->control('repassword', ['label' => 'Re-type New Password:', 'type' => 'password', 'class' => 'form-control', 'required' => true, 'placeholder' => 'Re-type New Password']) ?>
                        </div>
                        <?= $this->Form->submit('Update Password', ['class' => 'btn btn-md btn-flat theme-button']) ?>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button('Close', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
</div>