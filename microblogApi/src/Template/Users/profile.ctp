<?= $this->element('post/sidebar') ?>
<?= $this->Html->script('profile'); ?>
<div class="row">
    <div class="content-wrapper">
        <section class="content">
            <div class="col-lg-12 col-xs-12">
                <div class="box box-widget flat">
                    <div class="box-header with-border attachment-block clearfix">
                        <div class="box-body">
                            <?php
                                $follow = false;
                                foreach ($user->followers as $row) {
                                    if ($row['user_id'] === $current_user['id'])
                                        $follow = true;
                                }
                            ?>
                            <?= $this->Html->image('../users/'. $user->username . '/profile/' . $user->profile, ['class' => 'attachment-img']) ?>
                            <div class="attachment-pushed">
                                <h3 class="attachment-heading">
                                    <?= $user->username ?>
                                    <em><small class="text-muted">(<?= $user->first_name . ' ' . $user->last_name ?>)</small></em>
                                </h3>
                                <?php
                                    if ($current_user['id'] === $user->id) { ?>
                                        
                                        <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-pencil']) . ' Edit Profile', ['class' => 'btn btn-xs btn-link get-profile', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Edit Profile']) ?>
                                        <h4>
                                            <?= $this->Form->button('Followers', ['class' => 'btn btn-flat btn-xs theme-button user-followers', 'data-toggle' => 'tooltip', 'title' => 'View Followers']) ?>
                                            <?= $this->Form->button('Following', ['class' => 'btn btn-flat btn-xs theme-button user-following', 'data-toggle' => 'tooltip', 'title' => 'View Following']) ?>
                                        </h4>
                                        <?php 
                                    } else { 
                                        $followUnfollow = $follow ? 'Unfollow' : 'Follow' ?>
                                        <h4><button id="follow<?= $user->id ?>" class="btn btn-flat btn-xs theme-button follow-unfollow" data-id="<?= $user->id ?>"> <?= $followUnfollow ?> </button></h4>
                                    <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="loadProfilePosts"></div>
        </section>
    </div>
</div>