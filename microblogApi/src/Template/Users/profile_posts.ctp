<?php 
    if (!json_decode(json_encode($posts), true)) {
        ?>
        <div class="col-lg-12 col-xs-12">
            <div class="box box-widget flat">
                <div class="box-header">
                    <h4><i class="glyphicon glyphicon-info-sign text-yellow"></i> No Post to Display!</h4>
                </div>
            </div>
        </div>
        <?php 
    } else { 
        foreach ($posts as $post) {
        ?>
        <div class="col-lg-12 col-xs-12">
            <div class="box box-widget flat">
                <div class="box-header with-border">
                    <div class="user-block">
                        <?= $this->Html->image('../users/'. $post->user['username'] . '/profile/' . $post->user['profile'], ['class' => 'img-circle', 'width' => '128px']) ?>
                        <span class="username"><?= $this->Html->link($post->user['username'], ['controller' => 'users', 'action' => 'profile', $post->user['id']], ['class' => 'view-user-profile', 'data-id' => $post->user['id'], 'style' => 'color:;']) ?></span>
                        <i><span class="description"> <?= $timestamp = date('m-d-y', strtotime($post->created)) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post->created)) : date('D, M d, Y h:i A', strtotime($post->created)); ?> </span></i>
                    </div>
                    <div class="box-tools">
                        <!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimize"><i class="glyphicon glyphicon-minus"></i></button> -->
                        <?php 
                            if ($post->user_id === $current_user['id']) { ?>
                                <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-edit', 'data-toggle' => 'tooltip', 'title' => 'Edit Post']), ['class' => 'btn btn-box-tool edit-post', 'data-id' => $post->id, 'data-toggle' => 'modal'], ['escape' => false]) ?>
                                <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'glyphicon glyphicon-remove', 'data-toggle' => 'tooltip', 'title' => 'Remove Post']), ['class' => 'btn btn-box-tool delete-post', 'data-id' => $post->id, 'data-toggle' => 'modal'], ['escape' => false]) ?>
                                <?php 
                            } 
                        ?>
                    </div>
                    <div class="box-body">
                        <b><span class=""> <?= h(ucwords($post->title)) ?> </span></b><br><br>
                        <p><?= h($post->content) ?></p>
                        <?php 
                            if (!empty($post->image)) { ?>
                                <div class="attachment-block clearfix"><b>Attachment:</b><br>
                                    <?= $this->Html->image('../users/'. $post->user['username'] . '/posts/' . $post->image, ['class' => 'img-thumbnail', 'style' => 'max-width:40%;']) ?>
                                </div>
                                <?php
                            }

                            if (intval($post->is_retweet) === 1) {
                                if (intval($post->original['deleted']) === 1) { ?>
                                    <div class="callout callout-warning">
                                        <h5><span class="glyphicon glyphicon-info-sign"></span> This Post has been Removed by the Owner</h5>
                                    </div>
                                    <?php 
                                } else {
                                    $user = $this->requestAction('Users/getUser/'.$post->original['user_id']);
                                    $user = (json_decode($user, false));
                                    ?>
                                    <div class="box box-body" style="padding-left: 45px; background: rgba(243, 156, 18, 0.8);">
                                        <?= $this->Html->image('../users/' . $user->username . '/profile/' . $user->profile, ['class' => 'img-responsive img-circle img-sm', 'title' => $user->username]) ?>
                                        <div class="img-push">
                                            <span class="username"><?= $this->Html->link($user->username, ['controller' => 'users', 'action' => 'profile', $user->id], ['class' => 'view-user-profile', 'data-id' => $user->id, 'style' => 'color:;']) ?></span>
                                            <i><span class="description" style="font-size: 11px;"> <?= $timestamp = date('m-d-y', strtotime($post->original['created'])) == date('m-d-y') ? 'Today, ' . date('h:i A', strtotime($post->original['created'])) : date('D, M d, Y h:i A', strtotime($post->original['created'])); ?> </span></i> &nbsp; <span class="icon icon-twitter"></span><br>
                                            <p class="description"> <b><?= h(ucwords($post->original['title'])) ?></b> </p>
                                            <p class="description"> <?= h($post->original['content']) ?> </p>
                                            <?php 
                                                if (!empty($post->original['image'])) { ?>
                                                    <div class="description"><b style="font-size:8pt;">Attachment:</b><br>
                                                        <?= $this->Html->image('../users/'. $user->username . '/posts/' . $post->original['image'], ['class' => 'img-thumbnail', 'style' => 'max-width:30%;']) ?>
                                                    </div>
                                                <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                        <div class="box-footer" style="padding-top:8px!important; padding: 0px;">
                        <?php
                            $likeUnlike = '<i class="glyphicon glyphicon-thumbs-up"></i> Like';
                            $likeTitle = 'Like';
                            $likeActive = '';
                            foreach ($post->likes as $like) {
                                $likeUnlike = $like->user_id === $current_user['id'] ? '<i class="glyphicon glyphicon-thumbs-down active"></i> Unlike' : $likeUnlike;
                                $likeTitle = $like->user_id === $current_user['id'] ? 'Unlike' : $likeTitle;
                                $likeActive = $like->user_id === $current_user['id'] ? 'active' : '';
                            } 
                            ?>
                            <button id="like<?= $post->id ?>" class="btn btn-default btn-xs like-unlike <?= $likeActive ?>" data-toggle="tooltip" title="<?= $likeTitle ?>" data-id="<?= $post->id ?>" data-value="<?= \Cake\Routing\Router::url(['controller' => 'likes', 'action' => 'like_unlike']) ?>"> <?= $likeUnlike ?> </button>
                            <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'icon icon-comments']) . 'Comment', ['class' => 'btn btn-default btn-xs post-comment', 'data-toggle' => 'tooltip', 'title' => 'Comments', 'data-id' => $post->id], ['escape' => false]) ?>
                            <?= $this->Form->button($this->Html->tag('i', ' ', ['class' => 'icon icon-twitter']) . ' Retweet ', ['class' => 'btn btn-default btn-xs retweet-post', 'data-toggle' => 'tooltip', 'title' => 'Retweet Post', 'data-id' => $post->id], ['escape' => false]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        }
        ?>
        <div class="col-lg-12 col-xs-12">
            <div class="box box-widget flat text-center">
                <?php
                    if (count(json_decode(json_encode($posts), false)) >= 5) echo $this->Form->button('Show more', ['class' => 'btn btn-block show-more-result-profile']);
                ?>
            </div>
        </div>
        <?php
    }
?>