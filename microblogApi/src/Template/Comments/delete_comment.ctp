<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-remove"></span> Remove Comment</h4>
    </div>
    <?php
        if ($comment->user_id !== $current_user['id']) {
            ?>
            <div class="modal-body">
                <div class="callout callout-danger">You are not authorized to remove this Comment.</div>
            <div>
            <?php
        } else {
            echo $this->Form->create('Comment', ['type' => 'file', 'id' => 'removeCommentForm', 'value' => 'comments/remove_comment/'.$comment->id, 'url' => ['' => '', 'action' => 'remove_comment', $comment->id]]);
            ?>
            <div class="modal-body">
                <h4>Are you sure you want to <em>Remove</em> this comment?</h4>
            </div>
            <?php
        }
    ?>
    <div class="modal-footer">
        <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
        <?= $this->Form->button('Remove', ['class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
    <?= $this->Form->end(); ?>
</div>