<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-pencil"></span> Edit Comment</h4>
    </div>
    <?php
        if ($comment->user_id !== $current_user['id']) {
            ?>
            <div class="modal-body">
                <div class="callout callout-danger">You are not authorized to edit this Comment.</div>
            <div>
            <?php
        } else {
            echo $this->Form->create('Comment', ['type' => 'file', 'id' => 'editCommentForm', 'value' => 'comments/update_comment/'.$comment->id, 'url' => ['controller' => 'comments', 'action' => 'update_comment', $comment->id]]);
            ?>
            <div class="modal-body">
                <div id="flashMessage"></div>
                <div class="form-group <?= ($this->Form->isFieldError('comment')) ? 'has-error': '' ; ?>">
                    <?= $this->Form->control('comment', ['label' => false, 'class' => 'form-control edit-submit-comment', 'placeholder' => 'Comment', 'autocomplete' => 'off', 'value' => $comment->comment, 'required' => true]) ?>
                    <?= $this->Form->error('comment', null, array('class' => 'label label-danger')); ?>
                </div>
            </div>
            <?php
        }
    ?>
    <div class="modal-footer">
        <?= $this->Form->button('Cancel', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
        <?= $this->Form->button('Save', ['class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
    <?= $this->Form->end(); ?>
</div>