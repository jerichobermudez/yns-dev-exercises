<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-list"></span> Followers</h4>
    </div>
    <div class="modal-body">
        <?php 
            if (!json_decode(json_encode($followers), false)) { ?>
                <div class="callout callout-warning"><span class="glyphicon glyphicon-info-sign"></span> No Followers.</div>
                <?php
            } else {
                foreach ($followers as $follower) {
                    $follow = false;
                    foreach ($users as $row) {
                        if ($row['following_id'] === $follower->user->id)
                            $follow = true;
                    }
                ?>
                <div class="user-panel box-header with-border">
                    <div class="pull-left image">
                        <?= $this->Html->image('../users/'. $follower->user->username . '/profile/' . $follower->user->profile, ['class' => 'img-circle']) ?>
                    </div>
                    <div class="pull- info">
                        <p><?= $this->Html->link($follower->user->username, ['controller' => 'users', 'action' => 'profile', $follower->user->id], ['class' => 'view-user-profile', 'data-id' => $follower->user->id]) ?></p>
                        <?php
                            $followUnfollow = $follow ? 'Unfollow' : 'Follow';
                            echo $this->Form->button($followUnfollow, ['id' => 'follow'.$follower->user->id, 'class' => 'btn btn-flat btn-xs theme-button follow-unfollow', 'data-id' => $follower->user->id]);
                        ?>
                    </div>
                </div>
                <?php
                }
            }
        ?>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button('Close', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
</div>