<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-yellow"><span class="icon icon-list"></span> Following</h4>
    </div>
    <div class="modal-body">
    <?php
        if (!json_decode(json_encode($following), false)) { ?>
            <div class="callout callout-warning"><span class="glyphicon glyphicon-info-sign"></span> No Following.</div>
            <?php
        } else {
            foreach ($following as $follows) {
                ?>
                <div class="user-panel box-header with-border">
                    <div class="pull-left image">
                        <?= $this->Html->image('../users/'. $follows->following->username . '/profile/' . $follows->following->profile, ['class' => 'img-circle']) ?>
                    </div>
                    <div class="pull- info">
                        <p><?= $this->Html->link($follows->following->username, ['controller' => 'users', 'action' => 'profile', $follows->following->id], ['class' => 'view-user-profile', 'data-id' => $follows->following->id]) ?></p>
                        <button id="follow<?= $follows->following->id ?>" class="btn btn-flat btn-xs theme-button follow-unfollow" data-id="<?= $follows->following->id ?>"> Unfollow </button>
                    </div>
                </div>
                <?php
            }
        }
    ?>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button('Close', ['data-dismiss' => 'modal', 'class' => 'btn btn-md btn-flat theme-button']) ?>
    </div>
</div>