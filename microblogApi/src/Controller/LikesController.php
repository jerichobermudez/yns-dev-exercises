<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{

    public function likeUnlike($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $likesTable = $this->loadModel('Likes');
        $likeResult = $likesTable->findByPostIdAndUserId($id, $this->Auth->user('id'))->first();
        if ($likeResult) {
            $like = $this->Likes->get($likeResult->id);
            $this->Likes->delete($like);
            $code = 201;
            $type = 'unliked';
            $message = 'Post Unliked';
        } else {
            $likeResult = $likesTable->newEntity();
            $likeResult->post_id = $id;
            $likeResult->user_id = $this->Auth->user('id');
            $likesTable->save($likeResult);

            $code = 201;
            $type = 'liked';
            $message = 'Post Liked';
        }

        $result = [
            'status' => [
                'code' => $code,
                'type' => $type,
                'message' => $message
            ],
            'data' => [
                'likes' => $likeResult
            ]
        ];

        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Posts'],
        ];
        $likes = $this->paginate($this->Likes);

        $this->set(compact('likes'));
    }

    /**
     * View method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $like = $this->Likes->get($id, [
            'contain' => ['Users', 'Posts'],
        ]);

        $this->set('like', $like);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $like = $this->Likes->newEntity();
        if ($this->request->is('post')) {
            $like = $this->Likes->patchEntity($like, $this->request->getData());
            if ($this->Likes->save($like)) {
                $this->Flash->success(__('The like has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The like could not be saved. Please, try again.'));
        }
        $users = $this->Likes->Users->find('list', ['limit' => 200]);
        $posts = $this->Likes->Posts->find('list', ['limit' => 200]);
        $this->set(compact('like', 'users', 'posts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $like = $this->Likes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $like = $this->Likes->patchEntity($like, $this->request->getData());
            if ($this->Likes->save($like)) {
                $this->Flash->success(__('The like has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The like could not be saved. Please, try again.'));
        }
        $users = $this->Likes->Users->find('list', ['limit' => 200]);
        $posts = $this->Likes->Posts->find('list', ['limit' => 200]);
        $this->set(compact('like', 'users', 'posts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $like = $this->Likes->get($id);
        if ($this->Likes->delete($like)) {
            $this->Flash->success(__('The like has been deleted.'));
        } else {
            $this->Flash->error(__('The like could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
