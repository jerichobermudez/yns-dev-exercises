<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Cake\Http\Client;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('Followers');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
    }

    /**
     * Allow pages method
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['signup', 'forgot', 'activate', 'view']);
    }

    /**
     * Signin method
     *
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $http = new Client();
            $url = $this->pathToApi('users/login');
            $result = $http->post($url, $data, ['type' => 'json'])->getJson();
            $code = $result['status']['code'];
            $message = $result['status']['message'];
            if ($code === 201) {
                $user = $result['data']['user'];
                $user['token'] = $result['data']['token'];
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'posts', 'action' => '/']);
            } else {
                $this->Flash->error(__($message));
            }
        }
    }

    /**
     * Signup method
     *
     * @return \Cake\Http\Response|null
     */
    public function signup()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            
            $http = new Client();
            $url = $this->pathToApi('users/signup');
            $result = $http->post($url, $data, ['type' => 'json'])->getJson();
            $code = $result['status']['code'];
            $message = $result['status']['message'];
            if ($code === 201) {
                $user = $result['data']['user'];
                $folder = new Folder();
                $path = WWW_ROOT . DS . 'users/';
                $userDir = $path . $user['username'] . DS;
                if (!file_exists($folder->create($path))) { $folder->create($path); }
                if (!file_exists($folder->create($userDir))) { $folder->create($userDir); }
                if (!file_exists($folder->create($userDir . 'posts'))) { $folder->create($userDir . 'posts'); }
                if (!file_exists($folder->create($userDir . 'profile'))) {
                    $folder->create($userDir . 'profile');
                    $file = new File(WWW_ROOT . DS . 'img/default.png');
                    if ($file->exists()) {
                        $dir = new Folder($userDir . 'profile', true);
                        $file->copy($dir->path . DS . $file->name);
                    }
                }
                $content = 'Hi ' . $user['first_name'] . ", \n\nYou recently register to Microblog API.\nActivation Code: " . $user['activation_code'] . ". Click the link below to Activate your account\n\nLink: http://" . $_SERVER['HTTP_HOST'] . Router::url(['controller' => 'users', 'action' => 'activate']) . "\n\nThanks";
                $email = new Email('default');
                $email->setTransport('gmail')
                      ->setFrom(['yns@microblog.com' => 'MicroBlog API'])
                      ->setTo($user['email'])
                      ->setSubject('Activate Account')
                      ->send($content);
                $this->Flash->success(__($message));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__($message));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Activate Account method
     *
     * @return \Cake\Http\Response|null
     */
    public function activate()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);

            $http = new Client();
            $url = $this->pathToApi('users/activate');
            $result = $http->post($url, $data, ['type' => 'json'])->getJson();
            $code = $result['status']['code'];
            $message = $result['status']['message'];
            if ($code === 201) {
                $this->Flash->success(__($message));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__($message));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Forgot Password method
     *
     * @return \Cake\Http\Response|null
     */
    public function forgot()
    {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'posts', 'action' => '/']);
        }
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            
            $http = new Client();
            $url = $this->pathToApi('users/forgot');
            $result = $http->post($url, $data, ['type' => 'json'])->getJson();
            $code = $result['status']['code'];
            $message = $result['status']['message'];
            if ($code === 201) {
                $user = $result['data']['user'];
                $content = "You've recently request for new password to your Microblog Account.\nNew Password: " . $user['new_password'] . ".\n\nClick the link below to Login your Account\n\nLink: http://". $_SERVER['HTTP_HOST'] . Router::url(['controller' => 'users', 'action' => 'login']) ."\n\nThanks";
                $email = new Email('default');
                $email->setTransport('gmail')
                      ->setFrom(['yns@microblog.com' => 'MicroBlog API'])
                      ->setTo($user['email'])
                      ->setSubject('Forgot Password')
                      ->send($content);
                $this->Flash->success(__($message));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__($message));
            }
        }
    }

    /**
     * Logout method
     *
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    /**
     * getUser method
     *
     * @return \Cake\Http\Response|null
     */
    public function getUser($id = null)
    {
        $userID = $id ? $id : $this->Auth->user('id');
        $data = $this->Users->findById($userID, 0)->first();
        
        $this->response->body($data); 
        return $this->response;
    }

    /**
     * Profile method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profile($id = null)
    {
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        
        $id = $id ? $id : $this->Auth->user('id');

        $user = $this->Users->get($id, [
            'where' => ['followers.following_id' => $this->Auth->user('id')],
            'contain' => ['followers']
        ]);

        $this->set([
            'user' => $user,
            '_serialize' => ['user']
        ]);
    }

    /**
     * Profile Posts method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profilePosts($id = null, $limit)
    {
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        
        if ($this->request->is('ajax')) {
            $id = $id ? $id : $this->Auth->user('id');
            $posts = $this->Posts->find('all')
                ->where([
                    'Posts.deleted !=' => 1,
                    'Posts.user_id' => $id
                ])
                ->contain(['User', 'Original', 'likes'])
                ->limit($limit)
                ->order(['Posts.created' => 'DESC']);

            $this->set([
                'posts' => $posts,
                '_serialize' => ['posts']
            ]);
        }
    }

    /**
     * Get Edit Profile method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getProfile()
    {
        $this->request->allowMethod(['get']);
        if (!$this->Auth->user('id')) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        
        $user = $this->Users->findById($this->Auth->user('id'))->first();

        $this->set([
            'user' => $user,
            '_serialize' => ['user']
        ]);
    }

    /**
     * Update Image Profile method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updatePhoto()
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);
        $user = $this->Users->newEntity();
        $data = $this->request->getData();
        $user = $this->Users->patchEntity($user, $data);
        
        $usersTable = $this->loadModel('Users');
        $userResult = $usersTable->findById($this->Auth->user('id'))->first();

        $filename = null;
        if (!empty($data['profile']['tmp_name'])) {
            $filename= basename($data['profile']['name']);
            move_uploaded_file($data['profile']['tmp_name'],  WWW_ROOT . DS . 'users' . DS . $this->Auth->user('username') . DS . 'profile' . DS . $filename);
        }

        $userResult->profile = $filename;

        $code = null;
        $message = null;
        $result = [];

        $errors = [];
        foreach ($user->errors() as $key => $error) {
            foreach ($error as $value) {
                $errors = array_merge($errors, [$key => $value]);
            }
        }

        if (!$errors) {
            if ($usersTable->save($userResult)) {
                $code = 201;
                $message = 'Profile successfuly updated.';
            }
        } else {
            $code = 400;
            $message = 'Something went wrong! Please try again.';
            $errors = $errors;
        }
        
        $result = [
            'status' => [
                'code' => $code,
                'message' => $message,
                'error' => $errors
            ],
            'data' => [
                'users' => $userResult
            ]
        ];

        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }

    /**
     * Update Information method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updateInfo()
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);
        $user = $this->Users->newEntity();
        $data = $this->request->getData();
        $user = $this->Users->patchEntity($user, $data);
        
        $usersTable = $this->loadModel('Users');
        $userResult = $usersTable->findById($this->Auth->user('id'))->first();

        $email = $usersTable->find('all', [
            'conditions' => [
                'email' => $data['edit_email'],
                'id !=' => $this->Auth->user('id')
            ]
        ])->first();

        $username = $usersTable->find('all', [
            'conditions' => [
                'username' => $data['edit_username'],
                'id !=' => $this->Auth->user('id')
            ]
        ])->first();

        $oldUsername = $userResult->username;

        $userResult->first_name = h($data['first_name']);
        $userResult->last_name = h($data['last_name']);
        $userResult->username = h($data['edit_username']);
        $userResult->email = h($data['edit_email']);

        $code = null;
        $message = null;
        $result = [];

        $errors = [];
        foreach ($user->errors() as $key => $error) {
            foreach ($error as $value) {
                $errors = array_merge($errors, [$key => $value]);
            }
        }

        if ($email) { $errors = array_merge($errors, ['edit_email' => 'Email already used. Please try another.']); }

        if ($username) { $errors = array_merge($errors, ['edit_username' => 'Username already used. Please try another.']); }

        if (!$errors) {
            if ($usersTable->save($userResult)) {
                $userPath = WWW_ROOT . DS . 'users/';
                $oldDir = $userPath . $oldUsername;
                $newDir = $userPath . $userResult->username;
                rename($oldDir, $newDir);
                
                $code = 201;
                $message = 'Profile successfuly updated.';
            }
        } else {
            $code = 400;
            $message = 'Something went wrong! Please try again.';
            $errors = $errors;
        }
        
        $result = [
            'status' => [
                'code' => $code,
                'message' => $message,
                'error' => $errors
            ],
            'data' => [
                'users' => $userResult
            ]
        ];
        
        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }

    /**
     * Update Password method
     *
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updatePassword()
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);
        $user = $this->Users->newEntity();
        $data = $this->request->getData();
        $user = $this->Users->patchEntity($user, $data);
        
        $usersTable = $this->loadModel('Users');
        $userResult = $usersTable->findById($this->Auth->user('id'))->first();
        
        $code = null;
        $message = null;
        $result = [];

        $errors = [];
        foreach ($user->errors() as $key => $error) {
            foreach ($error as $value) {
                $errors = array_merge($errors, [$key => $value]);
            }
        }

        if (!password_verify($data['oldpassword'], $userResult->password)) {
            $errors = array_merge($errors, ['oldpassword' => 'Old Password not match!. Please try again.']);
        }

        if (password_verify($data['password'], $userResult->password)) {
            $errors = array_merge($errors, ['password' => 'Please create new password!']);
        }

        if (!$errors) {
            $userResult->password = $data['password'];
            if ($usersTable->save($userResult)) {
                $code = 201;
                $message = 'Profile successfuly changed.';
            }
        } else {
            $code = 400;
            $message = 'Something went wrong! Please try again.';
            $errors = $errors;
        }

        $result = [
            'status' => [
                'code' => $code,
                'message' => $message,
                'error' => $errors
            ],
            'data' => [
                'users' => $userResult
            ]
        ];

        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }
}
