<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{

    public function index($id)
    {
        $this->request->allowMethod(['get']);
        $comments = $this->Comments->find('all')
            ->where(['Comments.post_id' => $id, 'Comments.deleted !=' => 1])
            ->contain(['users', 'posts'])
            ->order(['Comments.id' => 'ASC']);
            
        $this->set([
            'id' => $id,
            'comments' => $comments,
            '_serialize' => ['comments']
        ]);
    }

    public function addComment()
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);
        $comment = $this->Comments->newEntity();
        $data = $this->request->getData();
        $comment = $this->Comments->patchEntity($comment, $data);

        $commentsTable = $this->loadModel('Comments');
        $commentResult = $commentsTable->newEntity();
        $comments = $this->Comments->patchEntity($commentResult, $data);

        $commentResult->user_id = $this->Auth->user('id');
        $commentResult->post_id = $data['post_id'];
        $commentResult->comment = h($data['comment']);
        $commentResult->deleted_date = null;

        $code = null;
        $message = null;
        $result = [];

        $errors = [];
        foreach ($comment->errors() as $key => $error) {
            foreach ($error as $value) {
                $errors = array_merge($errors, [$key => $value]);
            }
        }

        if (!$errors) {
            if ($commentsTable->save($commentResult)) {
                $code = 201;
                $message = 'Comment successfuly created';
            }
        } else {
            $code = 400;
            $message = 'Something went wrong! Please try again.';
            $errors = $errors;
        }
        
        $result = [
            'status' => [
                'code' => $code,
                'message' => $message,
                'error' => $errors
            ],
            'data' => [
                'comments' => $commentResult
            ]
        ];

        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }

    /** 
     * Check if user is authorized to edit comment Method
     * 
     * @param string|null $id Comment id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function editComment($id)
    {
        if ($this->request->is('ajax')) {
            $comment = $this->Comments->findById($id)->first();

            $this->set([
                'id' => $id,
                'comment' => $comment,
                '_serialize' => ['comment']
            ]);
        }
    }

    /** 
     * Ajax update comment Method
     * 
     * @param string|null $id Comment id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function updateComment($id)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);
        $comment = $this->Comments->newEntity();
        // if ($this->request->is('post')) {
            $data = $this->request->getData();
            $comment = $this->Comments->patchEntity($comment, $data);
            
            $commentsTable = $this->loadModel('Comments');
            $commentResult = $commentsTable->findById($id)->first();
            
            $commentResult->comment = $data['comment'];

            $code = null;
            $message = null;
            $result = [];

            $errors = [];
            foreach ($comment->errors() as $key => $error) {
                foreach ($error as $value) {
                    $errors = array_merge($errors, [$key => $value]);
                }
            }

            if (!$errors) {
                if ($commentsTable->save($commentResult)) {
                    $code = 201;
                    $message = 'Comment successfuly updated';
                }
            } else {
                $code = 400;
                $message = 'Something went wrong! Please try again.';
                $errors = $errors;
            }
            
            $result = [
                'status' => [
                    'code' => $code,
                    'message' => $message,
                    'error' => $errors
                ],
                'data' => [
                    'comments' => $commentResult
                ]
            ];
        // }

        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }

    /** 
     * Check if user is authorized to remove post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function deleteComment($id)
    {
        if ($this->request->is('ajax')) {
            $comment = $this->Comments->findByIdAndDeleted($id, 0)->first();
            
            $this->set([
                'comment' => $comment,
                '_serialize' => ['comment']
            ]);
        }
    }

    /** 
     * Ajax remove post Method
     * 
     * @param string|null $id Post id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function removeComment($id)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);
            
        $commentsTable = $this->loadModel('Comments');
        $commentResult = $commentsTable->findById($id)->first();

        $commentResult->deleted = 1;
        $commentResult->deleted_date = date('Y-m-d H:i:s');

        if ($commentsTable->save($commentResult)) {
            $code = 201;
            $message = 'Comment successfuly removed.';
        } else {
            $code = 400;
            $message = 'Something went wrong! Please try again.';
        }
        $result = [
            'status' => [
                'code' => $code,
                'message' => $message,
            ],
            'data' => [
                'comments' => ['post_id' => $commentResult->post_id]
            ]
        ];

        $result = json_encode($result);
        $this->response->body($result);
        return $this->response;
    }
}
