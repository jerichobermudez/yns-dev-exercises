<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'signup', 'activate', 'forgot']);
    }

    /**
     * Signin method
     *
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        $this->request->allowMethod(['post']);
        $user = $this->Auth->identify();
        if ($user) {
            if ($user['activated'] === 1) {
                $code = 201;
                $message = 'Login Success!';
            } else {
                $code = 400;
                $message = 'Please Activate your Account First! Check your email to activate your account';
            }
        } else {
            $code = 401;
            $message = 'Invalid Login Credentials!';
        }
    
        $this->set([
            'status' => [
                'code' => $code,
                'message' => $message
            ],
            'data' => [
                'token' => $this->generateToken($user['id']),
                'user' => $user,
            ],
            '_serialize' => ['status', 'data']
        ]);
    }

    public function signup()
    {
        $user = $this->Users->newEntity();
        $this->request->allowMethod(['post']);
        $data = $this->request->getData();
        $user = $this->Users->patchEntity($user, $data);

        $usersTable = $this->loadModel('Users');
        $result = $usersTable->newEntity();
        $users = $this->Users->patchEntity($result, $data);

        $activationCode = 'ABCDEFGHIJKLMNOPQRSYUVWXYZ1234567890';
        $activationCode = substr(str_shuffle($activationCode), 0, 8);
        $result->activation_code = $activationCode;
        if ($usersTable->save($result)) {
            $code = 201;
            $message = 'Registration Success! Please check your email to activate your account.';
        } else {
            $code = 401;
            $message = 'Something went wrong! Please try again.';
        }
        $user['activation_code'] = $activationCode;
        $this->set([
            'status' => [
                'code' => $code,
                'message' => $message
            ],
            'data' => [
                'user' => $user
            ],
            '_serialize' => ['status', 'data']
        ]);
    }

    public function activate()
    {
        $user = $this->Users->newEntity();
        $this->request->allowMethod(['post']);
        $data = $this->request->getData();
        $user = $this->Users->patchEntity($user, $data);
        
        $usersTable = $this->loadModel('Users');
        $result = $usersTable->findByActivationCode($data['activation_code'])->first();
        if ($result) {
            $result->activated = 1;
            $result->activation_code = null;
            $usersTable->save($result);
            $code = 201;
            $message = 'Account Activation Success! You may now login your account.';
        } else {
            $code = 401;
            $message = 'Something went wrong! Please check your email for your Activation Code and try again.';
        }
        $this->set([
            'status' => [
                'code' => $code,
                'message' => $message
            ],
            'data' => [
                'user' => $result
            ],
            '_serialize' => ['status', 'data']
        ]);
    }

    public function forgot()
    {
        $this->request->allowMethod(['post']);
        $data = $this->request->getData();
        $usersTable = $this->loadModel('Users');
        $result = $usersTable->findByEmail($data['email'])->first();
        if ($result) {
            $str = 'ABCDEFGHIJKLMNOPQRSYUVWXYZ1234567890';
            $newPassword = substr(str_shuffle($str), 0, 8);
            $result->password = $newPassword;
            $usersTable->save($result);
            $code = 201;
            $message = 'Password Request Success! Please check your email for your New Password.';
        } else {
            $code = 401;
            $message = 'Something went wrong! Please try again.';
        }
        $result['new_password'] = $newPassword;
        $this->set([
            'status' => [
                'code' => $code,
                'message' => $message
            ],
            'data' => [
                'user' => $result
            ],
            '_serialize' => ['status', 'data']
        ]);
    }
} 
