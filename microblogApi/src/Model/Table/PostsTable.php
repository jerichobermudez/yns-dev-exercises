<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\OriginalPostsTable&\Cake\ORM\Association\BelongsTo $OriginalPosts
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setPrimaryKey('id');

        $this->hasMany('likes');
        $this->hasMany('comments')->setConditions([
            'comments.deleted' => 0
        ]);
        $this->belongsTo('User', [
            'className' => 'users',
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Follower', [
            'className' => 'followers',
            'foreignKey' => false,
            'joinType' => 'LEFT'
        ])->setConditions([
            'OR' => ['Follower.following_id = Posts.user_id']
        ]);
        $this->belongsTo('Original', [
            'className' => 'posts',
            'foreignKey' => 'original_post_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notBlank('title', 'Title cannot be left blank.')
            ->maxLength('title', 35, 'Title must not exceed to 35 characters long');

        $validator
            ->notBlank('content', 'Content cannot be left blank.')
            ->maxLength('content', 140, 'Content must not exceed to 140 characters long');

        $validator
            ->allowEmptyFile('image')
            ->allowEmpty('image')
            ->add('image', 'custom', [
                    'rule' => ['extension', ['jpeg', 'png', 'jpg', 'gif']],
                    'message' => 'Invalid Image Extension. Extension should be jpeg, jpg, png & gif'
            ]);

        return $validator;
    }
}
