$(document).on('click', '.view-user-profile', function() {
    localStorage.setItem('profileId', parseInt($(this).data('id')));
    localStorage.setItem('profileUrl', $(this).data('value'));
});

$(document).on('click', '.follow-unfollow', function() {
    var id = $(this).data('id');
    var url = $(this).data('value');
    $(this).addClass('disabled');
    $.ajax({
        type: 'get',
        url: $('body').attr('root_directory') + 'followers/follow-unfollow/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#follow' + id).html('...');
        },
        success: function(result) {
            console.log(result);
            if (result.status.type === 'unfollow') {
                $('#follow' + id).html('Follow').blur();
            } else {
                $('#follow' + id).html('Unfollow').blur();
            }
            $('#follow' + id).removeClass('disabled');
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
});