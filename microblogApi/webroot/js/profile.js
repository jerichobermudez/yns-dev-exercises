$(document).on('click', '.view-user-profile', function() {
    localStorage.setItem('profileId', parseInt($(this).data('id')));

});

$(document).on('click', '.follow-unfollow', function() {
    var id = $(this).data('id');
    $(this).addClass('disabled');
    $.ajax({
        type: 'get',
        url: root_directory + 'followers/follow-unfollow/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#follow' + id).html('...');
        },
        success: function(result) {
            console.log(result);
            loadModal('followers/following', 'following');
            if (result.status.type === 'unfollow') {
                $('#follow' + id).html('Follow').blur();
            } else {
                $('#follow' + id).html('Unfollow').blur();
            }
            $('#follow' + id).removeClass('disabled');
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
});

function loadModal(url, modal) {
    $.ajax({
        type: 'get',
        url: root_directory + url,
        beforeSend: function () {
            $('#' + modal + 'Content').html(divLoader());
        },
        success: function(result) {
            $('#' + modal + 'Content').html(result);
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
}

$(document).on('click', '.user-followers', function() {
    $("#followersModal").modal();
    loadModal('followers/followers', 'followers');
});

$(document).on('click', '.user-following', function() {
    $("#followingModal").modal();
    loadModal('followers/following', 'following');
});

$(document).on('click', '.get-profile', function() {
    $("#getProfileModal").modal();
    loadModal('users/get-profile', 'getProfile');
});

$(document).on('submit', '#updateProfileForm', function(e) {
    e.preventDefault();
    var formData = new FormData($('#updateProfileForm')[0]);
    formData.append("profile", $("input[type=file]")[0].files[0]);
    $.ajax({
        type: 'post',
        url:  root_directory + 'users/update-photo',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            let status = result.status;
            $.jGrowl(status.message, {
                header: 'CODE: ' + status.code,
                position: 'top-right'
            });
            if (status.code === 201) {
                setTimeout(function(){ window.location.reload(); }, 900);
            } else {
                $('#updateProfileForm .form-group').removeClass('has-error');
                $('#updateProfileForm .error-message').remove();
                $.each(status.error, function(index, value) {
                    $('#updateProfileForm .form-group').addClass('has-error');
                    errorLabelMessage('#updateProfileForm', index, value);
                });
            }
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
});

$(document).on('submit', '#updateInformationForm', function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        type: 'post',
        url:  root_directory + 'users/update-info',
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            console.log(result);
            let status = result.status;
            $.jGrowl(status.message, {
                header: 'CODE: ' + status.code,
                position: 'top-right'
            });
            if (status.code === 201) {
                $('#updateInformationForm .form-group').removeClass('has-error');
                $('#updateInformationForm .error-message').remove();
                setTimeout(function(){ window.location.reload(); }, 900);
            } else {
                $('#updateInformationForm .form-group').removeClass('has-error');
                $('#updateInformationForm .error-message').remove();
                $.each(status.error, function(index, value) {
                    $('#updateInformationForm #profile_' + index).addClass('has-error');
                    errorLabelMessage('#updateInformationForm', index, value);
                });
            }
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
});

$(document).on('submit', '#updatePasswordForm', function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        type: 'post',
        url:  root_directory + 'users/update-password',
        data: formData,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function(result) {
            console.log(result);
            let status = result.status;
            $.jGrowl(status.message, {
                header: 'CODE: ' + status.code,
                position: 'top-right'
            });
            if (status.code === 201) {
                $('#updatePasswordForm .form-group').removeClass('has-error');
                $('#updatePasswordForm .error-message').remove();
                setTimeout(function(){ window.location.reload(); }, 900);
            } else {
                $('#updatePasswordForm .form-group').removeClass('has-error');
                $('#updatePasswordForm .error-message').remove();
                $.each(status.error, function(index, value) {
                    $('#updatePasswordForm #profile_' + index).addClass('has-error');
                    errorLabelMessage('#updatePasswordForm', index, value);
                });
            }
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
});

sessionStorage.setItem('profile-limit', 5);

$(document).on('click', '.show-more-result-profile', function() {
    var limit = parseInt(sessionStorage.getItem('profile-limit')) + 5;
    sessionStorage.setItem('profile-limit', limit);
    $(this).html('Loading...');
    setTimeout(function () {
        loadProfilePost(limit);
    }, 600);
});