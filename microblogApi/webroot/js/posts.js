/** root_directory */
var root_directory = $('body').attr('root_directory');

/** divLoader function for beforeSend data */
function divLoader() {
    return '<div class="col-lg-12 col-xs-12"><div class="box box-widget flat text-center"><img src="' + root_directory + 'webroot/img/hacker-preloader.gif" width="100px"></div></div>';
}

/** Start loadPost function */
function loadPost(limit = 5) {
    $.ajax({
        url:  root_directory + 'posts/post_content/' + limit,
        beforeSend: function () {
            $("#loadPosts").html(divLoader());
        },
        success: function(result) {
            $("#loadPosts").html(result).fadeIn();
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
}

/** Initialize load Post */
loadPost();
/** End loadPost function */

/** Start loadProfilePost function */
function loadProfilePost(limit = 5) {
    var profileId = localStorage.getItem('profileId') ? localStorage.getItem('profileId') : '';
    $.ajax({
        url:  root_directory + 'users/profile-posts/' + profileId + '/' + limit,
        beforeSend: function () {
            $("#loadProfilePosts").html(divLoader());
        },
        success: function(result) {
            $("#loadProfilePosts").html(result);
        }
    });
}
/** End loadProfilePost function */

/** Initialize load Post */
loadProfilePost();
/** End loadPost function */

/** Start postSubmitForm function */
function postSubmitForm(formID) {
    $(document).on('submit', formID, function(e) {
        e.preventDefault();
        var formData = new FormData($(formID)[0]);
        formData.append("image", $("input[type=file]")[0].files[0]);
        var url = $(this).attr('value');
        $.ajax({
            type: 'post',
            url:  root_directory + url,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },
            success: function(result) {
                let status = result.status;
                console.log(result);
                $.jGrowl(status.message, {
                    header: 'CODE: ' + status.code,
                    position: 'top-right'
                });
                if (status.code === 201) {
                    $('.modal').modal('hide')
                    $(formID)[0].reset();
                    loadPost();
                    loadProfilePost();
                    $(formID + ' .form-group').removeClass('has-error');
                    $(formID + ' .error-message').remove();
                    $('#addEditPreview, input[type=file]').attr('src', '');
                } else {
                    $(formID + ' .form-group').removeClass('has-error');
                    $(formID + ' .error-message').remove();
                    setTimeout(function() {
                        $(formID + ' .form-group').removeClass('has-error');
                        $(formID + ' .error-message').fadeOut();
                    }, 10000);
                    $.each(status.error, function(index, value) {
                        if (index === 'title' || index === 'content'|| index === 'image') {
                            $(formID + ' #post-' + index).addClass('has-error');
                            $(formID + ' #post-' + index).addClass('has-error');
                            errorLabelMessage(formID, index, value);
                            if (index === 'image') { 
                                errorLabelMessage(formID, 'editImage', value);
                                errorLabelMessage(formID, 'retweetImage', value);
                            }
                        }
                    });
                }
            },
            error: function(result) {
                errorGrowl(result.status, result.statusText);
            }
        });
    });

}
/** End postSubmitForm function */

/** Initialize postSubmitForm functions */
postSubmitForm('#addPostForm');
postSubmitForm('#editPostForm');
postSubmitForm('#removePostForm');
postSubmitForm('#retweetPostForm');

/** Start function errorLabelMessage */
function errorLabelMessage(form, input, message) {
    var createElement = $(document.createElement('div'));
    createElement.addClass('error-message').text(message);
    createElement.insertAfter($(form + ' #' + input));
}
/** End function errorLabelMessage */

/** Start showModal function */
function showModal(classBtn, targetMdl, controller) {
    $(document).on('click', '.' + classBtn, function() {
        $(targetMdl + 'Modal').modal();
        var url = root_directory + controller + '/' + classBtn + '/' + $(this).data('id');
        $.ajax({
            type: 'ajax',
            url:  url,
            beforeSend: function () {
                $(targetMdl + "Content").html(divLoader());
            },
            success: function(result) {
                $(targetMdl + "Content").html(result);
            },
            error: function(result) {
                errorGrowl(result.status, result.statusText);
            }
        });
    });
}
/** End function errorLabelMessage */

/** Initialize showModal functions */
showModal('delete-post', '#deletePost', 'posts');
showModal('edit-post', '#editPost', 'posts');
showModal('retweet-post', '#retweet', 'posts');
showModal('edit-comment', '#editComment', 'comments');
showModal('delete-comment', '#deleteComment', 'comments');

/** Start loadComment function */
function loadComment(id) {
    $.ajax({
        url: root_directory + 'comments/index/' + id,
        beforeSend: function () {
            $("#commentPostContent").html(divLoader());
        },
        success: function(result) {
            $("#commentPostContent").html(result);
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
}
/** End loadComment function */

/** Start Show comments on posts */
$(document).on('click', '.post-comment', function() {
    $("#commentPostModal").modal();
    loadComment($(this).data('id'));
});
/** End Show comments on posts */

/** Submit addCommentForm if hit enter */ 
$(document).on('keypress', '.comment-to-post', function(event) {
    if (event.keyCode == 13) {
        $('#addCommentForm').submit();
        return false;
    }
});

/** Submit editCommentForm if hit enter */ 
$(document).on('keypress', '.edit-submit-comment', function(event) {
    if (event.keyCode == 13) {
        $('#editCommentForm').submit();
        return false;
    }
});

/** Start commentSubmitForm function */
function commentSubmitForm(formID) {
    $(document).on('submit', formID, function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        var url = $(this).attr('value');
        $.ajax({
            type: 'post',
            url: root_directory + url,
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },
            success: function(result) {
                let status = result.status;
                $.jGrowl(status.message, { header: 'CODE: ' + status.code, position: 'top-right' });
                
                if (status.code === 201) {
                    $("#editCommentModal, #deleteCommentModal").modal('hide');
                    loadComment(result.data.comments.post_id);
                } else {
                    $(formID + ' .form-group').removeClass('has-error');
                    $(formID + ' .error-message').remove();
                    setTimeout(function() {
                        $(formID + ' .form-group').removeClass('has-error');
                        $(formID + ' .error-message').fadeOut();
                    }, 10000);
                    $.each(status.error, function(index, value) {
                        $(formID + ' .form-group').addClass('has-error');
                        errorLabelMessage(formID, index, value);
                    });
                }
            },
            error: function(result) {
                errorGrowl(result.status, result.statusText);
            }
        });
    });
}
/** end commentSubmitForm function */

/** Initialize commentSubmitForm functions */
commentSubmitForm('#addCommentForm');
commentSubmitForm('#editCommentForm');
commentSubmitForm('#removeCommentForm');

/** Like/unlike function */
$(document).on('click', '.like-unlike', function() {
    var id = $(this).data('id');
    $.ajax({
        type: 'get',
        url: root_directory + 'likes/like_unlike/' + id,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            $('#like' + id).html('...');
        },
        success: function(result) {
            if (result.status.type === 'unliked') {
                $('#like' + id).removeClass('active').html('<i class="glyphicon glyphicon-thumbs-up"></i> Like').attr('data-original-title', 'Like').blur();
            } else {
                $('#like' + id).addClass('active').html('<i class="glyphicon glyphicon-thumbs-down"></i> Unlike').attr('data-original-title', 'Unlike').blur();
            }
            console.log(result);
        },
        error: function(result) {
            errorGrowl(result.status, result.statusText);
        }
    });
});

$(document).on('click', '.view-user-profile', function() {
    localStorage.setItem('profileId', parseInt($(this).data('id')));
});

sessionStorage.setItem('limit', 5);

$(document).on('click', '.show-more-result', function() {
    var limit = parseInt(sessionStorage.getItem('limit')) + 5;
    sessionStorage.setItem('limit', limit);
    $(this).html('Loading...');
    setTimeout(function () {
        loadPost(limit);
    }, 600);
});

function errorGrowl(code, message) {
    // setInterval(function() {
        $.jGrowl(message, {
            header: 'ERROR CODE: ' + code,
            position: 'top-right'
        });
    // }, 5000);
}