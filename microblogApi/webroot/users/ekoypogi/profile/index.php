<!DOCTYPE html>
<html ng-app = "timekeepingApp" ng-cloak>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>SPM TIMEKEEPING</title>
        <link rel="shortcut icon" href="https://127.0.0.1/hrsys_dev/images/logo.png"/>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel="stylesheet" href="https://127.0.0.1/hrsys_dev/dependencies/adminlte-2.4.3/bower_components/bootstrap/dist/css/bootstrap.min.css">

        <!-- AdminLTE CSS -->
        <link rel="stylesheet" href="https://127.0.0.1/hrsys_dev/dependencies/adminlte-2.4.3/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="https://127.0.0.1/hrsys_dev/dependencies/adminlte-2.4.3/dist/css/skins/_all-skins.min.css">
        <script src = "https://127.0.0.1/hrsys_dev/dependencies/angularjs-1.6.9/angular.min.js"></script>
        <script src = "https://127.0.0.1/hrsys_dev/dependencies/jquery-3.3.1/jquery.min.js"></script>
        
        <!-- Sweet Alert CSS / JS -->
        <!-- <script src="dependencies/sweetalert/sweetalert.min.js"></script>
        <link rel="stylesheet" href="dependencies/sweetalert/sweetalert.css"> -->
        <script src="dependencies/sweetalert2/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="dependencies/sweetalert2/sweetalert2.min.css">

        <style> .login2 .loginBackdrop { width: 100%; height: 100%; background: url('https://127.0.0.1/hrsys_dev/images/index.jpg') no-repeat center; background-size: cover; position: fixed; } .btn-secondary { background-color: #6c757d; color: #ffffff; } </style>
    </head>
    <body class = "login2 bg-info" ng-controller = "timekeepingCtrl">
        <div class = "loginBackdrop"></div>
        <div class = "row pad">
            <div class = "col-md-8 col-sm-10 col-md-offset-2 col-sm-offset-1">
                <div class = "box box-solid box-primary">
                    <div class = "box-header">
                        <h5 class = "box-title">SPM | TIMEKEEPING</h5>
                        <h5 class = "box-title pull-right">{{displayTime}}</h5>
                    </div>
                    <div class = "box-body">
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <label for = "idNumber"><span class="btn btn-primary btn-flat" ng-click="searchEmployee()">ID NUMBER</span></label>
                                    </div>
                                    <form>
                                    <input id="idNumber" type="text" class="form-control" placeholder="INPUT YOUR ID NUMBER HERE..." ng-model="searchEmployeeID" ng-keypress="($event.keyCode === 13) && searchEmployee()" style = "font-size: 16pt;"></form>
                                </div>
                            </div>
                            <div ng-show = "showSummary">
                                <div class = "col-md-12"><hr></div>
                                <div class = "col-md-12">    
                                    <div class = "row">
                                        <div class = "col-lg-7">
                                            <div class = "box box-solid box-primary">
                                                <div class = "box-header">
                                                    <span class = "box-title">Summary: </span>
                                                </div>
                                                <div class = "box-body pad">
                                                    <table class = "table table-bordered" border width = "100%" style = "font-family: cursive; font-size: 18px;">
                                                        <tr>
                                                            <th width = "130px">ID Number: </th>
                                                            <td>{{employee.number}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Name: </th>
                                                            <td>{{employee.name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Time-In: </th>
                                                            <td>
                                                                {{employee.timeIn}}
                                                                <span ng-class = "{
                                                                    'bg-red badge' : employee.inStatus === 'Halfday' || employee.inStatus === 'Late' || employee.inStatus === 'Absent',
                                                                    'bg-green badge' : employee.inStatus === 'Regular'
                                                                }">
                                                                    {{employee.inStatus}}
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Time-Out: </th>
                                                            <td>
                                                                {{employee.timeOut}}
                                                                <span ng-class = "{
                                                                    'bg-green badge' : employee.outStatus === 'Regular',
                                                                    'bg-red badge' : employee.outStatus === 'Undertime'
                                                                }">
                                                                    {{employee.outStatus}}
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <div class = "row">
                                                        <div class = "col-xs-6">
                                                            <button id = "cancelLogBtn" type = "button" ng-click = "cancelLogin()" class = "btn btn-secondary btn-block btn-flat">Cancel</button>
                                                        </div>
                                                        <div class = "col-xs-6">
                                                            <button id = "saveLogBtn" type = "button" ng-disabled = "loginDisable" ng-click = "saveLog()" class = "btn btn-primary btn-block btn-flat">{{logButton}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class = "col-lg-5">
                                            <center>
                                                <div ng-show = "showPreview" id = "results"></div>
                                                <div ng-show = "showCamera" class = "img-rounded" id="livePreview" style = "border: 1px solid #3c8dbc;"></div>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script type = "text/javascript" src = "dependencies/webcam/webcam.min.js"></script>

    <script src = "js/main.js"></script>
    
</html>