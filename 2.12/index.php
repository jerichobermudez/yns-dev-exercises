<!DOCTYPE html>
<html>
    <head>
        <title>Exercise 2-12 | Show another image when you mouse over an image.</title>
    </head>
    <body>
        <h3>Show another image when you mouse over an image.</h3>
        <img src="https://cdn.shopify.com/s/files/1/0712/4751/products/BX7E-02E_High_Large_TOP_2000x.png" id="image" width="250px">
    </body>
    <script type="text/javascript" src="main.js"></script>
</hrml>